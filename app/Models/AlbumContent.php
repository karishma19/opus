<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlbumContent extends Model
{
    protected $table ='album_content';
	
	protected $primaryKey = 'albumcontent_id';

    protected $fillable =['albumcontent_id','post_id','useralbum_id','user_id','content_type','content_title','filename','content_folder','likes'];
    
    public $timestamps = false;
}
