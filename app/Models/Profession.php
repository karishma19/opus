<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table ='profession';
    
    protected $fillable =['name','is_active','i_date','u_date'];//
}
