<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationTable extends Model
{
    protected $table = 'ss_notification';

    protected $fillable =['notification_id','notify_id','notify_by','notify_to','type','is_read','i_time'];
    public $timestamps = false;
}
