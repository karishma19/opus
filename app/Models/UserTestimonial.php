<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTestimonial extends Model
{
    protected $table = 'user_testimonial';
    protected $fillable =['user_testimonial_id','testimonial','user_id','i_by','i_date'];
    public $timestamps = false;
}
