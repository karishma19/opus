<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model
{
    protected $table ='user_tags';
    protected $primaryKey = 'usertag_id';
    protected $fillable =['usertag_id','user_id','tag_id','likes','i_by','i_date','u_by','u_date'];
    public $timestamps = false;
}
