<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    protected $table ='post_like';
    protected $primaryKey = 'postlike_id';
    protected $fillable =['postlike_id','user_id','post_id','i_date'];
    public $timestamps = false;
}
