<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{
    protected $table = 'user_friends';
    protected $primaryKey = 'userfriend_id';
    protected $fillable =['userfriend_id','user_id','friend_id','group_id'];
    public $timestamps = false;
}
