<?php



namespace App\Models;



use Illuminate\Notifications\Notifiable;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Passport\HasApiTokens;

use Illuminate\Database\Eloquent\Model;
use App\Models\Profession;


class UserMaster extends Authenticatable

{

    use HasApiTokens,Notifiable;

    

    protected $table ='user_master';

	

	protected $primaryKey = 'user_id';



    protected $fillable =['user_id','fullname','email_id','user_password','user_photo','gender','profession','location','city','state','latitude','longitude','brief_desc','birth_date','is_active','is_search','is_deleted','i_by','i_date','u_by','u_date','captcha','match_code','search_keyword','online_status','is_block','device_type','device_token','social_id','is_notify','badge','block_count','first_login','firebase'];

    

    public $timestamps = false;

    // public function getBirthDateAttribute($value){
    //     return date('F d,Y', strtotime($this->attributes['birth_date']));
    // }
    // public function getbirthDateAttribute($value) {
    //     if($value == '1970-01-01' || $value == NUll || $value == '0000-00-00'){
    //         $date = '';
    //     }else{
    //         $date = $value;
    //     }
    //         return $date;
    // }

    public function getprofession()
    {
        return $this->hasOne('App\Models\Profession','id','profession');

    }
}

