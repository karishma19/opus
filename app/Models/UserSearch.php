<?php



namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserMaster;


class UserSearch extends Model

{

    protected $table = "user_search";

    protected $fillable = ['id','user_id','keyword','frienduser_id'];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\Models\UserMaster','user_id','id');

    }

}

