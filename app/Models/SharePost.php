<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharePost extends Model
{
    protected $table ='post_share';
    protected $primaryKey = 'postshare_id';
    protected $fillable =['postshare_id','user_id','post_id','type','share_text','is_share','i_by','i_date','u_by','u_date'];
    public $timestamps = false;
}
