<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPost extends Model
{
    protected $table = "user_post";
    protected $primaryKey = 'post_id';
    protected $fillable = ['post_id','user_id','post_caption','message','post_type','post_media','likes','is_deleted','i_by','i_date','u_by','u_date'];
    public $timestamps = false;
}
