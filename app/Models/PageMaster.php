<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageMaster extends Model
{
    protected $table = "page_master";
    protected $fillable = ['page_id','page_name','page_title','page_desc','title_tag','meta_tags','meta_desc','is_active','is_deleted','i_by','i_date','u_by','u_date'];
}
