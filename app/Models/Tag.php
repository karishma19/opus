<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class Tag extends Model

{

    protected $table ='tags';



    protected $fillable =['id','name','is_active','i_date','u_date','default_tag'];

    public $timestamps = false;

}

