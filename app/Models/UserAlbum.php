<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAlbum extends Model
{
    protected $table = "user_albums";
    protected $primaryKey = 'useralbum_id';
    protected $fillable = ['useralbum_id','user_id','album_title'];
    public $timestamps = false;
}
