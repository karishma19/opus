<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserComment extends Model
{
    protected $table = "user_comment";
 
    protected $fillable = ['comment_id','post_id','user_id','comment','is_approved','is_deleted','i_by','i_date']; 
    public $timestamps = false;
}
