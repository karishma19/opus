<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserChatHistory extends Model
{
    protected $table ='user_chat_history';
    protected $fillable =['user_id','to_user_id','is_block','i_date','u_date','is_dot_allow'];
    public $timestamps = false;

    public function userIsBlock()
    {
        return $this->hasMany('App\Models\UserChatHistory','user_id','to_user_id');
    }
}
