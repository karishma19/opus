<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{
    protected $table ='group_master';
    protected $primaryKey = 'group_id';
    protected $fillable =['group_id','user_id','group_title'];
    public $timestamps = false;
}
