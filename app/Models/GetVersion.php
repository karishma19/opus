<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GetVersion extends Model
{
    protected $table = "get_versions";
    protected $fillable = ['id','version','type'];
}
