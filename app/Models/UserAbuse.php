<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class UserAbuse extends Model

{

    protected $table = "user_abuse";

    protected $fillable = ['abuse_id','user_id','abuse_by','no_of_abuse'];
    
    public $timestamps = false;
}

