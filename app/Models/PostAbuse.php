<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostAbuse extends Model
{
    protected $table = 'post_abuse';
    protected $fillable =['postabuse_id','user_id','post_id'];
    public $timestamps = false;
   
}
