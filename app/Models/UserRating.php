<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    protected $table ='user_ratings';
	
	protected $primaryKey = 'userrating_id';

    protected $fillable =['userrating_id','user_id','rating_type','review','rate','i_by','i_date'];
    
    public $timestamps = false;
}
