<?php



namespace App\Providers;



use Illuminate\Support\Facades\Event;

use Illuminate\Auth\Events\Registered;

use Illuminate\Auth\Listeners\SendEmailVerificationNotification;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;



class EventServiceProvider extends ServiceProvider

{

    /**

     * The event listener mappings for the application.

     *

     * @var array

     */

    protected $listen = [

                Registered::class => [

                SendEmailVerificationNotification::class,

            ],

            'App\Events\AddUserTagEvent' => [

                'App\Listeners\AddUserTag',

            ],

            'App\Events\UserTagEvent' => [

                'App\Listeners\UserTag',

            ],

            'App\Events\GetFriendEvent' => [

                'App\Listeners\GetFriendListener',

            ],



            'App\Events\UserProfileEvent' => [

                'App\Listeners\UserProfileListener',

            ],

            'App\Events\EditProfileEvent' => [

                'App\Listeners\EditProfileListener',

            ],

            'App\Events\CreateCommentEvent' => [

                'App\Listeners\CreateCommentListener',

            ],



            'App\Events\GiveRateEvent' => [

                'App\Listeners\GiveRateListener',

            ],

                'App\Events\NotificationSettingEvent' => [

                'App\Listeners\NotificationSettingListener',

            ],

                'App\Events\AddFriendEvent' => [

                'App\Listeners\AddFriendListener',

            ],

            'App\Events\GetTestimonialEvent' => [

                'App\Listeners\GetTestimonialListener',

            ],

            'App\Events\AddTestimonialEvent' => [

                'App\Listeners\AddTestimonialListener',

            ],

            'App\Events\AbusePostEvent' => [

                'App\Listeners\AbusePostListener',

            ],

            'App\Events\SearchEvent' => [

                'App\Listeners\SearchListener',

            ],

            'App\Events\SearchResultEvent' => [

                'App\Listeners\SearchResultListener',

            ],

            'App\Events\AbusePostEvent' => [

                'App\Listeners\AbusePostListener',

            ],







                'App\Events\CreatePostEvent' => [

                'App\Listeners\CreatePostListener',

            ],

                'App\Events\FeedListEvent' => [

                'App\Listeners\FeedListListener',

            ],

        'App\Events\aboutUserEvent' => [

            'App\Listeners\aboutUserListener',

        ],

        'App\Events\tagsEvent' => [

            'App\Listeners\tagsListener',

        ],

        'App\Events\notificationEvent' => [

            'App\Listeners\notificationListener',

        ],

        'App\Events\postsEvent' => [

            'App\Listeners\postsListener',

        ],

         'App\Events\SharePostEvent' => [

            'App\Listeners\SharePostListener',

        ],

        'App\Events\CommentListEvent' => [

            'App\Listeners\CommentListListener',

        ],
        'App\Events\AddTagEvent' => [

            'App\Listeners\AddTagListener',

        ],
         'App\Events\LikeListEvent' => [

            'App\Listeners\GetLikeListener',

        ],

    ];



    /**

     * Register any events for your application.

     *

     * @return void

     */

    public function boot()

    {

        parent::boot();



        //

    }

}

