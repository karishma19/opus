<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\UserMaster;

class sendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $device_token;

    protected $message;
    protected $notification_type;
    protected $post_id;
    protected $title;
    protected $notificationCount;

    public function __construct($device_token,$message,$notification_type,$post_id,$title,$notificationCount)
    {
        $this->device_token = $device_token;
        $this->message = $message;
        $this->notification_type = $notification_type;
        $this->post_id = $post_id;
        $this->title = $title;
        $this->notificationCount = $notificationCount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $device_token = $this->device_token;
        $message = $this->message;
        $notification_type = $this->notification_type;
        $post_id = $this->post_id;
        $title = $this->title;
        $notificationCount = $this->notificationCount;
       
        // $is_notify = 'N';
        $is_notify = UserMaster::where('device_token',$device_token)->where('is_deleted','N')->where('is_active','Y')->value('is_notify');
        $device_type = UserMaster::where('device_token',$device_token)->where('is_deleted','N')->where('is_active','Y')->value('device_type');
        
        if($device_token != '' && $is_notify == 'Y')
        {
            $previous_badge = 0;        
            $new_badge = 0;
            $get_badge = UserMaster::where('device_token',$device_token)
                                        ->where('is_deleted','N')
                                        ->where('is_active','Y')
                                        ->value('badge');

            if (!empty($get_badge)) { 
                $new_badge = $get_badge + 1;   
                $badge_update = UserMaster::where('device_token',$device_token)->update(['badge' => $new_badge]);
            }   
            // url 
            $url = 'https://fcm.googleapis.com/fcm/send';

        
            $msg = array
            (
                'data' => $title,
                'body'  => $message,
                "sound"=> "default",       
                "content-available"=> 1,       
                "mutable-content"=> 1, 
                'type'  => $notification_type,
                'badge'  => $notificationCount
            );
            // registration_ids  = divice ID
            // $fields = array (
            //     'registration_ids' => array ($device_token),
            //     'data'=>$msg,
            //     'notification' => $msg
            // );
            $fields = array (
                    'registration_ids' => array ($device_token),
                    'notification'=>$msg
            );
            if($device_type== '0'){
                $fields = array (
                    'registration_ids' => array ($device_token),
                    'notification'=>$msg,
                    'data'=> ['data' => $title]
                );
            }
            $fields = json_encode ( $fields );
            
            $headers = array ('Authorization: key=' . Authorization_key,
                'Content-Type: application/json'
                        );

            $notification = $this->SendNotification($url, $msg, $fields,$headers);
            // if($device_type== '1'){
            //     $fields =[];
            //     $fields = array (
            //         'registration_ids' => array ($device_token),
            //         'data'=>$msg
            //     );

            //     $fields = json_encode ( $fields );

            //     $notification = $this->SendNotification($url, $msg, $fields,$headers);
            // }
            return;
        }    
        
    }

    // send notification
    public function SendNotification($url, $msg, $fields,$headers){

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        // echo "<pre>";
        // print_r($result);
        // exit();
        curl_close ( $ch ); 
        return;
    }
    
}
