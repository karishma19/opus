<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserMaster;
use App\Models\Profession;
use App\Helpers\FileHelp;

class EditProfileJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($profile_data)
    {
        $this->profile_data = $profile_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $profile_data = $this->profile_data;
        // dd($profile_data);

        if(isset($profile_data['fullname'])){
            $fullname = $profile_data['fullname'];
        }
        else{
            $fullname = UserMaster::select('fullname')->where('user_id',$profile_data['user_id'])->first();
            $fullname = $fullname['fullname'];
        }
        //image
        if(isset($profile_data['profile_picture'])){
            $profile_data_picture = $profile_data['profile_picture'];
        //dd($profile_data_picture);

            $path = public_path().'/uploads/images/';
            if ($profile_data_picture != null) {
                $ext = explode('.',$profile_data_picture->getClientOriginalName());
                
                $prefix = $ext[0];
                $prefix = str_replace(' ', '', $prefix);
                //dd($prefix);
                $filename = $prefix . '_' . time() . '.' . strtolower($ext[1]);
                //dd($filename);
                $profile_data_picture->move($path, $filename);
                
                // compression code of image
                $source_img = $path . "$filename";
                $destination_img = $path . "$filename";
                // End compression code of image
                
                $path_to_image_directory = $path . "$filename";
                $path_to_thumbs_directory = $path . "img_thumbs/$filename";
                
                // ThumbNail image Code
                // $createdThumbnail = $this->createThumbnail($path_to_image_directory,$path_to_thumbs_directory,240,240,false);
                
                $filename1 = $path . "$filename";
                
                $pathimg = $path . "img_thumbs/$filename";
                
                $imgSrc = $filename1;
                $ext = explode(".", $filename1);
                $fext = $ext[sizeof($ext) - 1];
                // getting the image dimensions
                list ($width, $height) = getimagesize($imgSrc);
                if ($fext == 'jpg' || $fext == 'jpeg')
                    $myImage = ImageCreateFromJPEG($imgSrc);
                elseif ($fext == 'png')
                    $myImage = imagecreatefrompng($imgSrc);
                elseif ($fext == 'gif')
                    $myImage = imagecreatefromgif($imgSrc);
                    // calculating the part of the image to use for thumbnail
                if ($width > $height) {
                    $y = 0;
                    $x = ($width - $height) / 4;
                    $smallestSide = $height;
                } else {
                    $x = 0;
                    $y = ($height - $width) / 4;
                    $smallestSide = $width;
                }
                
                // copying the part into thumbnail
                $thumbSizew = 240;
                $thumbSizeh = 240;
                $thumb = imagecreatetruecolor($thumbSizew, $thumbSizeh);
                imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSizew, $thumbSizeh, $smallestSide, $smallestSide);
                ob_start();
                if ($fext == 'jpg' || $fext == 'jpeg')
                    imagejpeg($thumb, $pathimg);
                elseif ($fext == 'png')
                    imagepng($thumb, $pathimg);
                elseif ($fext == 'gif')
                    imagegif($thumb, $pathimg);
                
                $rawImageBytes = ob_get_clean();
            }
        }
        else {
            $get_image = UserMaster::select('user_photo')->where('user_id',$profile_data['user_id'])->first();
            //dd($get_image);
            $filename = $get_image['user_photo'];
        }
        //gender
        if(isset($profile_data['gender'])){
            if($profile_data['gender'] == 'null' || $profile_data['gender'] == ''){
                $gender = null;
            }
            else{
                $gender = $profile_data['gender'];
            }            
        }
        else{
            $gender = UserMaster::select('gender')->where('user_id',$profile_data['user_id'])->first();
            $gender = $gender['gender'];
        }
        //get profession id
        if(isset($profile_data['profession'])){
            $profession = Profession::select('id')->where('name','like',"%%{$profile_data['profession']}%%")->first();
            if($profession['id'] == null){
                $get_profession = 31;
            }
            else{
                $get_profession = $profession['id'];
            }
        }
        else{
            $profession = UserMaster::select('profession')->where('user_id',$profile_data['user_id'])->first();
            $get_profession = $profession['profession'];
            //dd($get_profession);
        }
        //location
        if(isset($profile_data['location'])){
            $location = $profile_data['location'];
        }
        else{
            $location = UserMaster::select('location')->where('user_id',$profile_data['user_id'])->first();
            $location = $location['location'];
        }
        //email
        if(isset($profile_data['email'])){
            $email = $profile_data['email'];
        }
        else{
            $email = UserMaster::select('email_id')->where('user_id',$profile_data['user_id'])->first();
            $email = $email['email_id'];
        }
        //birthdate
        if(isset($profile_data['birth_date'])){
            $birth_date = $profile_data['birth_date'];
        }
        else{
            $birth_date = UserMaster::select('birth_date')->where('user_id',$profile_data['user_id'])->first();
            $birth_date = $birth_date['birth_date'];
        }
        //brief_desc
        if(isset($profile_data['brief_desc'])){
            $brief_desc = $profile_data['brief_desc'];
        }
        else{
            $brief_desc = UserMaster::select('brief_desc')->where('user_id',$profile_data['user_id'])->first();
            $brief_desc = $brief_desc['brief_desc'];
        }
        // echo "<pre>";
        // print_r(strtotime($birth_date));
        // exit();
    //     $time_input = strtotime($birth_date);
    // echo "<br>";
    // //   print_r($time_input);
    // //   exit();  
    // $date_input = getDate($time_input); 
    //     echo "<pre>";
    //     print_r($date_input);
    //     exit();
        //profile data update
        $user_count = UserMaster::where('user_id',$profile_data['user_id'])->where('is_active','Y')->where('is_deleted','N')->where('is_block','N')->count();
        if($user_count > 0)
        {
            $data = array(
                'fullname'=>$fullname,
                'user_photo'=>$filename,
                'gender'=>$gender,
                'profession'=>$get_profession,
                'location'=>$location,
                'email_id'=>$email,
                'birth_date'=>$birth_date,
                // 'birth_date'=>strtotime($birth_date),
                'brief_desc'=>$brief_desc
            );

            // echo "<pre>";
            // print_r($data);
            // exit();
            $user_update = UserMaster::where('user_id',$profile_data['user_id'])->update($data);

            $user_data = UserMaster::where('user_id',$profile_data['user_id'])->where('is_active','Y')->get()->toArray();
            // $user = [];
            foreach ($user_data as $key => $value) {
                // dd($value);
                $profession_name = Profession::select('name')->where('id',$value['profession'])->first();
                $value['profession'] = $profession_name['name'];
                $user = $value; 
            }
            //$success['user_data'] = $user;            
            return $user;
        }
    }
}
