<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;;
use Illuminate\Http\Request;
use App\Models\UserMaster;

class NotificationSettingJob
{
    
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $notification_setting;
    public function __construct($notification_setting)
    {
        $this->notification_setting = $notification_setting;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {   
        $notification_setting = $this->notification_setting;
        $user_data = UserMaster::firstOrCreate(['user_id'=>$notification_setting['user_id']]);
        
        $user_data->fill($notification_setting);
        
        $user_data->save();
        return;
    }
}
