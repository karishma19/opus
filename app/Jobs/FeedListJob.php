<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserFriend;

use App\Models\GroupMaster;
use App\Models\PostLike;

use App\Models\UserComment;

use App\Models\UserMaster;

use App\Models\Tag;

use App\Models\UserTag;

use App\Models\UserRating;

use App\Helpers\UserPostHelp;

use App\Helpers\UserCommentHelp;

use App\Helpers\PostLikeHelp;

use App\Helpers\SharePostHelp;

use App\Helpers\UserExist;

use App\Helpers\UserTagHelp;

use App\Helpers\MostVisitedTagHelp;

use App\Helpers\UserRateHelp;

use DB;



class FeedListJob

{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $feed_data;

    public function __construct($feed_data)

    {

        $this->feed_data = $feed_data;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

        $feed_data = $this->feed_data;
        
        $user_id = $feed_data['user_id'];

        $pageNo = $feed_data['pageNo'];

        $pageCount = $feed_data['pageCount'];

        $friends_id_array = array();

        $suggested_profile_array = array();

        $posts_array =array();
        $total_rate=0;
        

            $friends_data = UserFriend::select('user_friends.*','user_master.user_id','user_master.fullname','user_master.location','user_master.online_status','user_master.user_photo','group_master.group_title')

                                            ->leftJoin('user_master','user_friends.friend_id', '=', 'user_master.user_id')

                                            ->leftJoin('group_master','user_friends.group_id', '=', 'group_master.group_id')

                                            ->where('user_friends.user_id',$user_id)

                                            ->where('user_master.is_deleted','N')

                                            ->where('user_master.is_active','Y')

                                            ->orderBy('user_master.fullname','ASC')

                                            ->get()

                                            ->toArray();

            if(!empty($friends_data)){

                foreach ($friends_data as $friend_data){

                    array_push($friends_id_array,$friend_data['friend_id']);

                }

            }


            $friends_id_array[sizeof($friends_id_array)] = $user_id;

            $is_data = UserPostHelp::getPostsWithPagecount($friends_id_array,1,1,'Y');
            
            
            if(!empty($is_data) && $is_data > 0){

                $posts_data = UserPostHelp::getPostsWithPagecount($friends_id_array,$pageCount,$pageNo,'N',$user_id);

            }else{

                $posts_data = UserPostHelp::getPostsWithPagecount(0,$pageCount,$pageNo,'N',$user_id);

            }
 
            if(!empty($posts_data)){

                $i = 0;

                foreach ($posts_data as $posts){

                    

                    $latest_comment_array = array();

                    $post_id = $posts['post_id'];   



                    $comment_count = UserCommentHelp::fetchCountCommentbyPostIdWithoutUserData($post_id);

                   

                    $latest_comment = UserCommentHelp::fetchLatestCommentsWithPostId($post_id);



                    $postlikebySameUser = PostLikeHelp::checkPostlikebyuser($post_id,$user_id);
                    

                    $postlikebyuser = PostLike::where('post_like.post_id', $post_id)      

                                    ->count();
                    

                    

                    $share_post_user_data  = SharePostHelp::getSharedPostByWithUserData($post_id,$user_id);
                    // echo "<pre>";
                    // print_r($share_post_user_data);
                    // exit();
                    

                    // latest_comment

                    if(!empty($latest_comment)){

                        $latestComment = 0;

                        foreach($latest_comment as $ltcomment){

                            $latest_comment_array[$latestComment]['user_id'] = $ltcomment['i_by'];

                            $latest_comment_array[$latestComment]['user_photo'] = $ltcomment['user_photo'];

                            $latest_comment_array[$latestComment]['user_fullname'] = $ltcomment['fullname'];

                            $latest_comment_array[$latestComment]['comment'] = $ltcomment['comment']; 

                            $latestComment++;

                        }                                      



                    }



                        $cur_date=date('Y-m-d H:i:s');

                        $post_date=date('Y-m-d H:i:s',$posts['i_date']);

                        $time1=strtotime($cur_date);

                        $time2=strtotime($post_date);

                        $diff= UserExist::dateDifference($time1,$time2,1);

                        

                        if($time1>$time2){

                            $time_ago=$diff." ago";

                        }else{

                            $time_ago='just now';

                        }

                        

                        $posts_array[$i]['user_id']=$posts['user_id'];

                        $posts_array[$i]['post_id']=$posts['post_id'];

                        $posts_array[$i]['media_type']=$posts['post_type'];

                        $posts_array[$i]['fullname']=$posts['fullname'];

                        $posts_array[$i]['user_photo']=$posts['user_photo'];

                        if(!empty($share_post_user_data) && $share_post_user_data != ''){

                            $share_post_user_data = head($share_post_user_data);

                            $posts_array[$i]['share_post_user_id']=$share_post_user_data['user_id'];

                            $posts_array[$i]['share_post_user_fullname']=$share_post_user_data['fullname'];

                        }

                        $posts_array[$i]['time']=$time_ago;

                        $posts_array[$i]['description']=$posts['message'];

                        $posts_array[$i]['post_caption']=$posts['post_caption'];

                        if($posts['post_type'] == 2 || $posts['post_type'] == 3){

                            $posts_array[$i]['post_photo'] =  $posts['post_media'];

                        }if($posts['post_type'] == 3){

                            $string = $this->str_replace_last('.jpg','',$posts['post_media']);                                    

                            $posts_array[$i]['video_name'] =  $string;

                        }

                        $posts_array[$i]['user_id']=$posts['user_id'];

                        $posts_array[$i]['is_liked']=$postlikebySameUser;

                        $posts_array[$i]['comment_count']=$comment_count;

                        $posts_array[$i]['latest_comment']=$latest_comment_array;

                        $posts_array[$i]['like_count']=$postlikebyuser;

                        $i++;                      

                }

            }

            

            // suggested Profiles Code

            $usertags = UserTag::select('user_tags.*','tags.name')

                                    ->where('user_tags.user_id',$user_id)
                                    ->leftJoin('tags', 'tags.id', '=', 'user_tags.tag_id')


                                    ->get()

                                    ->toArray();

            
            $tags =array();

            $tags_id =array();
           
            foreach ($usertags as$key => $usertag){

                array_push($tags, $usertag['name']);

                array_push($tags_id, $usertag['tag_id']);

            }
            

            $suggestedProfile_data_array = array();
            if(!empty($tags)){

                $users_no = 0;

                foreach ($tags as $most_vstd_tag) { 

                    $suggestedProfiles_data = MostVisitedTagHelp::getUserofMostVisitedTag($most_vstd_tag,$pageCount,$pageNo,'suggestedProfile',$user_id);


                    
                    if(!empty($suggestedProfiles_data)){

                        foreach ($suggestedProfiles_data as $suggested_profile){ 

                            $suggested_user_tags = UserTag::select('user_tags.*','tags.name')

                                                                ->leftJoin('tags', 'tags.id', '=', 'user_tags.tag_id')

                                                                ->where('user_tags.user_id',$suggested_profile['user_id'])

                                                                ->get()

                                                                ->toArray();

                            $suggested_user_tags_array =array();

                            if(!empty($suggested_user_tags)){

                                foreach ($suggested_user_tags as $suggested_user_tag){

                                    array_push($suggested_user_tags_array, $suggested_user_tag['name']);

                                }                                

                            }

                            $total_user_give_rate = UserRateHelp::getAllRatingsforMobile($suggested_profile['user_id']);


                           
                            

                            $total_rate_count = UserRateHelp::gettotalRatingsforMobile($suggested_profile['user_id']);

                            if(!empty($total_rate_count)){

                                foreach ($total_rate_count as $count){

                                    $total_rate +=  $count['rate'];

                                }

                            }else{

                                $total_rate = 0;

                            }

                            if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                $avg= round(($total_rate/$total_user_give_rate),2);

                            }else{

                                $avg = 0;

                            }

                            $suggested_profile_ary = array();

                            $suggested_profile_ary['user_id']=$suggested_profile['user_id'];

                            $suggested_profile_ary['fullname']=$suggested_profile['fullname'];

                            $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];

                            $suggested_profile_ary['tag']=$suggested_profile['keyword'];

                            $suggested_profile_ary['profession']=$suggested_profile['profession'];

                            $suggested_profile_ary['location']=$suggested_profile['location'];

                            $suggested_profile_ary['user_tags']=$suggested_user_tags_array;

                            $suggested_profile_ary['average_rating']=$avg;

                            $suggested_profile_ary['user_give_rating_count']=$total_user_give_rate;

                            $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;

                            $users_no++;

                        }

                    }                    

                }

            }

            // suggestedProfile_data_array

            $suggestedProfile_data_array = array();

            $alreadyPushedUser = array();
            
            if(!empty($tags)){

                $users_no = 0;                          
               
                foreach ($tags_id as $most_vstd_tag) {                                     
                   
                    // $suggestedProfiles_data = UserCommentHelp::getUserFromTag($most_vstd_tag,$pageCount,$pageNo,$user_id);
                    $suggestedProfiles_data = UserCommentHelp::getUserFromTag($most_vstd_tag,$pageNo,$pageCount,$user_id);
                   
                    if(!empty($suggestedProfiles_data)){

                        

                        foreach ($suggestedProfiles_data as $key => $suggested_profile) {

                            if($suggested_profile['user_id'] != $user_id){

                                if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){

                                    $suggested_user_tags = UserTagHelp::getTags($suggested_profile['user_id']);

                                   

                                    $suggested_user_tags_array =array();

                                    if(!empty($suggested_user_tags)){

                                        foreach ($suggested_user_tags as $suggested_user_tag){

                                            array_push($suggested_user_tags_array, $suggested_user_tag['user_tag']);

                                        }                                

                                    }



                                    $total_user_give_rate = UserRateHelp::getAllRatingsforMobile($suggested_profile['user_id']);


                                    
                                    // total_rate_count

                                    $total_rate_count = UserRateHelp::gettotalRatingsforMobile($suggested_profile['user_id']);

                                    if(!empty($total_rate_count)){

                                        foreach ($total_rate_count as $count){

                                            $total_rate +=  $count['rate'];

                                        }

                                    }else{

                                        $total_rate = 0;

                                    }

                                    if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                        $avg= round(($total_rate/$total_user_give_rate),2);

                                    }else{

                                        $avg = 0;

                                    }

                                    $suggested_profile_ary = array();

                                    $suggested_profile_ary['user_id']=$suggested_profile['user_id'];

                                    $suggested_profile_ary['fullname']=$suggested_profile['fullname'];

                                    $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];

                                    // $suggested_profile_ary['tag']=$suggested_profile['keyword'];

                                    $suggested_profile_ary['profession']=$suggested_profile['profession'];

                                    $suggested_profile_ary['location']=$suggested_profile['location'];

                                    $suggested_profile_ary['user_tags']=$suggested_user_tags_array;

                                    $suggested_profile_ary['average_rating']=$avg;

                                    $suggested_profile_ary['user_give_rating_count']=$total_user_give_rate;

                                    $suggestedProfile_data_array[(int)$users_no] = $suggested_profile_ary;

                                    array_push($alreadyPushedUser,$suggested_profile['user_id']);

                                    $users_no++;

                                } 

                            }else{

                                $users_no = 0;              

                                $suggestedProfiles_data =  UserExist::getUserIfEmptyTag($pageCount,$pageNo,$user_id);



                                if(!empty($suggestedProfiles_data)){

                                    foreach ($suggestedProfiles_data as $suggested_profile){

                                        if($suggested_profile['user_id'] != $user_id){

                                            if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){

                                                $suggested_user_tags = UserTagHelp::getTags($suggested_profile['user_id']);

                                   

                                                $suggested_user_tags_array =array();

                                                if(!empty($suggested_user_tags)){

                                                    foreach ($suggested_user_tags as $suggested_user_tag){

                                                        array_push($suggested_user_tags_array, $suggested_user_tag['user_tag']);

                                                    }                                

                                                }



                                                $total_user_give_rate = UserRateHelp::getAllRatingsforMobile($suggested_profile['user_id']);


                                                // total_rate_count

                                                $total_rate_count = UserRateHelp::gettotalRatingsforMobile($suggested_profile['user_id']);

                                                if(!empty($total_rate_count)){

                                                    foreach ($total_rate_count as $count){

                                                        $total_rate +=  $count['rate'];

                                                    }

                                                }else{

                                                    $total_rate = 0;

                                                }

                                                if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                                    $avg= round(($total_rate/$total_user_give_rate),2);

                                                }else{

                                                    $avg = 0;

                                                }

                                                $suggested_profile_ary = array();

                                                $suggested_profile_ary['user_id']=$suggested_profile['user_id'];

                                                $suggested_profile_ary['fullname']=$suggested_profile['fullname'];

                                                $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];

                                                // $suggested_profile_ary['tag']=$suggested_profile['keyword'];

                                                $suggested_profile_ary['profession']=$suggested_profile['profession'];

                                                $suggested_profile_ary['location']=$suggested_profile['location'];

                                                $suggested_profile_ary['user_tags']=$suggested_user_tags_array;

                                                $suggested_profile_ary['average_rating']=$avg;

                                                $suggested_profile_ary['user_give_rating_count']=$total_user_give_rate;

                                                $suggestedProfile_data_array[(int)$users_no] = $suggested_profile_ary;

                                                array_push($alreadyPushedUser,$suggested_profile['user_id']);

                                                $users_no++;

                                            }

                                        }

                                    }

                                }

                            }                           

                        }

                    }

                }                  

            }else{

                $users_no = 0;              

                $suggestedProfiles_data =  UserExist::getUserIfEmptyTag($pageCount,$pageNo,$user_id);
                 
                if(!empty($suggestedProfiles_data)){

                    foreach ($suggestedProfiles_data as $suggested_profile){

                        if($suggested_profile['user_id'] != $user_id){

                            if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){

                                $suggested_user_tags = UserTagHelp::getTags($suggested_profile['user_id']);
                               
                                $suggested_user_tags_array =array();

                                if(!empty($suggested_user_tags)){

                                    foreach ($suggested_user_tags as $suggested_user_tag){

                                        array_push($suggested_user_tags_array, $suggested_user_tag['user_tag']);

                                    }                                

                                }

                                $total_user_give_rate = UserRateHelp::getAllRatingsforMobile($suggested_profile['user_id']);
                       // total_rate_count

                                $total_rate_count = UserRateHelp::gettotalRatingsforMobile($suggested_profile['user_id']);

                                if(!empty($total_rate_count)){

                                    foreach ($total_rate_count as $count){

                                        $total_rate +=  $count['rate'];

                                    }

                                }else{

                                    $total_rate = 0;

                                }

                                if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                    $avg= round(($total_rate/$total_user_give_rate),2);

                                }else{

                                    $avg = 0;

                                }

                                $suggested_profile_ary = array();

                                $suggested_profile_ary['user_id']=$suggested_profile['user_id'];

                                $suggested_profile_ary['fullname']=$suggested_profile['fullname'];

                                $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];

                                // $suggested_profile_ary['tag']=$suggested_profile['keyword'];

                                $suggested_profile_ary['profession']=$suggested_profile['profession'];

                                $suggested_profile_ary['location']=$suggested_profile['location'];

                                $suggested_profile_ary['user_tags']=$suggested_user_tags_array;

                                $suggested_profile_ary['average_rating']=$avg;

                                $suggested_profile_ary['user_give_rating_count']=$total_user_give_rate;

                                $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;

                                array_push($alreadyPushedUser,$suggested_profile['user_id']);

                                $users_no++;

                            }

                        }

                    }

                }

            }

            


            if(isset($pageCount) && isset($pageNo) && !empty($suggestedProfile_data_array)){

                $suggested_profile_array = array_slice($suggestedProfile_data_array,$pageNo*$pageCount-$pageCount,$pageCount);

            }

            $status = true;

            $msg = "Success";

            $feed_list_data = ['Feel_list'=>$posts_array,'suggested_profile_array'=>$suggested_profile_array,'status'=>$status,'msg'=>$msg];

            return $feed_list_data;

    }

    

    // string replace

    public function str_replace_last( $search , $replace , $str ) {

        if( ( $pos = strrpos( $str , $search ) ) !== false ) {

            $search_length  = strlen( $search );

            $str    = substr_replace( $str , $replace , $pos , $search_length );

        }

        return $str;

    }

}

