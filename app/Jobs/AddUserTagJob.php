<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\UserMaster;
use App\Models\Tag;
use App\Models\UserTag;
use Illuminate\Http\Request;

class AddUserTagJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tags;
    public function __construct($tags)
    {
        $this->tags = $tags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $tags =  $this->tags;
        
        $user_id = $tags['user_id'];
        $tag = $tags['tag'];
        
        if(!empty($tag)){
            $i_date = strtotime('now');

            $data = ['name'=>$tag,'is_active'=>1,'i_date'=>$i_date,'u_date'=>$i_date,'default_tag'=>'0'];
            
            $tag_save = Tag::create($data);
            $tag_id = $tag_save->id;
            // if($tag_id){
            //     $user_tag = UserTag::create(['user_id'=>$user_id,'tag_id'=>$tag_id,'likes'=>0,'i_by'=>$user_id,'i_date'=>$i_date,'u_by'=>$user_id,'u_date'=>$i_date]);
            // }
            return $tag_id;
        }
    }
}
