<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserPost;
use App\Models\PostLike;
use App\Models\UserMaster;
use App\Models\NotificationTable;
use App\Jobs\sendNotificationJob;



class GetLikeJob

{



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $like_details;

    public function __construct($like_details)

    {

        $this->like_details = $like_details;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {
        $like_details = $this->like_details;

        $user_id = $like_details['userID'];
        $post_id=$like_details['postID'];
        $type=$like_details['type'];
        
        if($type=='like'){
            $save_detail = New PostLike();
            $save_detail->post_id = $post_id;
            $save_detail->user_id = $user_id;
            $save_detail->i_date = time();
            $save_detail->save();
            
            $postdata = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')
                                ->leftjoin('user_master','user_master.user_id','=','user_post.user_id')
                                ->where('user_post.post_id',$post_id)
                                ->where('user_post.is_deleted','N')
                                ->get()
                                ->toArray();
                            
            $postdata = head($postdata);
            

            if($user_id != $postdata['user_id']){
                $notification_type = config('Constant.like');
                $notification_detail = new NotificationTable();
                $notification_detail->notify_id = $post_id;
                $notification_detail->notify_by = $user_id;
                $notification_detail->notify_to = $postdata['user_id'];
                $notification_detail->type = $notification_type;
                $notification_detail->i_time = time();
                
                $notification_detail->save();

                $fullname = UserMaster::where('user_id',$user_id)->first();
                $token = UserMaster::where('user_id',$postdata['user_id'])->first();
                if(!empty($fullname))
                {
                    $device_token = $token['device_token'];
                    $message = $fullname['fullname'];
                }else{ 
                    $device_token='';
                    $message='';
                }   
                $device_token = $device_token;
                // $message = $message." like your post";
                $message = $message." has liked your post";
                    

                $title = "post,".$postdata['post_id'];
                
                $is_notify = UserMaster::where('user_id',$postdata['user_id'])->value('is_notify');
       
                $notificationCount ='';
                if($is_notify == 'Y'){
                    $notificationCount = NotificationTable::where('notify_to', $postdata['user_id'])

                                                                ->where('is_read', 'N')

                                                                ->count();
                }

                $send_notification = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$post_id,$title,$notificationCount));
            }
        }else{

            $like_delete = PostLike::where('user_id', $user_id)
                                        ->where('post_id',$post_id)
                                        ->delete();
        }
        return;
    }

}

