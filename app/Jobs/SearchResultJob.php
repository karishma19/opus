<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Helpers\SearchResultHelp;
use App\Models\UserAbuse;
use App\Models\UserRating;
use App\Models\UserTag;
use App\Models\Profession;
use App\Models\UserMaster;
use App\Models\UserSearch;
use DB;

class SearchResultJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$page_count,$page_no,$tag)
    {
        $this->user_id = $user_id;
        $this->page_count = $page_count;
        $this->page_no = $page_no;
        $this->tag = $tag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $user_id = $this->user_id;
        $page_count = $this->page_count;
        $page_no = $this->page_no;
        $tag = $this->tag;
        $keyword = $tag;
        $location = '';
        
        if (strpos($tag, ' in ') !== false){
            
            $tagdata = explode(' in ', $tag);
            
            $tag = $tagdata[0];
            $location = $tagdata[1];
        }
        if (strpos($tag, ' from ') !== false){
            $tagdata = explode(' from ', $tag);
            $tag = $tagdata[0];
            $location = $tagdata[1];
        }
        
        // get users by tags start
        $tags_uers = SearchResultHelp::getUsersFromTags($tag, null, null, $user_id);
        // dd($tags_uers);
        $tag_userids = [];
        if(!empty($tags_uers)){
            foreach ($tags_uers as $tag_user_key => $tag_user_value) {
                $tag_userids[$tag_user_key] = $tag_user_value['user_id'];
            }
        }
        
        $abuse_userids=[];
        if(isset($user_id) && $user_id!=''){
            $abuseusers_resultset = UserAbuse::where('user_id',$user_id)->get()->toArray();
            //dd($abuseusers_resultset);
            $_cnt = 0;
            foreach($abuseusers_resultset as $res){
                //dd($res);
                $abuse_userids[$_cnt] = $res['abuse_by'];
                $_cnt++;
            }
        }

        // get users by name start
        $search_data = SearchResultHelp::getSearchUser($tag, $tag_userids, $abuse_userids, $user_id, $page_count,$page_no,$location);
        
       

        // $merge_data = array_merge($location_data,$search_data);
        
        // $search_data = array_map("unserialize", array_unique(array_map("serialize", $merge_data)));
        // $unique = array_unique($merge_data);
        // echo "<pre>";
        // print_r($unique);
        // exit();
        // dd(count($search_data));
        $total_results = count($search_data);
        // SearchResultHelp::getSearchUserCount($tag, $tag_userids, $abuse_userids, $user_id, $page_count,$page_no);
        //dd($total_results);
        //$total_results = SearchResultHelp::getSearchUserCount($tag, $tag_userids, $abuse_userids, $user_id);
        $suggested_profile_array = [];
        //$total_results = count($search_data);
        if(!empty($search_data)){
            $users_no = 0;
            foreach ($search_data as $suggested_profile){
                $suggested_user_tags = UserTag::select('tags.id','tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                $suggested_user_tags_array = [];
                if(!empty($suggested_user_tags)){
                    foreach ($suggested_user_tags as $suggested_user_tag){
                        array_push($suggested_user_tags_array, $suggested_user_tag['name']);
                    }                                
                }
                // $total_user_give_rate = UserRating::where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->count();
                // $total_rate_count = UserRating::select(\DB::raw('SUM(rate) as rate'))->where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->get()->toArray();
                // if(!empty($total_rate_count)){
                //     foreach ($total_rate_count as $count){
                //         $total_rate=  $count['rate'];
                //     }
                // }else{
                //     $total_rate = 0;
                // }
                // if($total_rate > 0 && !empty($total_rate) && $total_rate != null){
                //     $avg= round(($total_rate/$total_user_give_rate),2);
                // }else{
                //     $avg = 0;
                // }
                $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

                                                ->whereNotNull('rate')
                                                ->groupBy('i_by')
                                                ->get()
                                                ->toArray();
                $rating_count = count($rating_count);
                             

                $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

                                            ->select('*',DB::raw("SUM(rate) as rate"))
                                            ->whereNotNull('rate')
                                            ->groupBy('i_by')

                                            ->get()

                                            ->toArray();

                $total_rate =0;  
                if(!empty($total_rate_count)){

                    foreach ($total_rate_count as $count){

                        $total_rate +=  $count['rate'];

                    }

                }
                $avg=0;
                if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                    $avg= round(($total_rate/$rating_count),2);

                }else{

                    $avg = 0;

                }
                $suggested_profile_ary = []; 
                $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
                $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
                $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
                $suggested_profile_ary['profession']=$suggested_profile['name'];
                $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
                // $suggested_profile_ary['profession']=$suggested_profile['profession'];
                // $suggested_profile_ary['profession_name']=$suggested_profile['profession_name'];
                $suggested_profile_ary['location']=$suggested_profile['location'];
                 $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
                 $suggested_profile_ary['average_rating']=$avg;
                 $suggested_profile_ary['user_give_rating_count']=$rating_count;
        //dd($suggested_profile_ary);
                $suggested_profile_array[$users_no] = $suggested_profile_ary; 
                if($user_id != $suggested_profile['user_id']){
                    $save_detail = New UserSearch();
                    $save_detail->user_id = $user_id;
                    $save_detail->keyword = $keyword;
                    $save_detail->frienduser_id = $suggested_profile['user_id'];
                    $id = $save_detail->save();
                    //dd($id);
                } 
                $users_no++;       
            }
        }
        
        $data = ['search_result' => $suggested_profile_array, 'total_results' => $total_results];
        //dd($data);
        return $data;
    }
}
