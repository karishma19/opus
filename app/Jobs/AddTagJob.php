<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserTag;
use App\Models\UserMaster;
use App\Helpers\UserTagHelp;

class AddTagJob
{
    use Dispatchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $complete_profile_data;
    public function __construct($complete_profile_data)
    {
        $this->complete_profile_data = $complete_profile_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $complete_profile_data = $this->complete_profile_data;
        // echo "<pre>";
        // print_r($complete_profile_data);
        // exit();
        $professions ='';
        $location ='';
        $user_id = $complete_profile_data['userID'];
        if(isset($complete_profile_data['professions']) != ''){
            $professions = $complete_profile_data['professions'];
        }
        if(isset($complete_profile_data['location']) != ''){
            $location = $complete_profile_data['location'];
        }
        // tag save
        if(isset($complete_profile_data['tagID'])){
            $tags = $complete_profile_data['tagID'];
            $tags = explode(',', $tags);
            // user tag save
            foreach ($tags as $key => $tag_value) {
                $tag_data = new UserTag();

                $tag_data->user_id = $user_id;
                $tag_data->tag_id = $tag_value;
                $tag_data->i_by = $user_id;
                $tag_data->u_by = $user_id;
                $tag_data->likes = 0;
                $cur_date=date('Y-m-d H:i:s');
                $tag_data->i_date =strtotime($cur_date);
                $post_date=date('Y-m-d H:i:s',$tag_data['i_date']);
                $tag_data->u_date=strtotime($post_date);
                
                $tag_data->save();            
            }
        
        }
        
        // user proffession and location save

        $user_update = UserMaster::where('user_id',$user_id)->update(['profession'=>$professions,'location'=>$location]);
        

        $user_data = UserMaster::select('user_master.*','profession.name as profession')
                                        ->where('user_id',$user_id)
                                        ->leftJoin('profession','user_master.profession','=','profession.id')
                                        ->get()
                                        ->toArray();
        $user_data = head($user_data);
        if($user_data['birth_date'] == '1970-01-01' || $user_data['birth_date'] == NUll || $user_data['birth_date'] == '0000-00-00'){
            $date = '';
        }else{
            // $date = date('d-m-Y', strtotime($value));
        $user_data['birth_date'] = date('F d,Y', $user_data['birth_date']);
        }
        
        $tags = UserTagHelp::getTags($user_id);
        
        $user_tag =[];
        foreach ($tags as $key => $value) {
            array_push($user_tag, $value['user_tag']);
        }
        
        $data = ['user_data'=>$user_data,'user_tag'=>$user_tag];
        return $data;
    }
}
