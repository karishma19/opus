<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserRating;

use App\Models\UserFriend;

use DB;

use App\Helpers\FriendsHelp;



class UserProfileJob

{

    

    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $user_detail;

    public function __construct($user_detail)

    {

        $this->user_detail = $user_detail;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

        $user_detail = $this->user_detail;

        $user_data = $user_detail['user_data'];

        $user_id = $user_detail['user_id'];

        $touser_id = $user_detail['touser_id'];
        $total_rate = 0;


        $output_data = array();

        $user_data = head($user_data);



        $rating_count = UserRating::where('user_ratings.user_id',$user_id)

                                                ->whereNotNull('rate')
                                                ->groupBy('i_by')
                                                ->get()
                                                ->toArray();
        $rating_count = count($rating_count);
                                             

        $total_rate_count = UserRating::where('user_id',$user_id)

                                    ->select('*',DB::raw("SUM(rate) as rate"))
                                    ->whereNotNull('rate')
                                    ->groupBy('i_by')

                                    ->get()

                                    ->toArray();

                
        if(!empty($total_rate_count)){

            foreach ($total_rate_count as $count){

                $total_rate +=  $count['rate'];

            }

        }else{

            $total_rate = 0;

        }
        
        if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

            $avg= round(($total_rate/$rating_count),2);

        }else{

            $avg = 0;

        }

        if($touser_id > 0){

            $friend_id = $touser_id;

            $checkIsFriend = FriendsHelp::checkIsFriend($user_id,$friend_id);

        }

        if($user_data['is_block'] == 'Y'){$is_block = true;}else{$is_block = false;}

        $output_data = array(

            'user_image'=>$user_data['user_photo'],

            'user_name'=>$user_data['fullname'],

            'user_rating'=>$rating_count,

            'is_blocked'=>$is_block,  

            'avg_user_rating'=>$avg,
            
            'firebase'=>$user_data['firebase']


        );

        if($touser_id > 0){

            $output_data['is_friend'] = $checkIsFriend;

        }

    return $output_data;

    }

}

