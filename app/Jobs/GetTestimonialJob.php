<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserMaster;

use App\Models\UserTestimonial;



class GetTestimonialJob 

{

   // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $testimonials_details;

    public function __construct($testimonials_details)

    {

        $this->testimonials_details = $testimonials_details;

    }



    /**

     * Execute the job.

     *

     * @return void  2837300085

     */    

    public function handle(Request $request)

    {

        $testimonials_details = $this->testimonials_details;

        $user_id = $testimonials_details['user_id'];
        $pageCount = $testimonials_details['pageCount'];
        $pageNo = $testimonials_details['pageNo'];
        if(isset($pageNo)){   
            $pageNo = (($pageNo-1)*$pageCount);   
        }
        $testimonials=[];
        $testimonials_data = UserTestimonial::select('user_testimonial.*','user_master.fullname','user_master.user_photo')
                                                    ->leftJoin('user_master', 'user_testimonial.i_by', '=', 'user_master.user_id')
                                                    ->where('user_testimonial.user_id', $user_id)
                                                    ->orderBy('user_testimonial_id','desc')
                                                    ->offset($pageNo)->limit($pageCount)
                                                    ->get()
                                                    ->toArray();
                                                    
                if(!empty($testimonials_data)){

                    $i = 0;

                    foreach ($testimonials_data as $testimonials_Detail){                    

                        $testimonials[$i]['user_id']=$testimonials_Detail['i_by'];

                        $testimonials[$i]['testimonial_id']=$testimonials_Detail['user_testimonial_id'];

                        $testimonials[$i]['fullname']=$testimonials_Detail['fullname'];

                        $testimonials[$i]['user_photo']=$testimonials_Detail['user_photo'];

                        $testimonials[$i]['description']=$testimonials_Detail['testimonial'];

                        $testimonials[$i]['date']=date('F d,Y', $testimonials_Detail['i_date']);

                        $i++;

                    }

                }



         return $testimonials;

        

    }

}

