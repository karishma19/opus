<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserMaster;

use App\Models\GroupMaster;

use App\Models\UserFriend;

use App\Models\NotificationTable;

use App\Helpers\UserExist;



class AddFriendJob

{

    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $friend_data;

    public function __construct($friend_data)

    {

        $this->friend_data =$friend_data;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

        $friend_data = $this->friend_data;

        $user_id = $friend_data['user_id'];

        $friend_id = $friend_data['friend_id'];

        

        $friendgroup = array(   

            'group_id'=>0,

            'user_id'=>$user_id,

            'group_title'=>'friend',

            'flag'=>'add'

        );

        $group_data = GroupMaster::where('user_id',$user_id)->get()->toArray();

        $chk_group = array();

        foreach($group_data as $grp)

        {

            $chk_group[$grp['group_title']] = $grp['group_title'];

        }



        $chk_name='friend';

        $s_chk=$chk_name;



        if (in_array($s_chk, $chk_group)) {

            $gpdata = GroupMaster::where('group_title','friend')

                                        ->orderBy('group_id', 'DESC')

                                        ->limit(1)

                                        ->get()

                                        ->toArray();

            $gpdata =head($gpdata);

            $gpid = $gpdata['group_id'];

        }else{

            $group_master_save = GroupMaster::firstOrNew(['group_id'=>'']);

            $group_master_save->fill($friendgroup);

            $group_master_save->save();

           

            $gpid = $group_master_save->group_id;

        }

        // create add friends

        $data = array(

            'user_id'=>$user_id,

            'friend_id'=>$friend_id,

            'group_id'=>$gpid,

            

            );

        $datafriend = array(

            'user_id'=>$friend_id,

            'friend_id'=>$user_id,

            'group_id'=>$gpid,                    

            );

        

        $addFriend = UserFriend::create($data);

        $datafriend = UserFriend::create($datafriend);

        $notify_id =$datafriend->userfriend_id; 


        $type = config('Constant.AddFriend');
        
        $Notification_data = array('notify_id'=>$notify_id,'notify_by'=>$user_id, 'notify_to'=>$friend_id, 'type'=>$type,'i_time'=>time());

        

        $NotificationTable = NotificationTable::create($Notification_data);

        

        // $from_user_data = UserExist::getUser($user_id);

        // $full_name = $from_user_data['fullname'];



        // $to_user_data = UserExist::getUser($friend_id);

        // $device_token = $to_user_data['device_token'];

        // $message = "$full_name added on you as friend"; 



        // send notification 

        

        if(!empty($notify_id) && is_numeric($notify_id)){                   

            $status = true;

            $msg = "Friend added successfully";
            $user_id = $user_id;
            $friend_id = $friend_id;

        }else{

            $status = false;

            $msg = "Something went wrong while adding friend"; 

        }

        return ['status'=>$status,'msg'=>$msg,'user_id'=>$user_id,'friend_id'=>$friend_id];

    }

}

