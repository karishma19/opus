<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserComment;
use App\Models\UserMaster;
use App\Models\NotificationTable;
use App\Jobs\sendNotificationJob;

class CreateCommentJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($comment_data)
    {
        $this->comment_data = $comment_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $comment_data = $this->comment_data;
        $comment_id = (int)$comment_data['comment_id'];
                //dd($comment_id);
        $i_by = $comment_data['i_by'];
        $user_id = $comment_data['user_id'];
        $post_id = $comment_data['post_id'];
        if ($comment_id == 0) {
            $save_detail = New UserComment();
            $save_detail->post_id = $comment_data['post_id'];
            $save_detail->user_id = $comment_data['user_id'];
            $save_detail->comment = $comment_data['comment'];
            $save_detail->i_by = $i_by;
            $save_detail->i_date = time();
            $save_detail->save();
        }
        $id = $save_detail['id'];
        //dd($id);
        if(!empty($id) && is_numeric($id)){   
            $user_data = UserMaster::select('user_master.user_id','user_master.user_photo','user_master.fullname','user_comment.comment_id','user_comment.comment')->leftjoin('user_comment','user_comment.i_by','=','user_master.user_id')->where('user_comment.comment_id',$id)->where('user_comment.is_deleted','N')->first();
           // dd($user_data);

            if($i_by != $user_id){
                //Send   notification
                $notification_type = config('Constant.comment');
                $save_detail = new NotificationTable();
                $save_detail->notify_id = $post_id;
                $save_detail->notify_by = $i_by;
                $save_detail->notify_to = $user_id;
                $save_detail->type = $notification_type;
                $save_detail->i_time = time();
                $save_detail->save();
                
                $fullname = UserMaster::where('user_id',$i_by)->first();
                $token = UserMaster::where('user_id',$user_id)->first();
                $device_token = $token['device_token'];
                // $message = $fullname['fullname']." comment on your post";
                $message = $fullname['fullname']." has commented to your post";

                $title ="post,$post_id";

                $is_notify = UserMaster::where('user_id',$user_id)->value('is_notify');
       
                $notificationCount ='';
                if($is_notify == 'Y'){
                    $notificationCount = NotificationTable::where('notify_to', $user_id)

                                                                ->where('is_read', 'N')

                                                                ->count();
                }
                
                $send_notification = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$post_id,$title,$notificationCount));
            }
            //$sendPushMessage = $this->sendnotification($device_token,$message,2,$post_id);
        }
           return;
    }

    // public function sendnotification($device_token,$message="Push Notification Message.",$notification_type="",$post_id ="")
    // {
    //     $is_notify = 'N';
    //     $notify = UserMaster::select('is_notify')->where('device_token',$device_token)->where('is_deleted','N')->where('is_active','Y')->first();
    //     $is_notify = $notify['is_notify'];
    //     //dd($is_notify);
        
    //     if($device_token != '' && $is_notify == 'Y')
    //     {              
    //         $previous_badge = 0;        
    //         $new_badge = 0; 
    //         $get_badge = UserMaster::select('badge')->where('device_token',$device_token)->where('is_deleted','N')->where('is_active','Y')->get();       
    //         if (!empty($get_badge['badge'])) { 
    //             $new_badge = $get_badge['badge'] + 1;   
    //             $badge_update = UserMaster::where('device_token',$device_token)->update(['badge' => $new_badge]);
    //         }
    //         /***************Live***************/
    //         //$apnsHost = 'gateway.push.apple.com';
    //         //$apnsCert = 'rta_push.pem';
    //         /***************Live***************/ 
    //         /***************local***************/
    //         $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

    //         $data = [
    //             'message' => $message,
    //             'url'=>$url
    //             'badge' => $new_badge,
    //             'content-available'=>1,
    //             'notification_type'=>$notification_type,
    //             'chat_id'=>$chat_id,
    //             'sender_id'=>$sender_id,
    //             'fullname'=>$sender_full_name,
    //             'image'=>$sender_photo
    //         ];
            
    //         // $extraNotificationData = ["message" => $data];

    //         $fcmNotification = [
    //             'registration_ids' => $device_token, //multple token array
    //             'data' => $data,
    //         ];
    //         // dd($fcmNotification);
    //         $headers = [
    //             'Authorization: key=AAAAsX_28Xc:APA91bGn4h3H_LVZv0sdq9r_7_Ox1N7N_oPuBhOG7rx7izm5r7PFdvQ-tPquSungq9Nza5H_z3t97d0xVLTwyO0ELs_iNsx89ImbMcblEdTyVEvCWrwuhYu5IxOx7Rk8ldyiM-K_s0_C','project_id:762356101495',
    //             'Content-Type: application/json'
    //         ];


    //         $ch = curl_init();
    //         curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    //         curl_setopt($ch, CURLOPT_POST, true);
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    //         $result = curl_exec($ch);
    //         curl_close($ch);
    //         // dd($result);
    //         // return response()->json();
    //         return true;
    //     }
    // }
}
