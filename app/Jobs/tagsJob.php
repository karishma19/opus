<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\UserMaster;
use App\Models\UserTag;

class tagsJob
{
  use Dispatchable;
  protected $userID;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userID)
    {
      $this->userID = $userID;
  }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
     $userID = $this->userID ;
     $tags =[];
     
     $tags_data = UserTag::select('tags.name as user_tag','tags.id as tag_id')
            ->where('user_id',$userID)
            ->leftJoin('tags', 'tags.id', '=', 'user_tags.tag_id')
            ->get()
            ->toArray();

        if(!empty($tags_data)){
          $i = 0;
          foreach ($tags_data as $key => $tag) {
            $tags[$i]['user_tag']=$tag['user_tag'];
            $tags[$i]['tag_id']=$tag['tag_id'];
            $i++;
          }
        }

     return $tags;
 }
}
