<?php

namespace App\Jobs;

use App\Models\UserAbuse;
ini_set('max_execution_time', 50000);



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserPost;

use App\Models\PostLike;

use App\Models\UserComment;

use App\Helpers\UserPostHelp;
use App\Helpers\UserExist;



class postsJob

{

    use Dispatchable;

    protected $userID;

    protected $postID;

    /**

     * Create a new job instance.

     *

     * @return void

     */

    public function __construct($userID)

    {

        $this->userID = $userID;

        // $this->postID = $postID;



    }



    //post_media media_type 

    protected function str_replace_last( $search , $replace , $str ) {

        if( ( $pos = strrpos( $str , $search ) ) !== false ) {

            $search_length  = strlen( $search );

            $str    = substr_replace( $str , $replace , $pos , $search_length );

        }

        return $str;

    }



     /**

     * Execute the job.

     *

     * @return void

     */

     public function handle(Request $request)

     {

        $post_data = $this->userID;

        

        $userID = $post_data['userID'];

        $loginuser_id = $post_data['userID'];

        $pageNo = $post_data['pageNo'];

        $pageCount = $post_data['pageCount'];
        $posts_array =[];
        $i = 0;

         // $user_detail = UserPostHelp::getPostsWithPagecount($userID,$pageCount,$pageNo,'N',$loginuser_id);
        
          $user_id=$userID;

         $user_id_array = $user_id;
        if(!is_array($user_id)){
            $user_id_array = [$user_id];
        }
        $where_str = "1 = ?";
        $where_params = array(1);
        
        // echo "<pre>";
        // print_r($abuse_user);
        // exit();
        $abuse_user = UserAbuse::select('user_id')->where('abuse_by',$loginuser_id)->get()->toArray();
        $abuse_user_data = [];
        if(!empty($abuse_user)){
            foreach ($abuse_user as $abuse_user){
                array_push($abuse_user_data,$abuse_user['user_id']);
            }
        }
       
        if(is_array($user_id)){
            if(sizeof($user_id) >= 0){   
                $user_id =  implode(',',$user_id);
                $where_str .= " AND user_post.user_id IN($user_id)";                
            }
            if(!empty($abuse_user_data)){
                if(sizeof($abuse_user_data) >= 0){   
                    $abuse_user_data =  implode(',',$abuse_user_data);
                    
                    $where_str .= " AND user_post.user_id NOT IN($abuse_user_data)";                
                }
            }    

        }elseif ($user_id == '0') {
            $where_str .= " AND user_post.user_id != '$user_id'";
        }
        else{
            $where_str .= " AND user_post.user_id = '$user_id'";
        }

        // if login user id set

        // $login_user = 106;

        if(isset($loginuser_id)){
                        $where_str .= " AND user_post.post_id NOT IN(select post_id from post_abuse where user_id = $loginuser_id)";

        }
        if(isset($pageNo)){   

            $pageNo = (($pageNo-1)*$pageCount);              

            // $select->offset($pageNo);

        }

        
        $user_detail = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                            ->leftjoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                            ->whereRaw($where_str,$where_params)

                            ->where('user_post.is_deleted', 'N')
                            ->offset($pageNo)->limit($pageCount)
                            ->orderBy('user_post.post_id', 'DESC')

                            ->get()

                            ->toArray();     

         foreach($user_detail as $key=>$value){

           

            $cur_date=date('Y-m-d H:i:s');

            $post_date=date('Y-m-d H:i:s',$value['i_date']);

            $time1=strtotime($cur_date);

            $time2=strtotime($post_date);

            $diff= UserExist::dateDifference($time1,$time2,1);

            if($time1>$time2){

                $time_ago=$diff." ago";

            }else{

                $time_ago='just now';

            }

        // is_liked

            $post_id = $value['post_id'];

            $user_id = $value['user_id'];



            $is_liked = PostLike::where('post_id', $post_id)

            ->where('user_id', $user_id)

            ->count();

            if ($is_liked > 0) {

                $is_liked = 'Y';

            }else{

                $is_liked = 'N';

            }





        // comment_count

            $comment_count = UserComment::where('post_id', $post_id)

                                    ->where('is_deleted','N')

                                    ->count();





            // latest_comment            

            $latest_comment = UserComment::select('user_comment.*','user_master.fullname','user_master.user_photo')->where('post_id', $post_id)

                                            ->join('user_master', 'user_comment.i_by','=', 'user_master.user_id','left')

                                            ->orderBy('comment_id','DESC')

                                            ->limit(1)

                                            ->get()->toArray();







    // // like_count

            $like_count = PostLike::where('post_like.post_id', $post_id)      

                                    ->count();
            
            $posts_array[$i]['user_id']=$value['user_id'];

            $posts_array[$i]['post_id']=$value['post_id'];

            $posts_array[$i]['media_type']=$value['post_type'];

            $posts_array[$i]['fullname']=$value['fullname'];

            $posts_array[$i]['user_photo']=$value['user_photo'];   

            // $posts_array[$key]['time'] = $time_ago;
            $posts_array[$i]['time']=$time_ago;

            $posts_array[$i]['description']=$value['message'];

            $posts_array[$i]['post_caption']=$value['post_caption'];



            if($value['post_type'] == 2 || $value['post_type'] == 3){

                $posts_array[$key]['post_photo'] =  $value['post_media'];

            }if($value['post_type'] == 3){

                $string = $this->str_replace_last('.jpg','',$value['post_type']);

                $posts_array[$key]['video_name'] =  $string;

            }

            

            $posts_array[$key]['is_liked'] = $is_liked;

            $posts_array[$key]['comment_count'] = $comment_count;

            $posts_array[$key]['latest_comment'] = $latest_comment;

            $posts_array[$key]['like_count'] = $like_count;   

            $i++;

        }



        return $posts_array;

    }

}

