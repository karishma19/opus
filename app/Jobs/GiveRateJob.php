<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserRating;

class GiveRateJob
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $rating_detail;

    public function __construct($rating_detail)
    {
        $this->rating_detail = $rating_detail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $rating_detail = $this->rating_detail;
        
        $rating_data = UserRating::firstOrNew(['userrating_id'=>$rating_detail['userrating_id']]);
        $rating_data->fill($rating_detail);
        $rating_data->save();

        return $rating_data->userrating_id;
    }
}
