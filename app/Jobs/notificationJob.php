<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\NotificationTable;
use App\Models\UserMaster;
use App\Models\UserTag;
use App\Helpers\UserExist;

class notificationJob
{
    use Dispatchable;
    protected $userID;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userID)
    {
        $this->userID = $userID;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $userID = $this->userID ;
        
        $notification_type = config('Constant.chatNotification');    
        $notification_data = NotificationTable::select('ss_notification.notification_id','user_master.fullname','user_master.user_photo','ss_notification.type','ss_notification.i_time','ss_notification.notify_by','ss_notification.notify_id')
                    ->leftJoin('user_master', 'user_master.user_id', '=', 'ss_notification.notify_by')
                    ->where('notify_to', $userID)
                    ->whereNotNull('user_master.firebase')
                    ->where('user_master.is_deleted','N')
                    ->where('user_master.is_active','Y')
                    ->where('user_master.is_block','N')
                    ->where('is_clear', 'N')
                    ->where('type', '!=', $notification_type)
                    ->orderBy('i_time', 'DESC')
                    ->get()
                    ->toArray();
        
        $notification_arr=array();
        $i=0;
        if(!empty($notification_data)){
            $status  = true;
            $msg = "Success";
            foreach($notification_data as $key=>$notify){
                $cur_date=date('Y-m-d H:i:s');
                $post_date=date('Y-m-d H:i:s',$notify['i_time']);
                $time1=strtotime($cur_date);
                $time2=strtotime($post_date);

                $diff= UserExist::dateDifference($time1,$time2,1);

                if($time1>$time2){
                    $time_ago=$diff ." ago";
                }else{
                    $time_ago='just now';
                }  

                $userData = UserExist::getUser($notify['notify_by']);
        
                $notification_arr[$i]['user_id']=$notify['notify_by'];
                $notification_arr[$i]['user_name']=$userData['fullname'];
                $notification_arr[$i]['user_image']=$userData['user_photo'];
                if($notify['type'] == '1' || $notify['type'] == '2' || $notify['type'] == '8'){
                    $notification_arr[$i]['post_id']=$notify['notify_id'];
                }
                $notification_arr[$i]['notification_type']=$notify['type'];
                $notification_arr[$i]['notification_date']=$time_ago;
                $i++;
            }
            $makeAllNotificationAsRead = NotificationTable::where('is_read','N')
                                                            ->where('notify_to',$userID)
                                                            ->update(['is_read'=>'Y']);
        }else{
            $status = false;
            $msg = "notification not found";
        }
        $output =['notification_arr'=>$notification_arr,'status'=>$status,'msg'=>$msg];
        return $output;
    }
}
