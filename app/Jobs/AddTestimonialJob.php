<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserTestimonial;
use App\Models\UserMaster;
use App\Models\NotificationTable;

class AddTestimonialJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($testimonial_data)
    {
        $this->testimonial_data = $testimonial_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $testimonial_data = $this->testimonial_data;
        //dd($testimonial_data);
        $user_testimonial_id = (int)$testimonial_data['user_testimonial_id'];
        $i_by = $testimonial_data['i_by'];
        $user_id = $testimonial_data['user_id'];
        if ($user_testimonial_id == 0) {
            $save_detail = New UserTestimonial();
            $save_detail->user_testimonial_id = $testimonial_data['user_testimonial_id'];
            $save_detail->user_id = $testimonial_data['user_id'];
            $save_detail->testimonial = $testimonial_data['testimonial'];
            $save_detail->i_by = $i_by;
            $save_detail->i_date = time();
            $save_detail->save();
        }

        // $notification_type = config('Constant.comment');
        // $fullname = UserMaster::where('user_id',$i_by)->first();
        // //dd($fullname['fullname']);
        // $token = UserMaster::where('user_id',$user_id)->first();
        // $device_token = $token['device_token'];
        // //dd($device_token);
        // $post_id='';
        // $message = $fullname['fullname']." wrote testimonial for you";
        
        // $send_notification = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$post_id));
        //$sendPushMessage = $this->sendnotification($device_token,$message,2,$post_id);
        return;
    }
}
