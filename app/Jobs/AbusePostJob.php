<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\PostAbuse;



class AbusePostJob

{

   // use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**

     * Create a new job instance.

     *

     * @return void

     */

    use Dispatchable;

    public $data;

    public function __construct($data)

    {

        $this->data = $data;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

          $data = $this->data; 

          $res_data =[];

         

         $user_id = $data['UserID'];   

         $post_id = $data['postID'];

         

         $alreadyAbuse = PostAbuse::where('user_id',$user_id)

                        ->where('post_id',$post_id)   

                        ->count(); 
        $abuse_count ='N';
        if($alreadyAbuse >0){
            $abuse_count ='Y';
        }
          if($abuse_count == 'N'){

            $postabuseData = array(

                'postabuse_id'=>0,

                'user_id'       => $user_id,

                'post_id'       => $post_id,

                'flag'          => 'add'

            );



            if($postabuseData['flag'] == 'add'){

                $data = array(

                    'postabuse_id'=>0,

                    'post_id'=>$postabuseData['post_id'],

                    'user_id'=>$postabuseData['user_id']

                    );

            }

           

           $postAbuse_save = PostAbuse::create($data);

              

            $status = 0;

            $msg = "Success";

        }else{

            $status = 1;

            $msg = "Already Post is Abuse by same User";

        }

        $res_data = ['status'=>$status,'msg'=>$msg];



           return $res_data;

    }

}

