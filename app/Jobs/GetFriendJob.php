<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserFriend;

use App\Models\UserMaster;



class GetFriendJob

{



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $friends_detail;

    public function __construct($friends_detail)

    {

        $this->friends_detail = $friends_detail;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

        $friends_detail = $this->friends_detail;

        

        $user_id = $friends_detail['user_id'];

        $pageNo = $friends_detail['pageNo'];

        $pageCount = $friends_detail['pageCount'];

        $touser_id = $friends_detail['touser_id'];

        $loginuser_id = $friends_detail['loginuser_id'];

        $friends_array =[];



        $friends_data = UserFriend::select('user_master.fullname','user_master.user_photo','user_friends.friend_id','user_master.firebase','user_master.location')->leftjoin('user_master','user_friends.friend_id', '=', 'user_master.user_id')

                                ->where('user_friends.user_id',$user_id)

                                ->where('user_master.is_deleted','N')

                                ->where('user_master.is_active','Y')
                                ->where('user_master.firebase','!=','')

                                ->offset($pageNo)

                                ->limit($pageCount)

                                ->orderBy('user_master.fullname','ASC')

                                ->get()->ToArray();

        if(!empty($friends_data)){

            $i = 0;

            foreach ($friends_data as $friends){

                

                $friend_id =$friends['friend_id'];

                $where_str = "1 = ?";

                $where_params = array(1);



                $where_str ="(user_friends.user_id = '$loginuser_id'";

                $where_str .= " AND user_friends.friend_id = '$friend_id')";

                $where_str .= " OR (user_friends.user_id = '$friend_id' ";

                $where_str .= " AND user_friends.friend_id = '$loginuser_id') ";

                

                $checkIsFriend = UserFriend::whereRaw($where_str,$where_params)->get()->toArray();

                $friend_id = "N";

                if (count($checkIsFriend)>0) {

                    $friend_id = "Y";

                }

                

                $friends_array[$i]['fullname']=$friends['fullname'];

                $friends_array[$i]['profile_image']=$friends['user_photo'];

                $friends_array[$i]['friend_id']=$friends['friend_id'];
                $friends_array[$i]['firebase']=$friends['firebase'];
                $friends_array[$i]['location']=$friends['location'];

                if($touser_id > 0){

                    $friends_array[$i]['is_friend'] = $friend_id;

                }

                $i++;

            } 

        }

        return $friends_array;

    }

}

