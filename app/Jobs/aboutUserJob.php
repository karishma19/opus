<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserMaster;

use App\Helpers\UserRateHelp;

use App\Helpers\UserExist;



class aboutUserJob 

{

    use Dispatchable;

    protected $userID;



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public function __construct($userID) {

        $this->userID = $userID;

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle()

    {

        $userID = $this->userID ;

        $user_data =[];
        $total_rate =0;


        $user_data = UserExist::getUser($userID);

        $total_user_give_rate = UserRateHelp::getAllRatingsforMobile($userID);

        $total_rate_count = UserRateHelp::gettotalRatingsforMobile($userID);



        if(!empty($total_rate_count)){

            foreach ($total_rate_count as $count){

                $total_rate +=  $count['rate'];

            }

        }
        
        if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

            $avg= round(($total_rate/$total_user_give_rate),2);

        }else{

            $avg = 0;

        }
        if($user_data != '' && !empty($user_data)){
            if($user_data['birth_date'] == '1970-01-01' || $user_data['birth_date'] == NUll || $user_data['birth_date'] == '0000-00-00' || $user_data['birth_date'] == 0){
                    $date = null;
            }else{
                // $user_data['birth_date'] = date('F d,Y', $user_data['birth_date']);
                $user_data['birth_date'] = date("F d,Y", strtotime($user_data['birth_date']));
            }            
        }
        // echo "<pre>";
        // print_r($user_data);
        // exit();
        $rattings_array=array('total_user_give_rate'=>$total_user_give_rate,'total_rate'=>$total_rate,'avg_rate'=>$avg);

        $user_detail=array_merge($user_data,$rattings_array);

        

        return $user_detail;



    }

}

