<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserComment;

use App\Models\UserPost;

use App\Models\UserMaster;
use App\Models\PostLike;

use App\Helpers\PostLikeHelp;

use App\Helpers\UserExist;



class CommentListJob

{

    use Dispatchable;

    protected $commentList_data;





    /**

     * Create a new job instance.

     *

     * @return void

     */

    public function __construct($commentList_data)

    {

        $this->commentList_data = $commentList_data;



    }

    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

        $commentList_data = $this->commentList_data;

        $userID = $commentList_data['userID'];

        $pageNo = $commentList_data['pageNo'];

        $pageCount = $commentList_data['pageCount'];

        $postId = $commentList_data['postId'];

        $comments_array =[];

        $postdata_array =[];

        $where_str = "1 = ?";

        $where_params = array(1);



        if(isset($pageCount)){

            $where_str .= " limit $pageCount";

        }

        if(isset($pageNo)){   

            $pageNo = (($pageNo-1)*$pageCount);  

            $where_str .= " offset $pageNo";

        }

        

        $comments = UserComment::select('user_comment.*','user_master.user_id','user_master.fullname','user_master.user_photo')

                                    ->leftJoin('user_master', 'user_comment.i_by', '=', 'user_master.user_id')

                                    ->where('post_id',$postId)

                                    ->where('user_comment.is_deleted','N')

                                    ->whereRaw($where_str,$where_params)

                                    ->get()

                                    ->toArray();

        

        if(!empty($comments)){

            $i = 0;

            foreach ($comments as $comment){

                $comments_array[$i]['comment_id']=$comment['comment_id'];

                $comments_array[$i]['user_fullname']=$comment['fullname'];

                $comments_array[$i]['user_photo']=$comment['user_photo'];

                $comments_array[$i]['comment']=$comment['comment'];

                $comments_array[$i]['user_id']=$comment['user_id'];

                $i++;

            }

        }



        $postdata = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                                ->leftJoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                                ->where('user_post.post_id',$postId)

                                ->where('user_post.is_deleted','N')

                                ->get()

                                ->toArray();



        $postdata = head($postdata);



        $comment_count = UserComment::where('post_id',$postId)

                                        ->where('is_deleted', "N")

                                        ->count();

        $latest_comment = UserComment::select('user_comment.*','user_master.fullname','user_master.user_photo')

                                            ->leftJoin('user_master', 'user_comment.i_by', '=', 'user_master.user_id')

                                            ->where('post_id',$postId)

                                            ->orderBy('comment_id','DESC')

                                            ->limit(1)

                                            ->get()

                                            ->toArray();



        $latest_comment =head($latest_comment);



        $postlikebySameUser = PostLikeHelp::checkPostlikebyuser($postId,$userID);

        // $postlikebyuser = PostLikeHelp::getUsersLikePost($postId);
        $postlikebyuser = PostLike::where('post_like.post_id', $postId)      

                                    ->count();;


        



        $cur_date=date('Y-m-d H:i:s');

        $post_date=date('Y-m-d H:i:s',$postdata['i_date']);

        $time1=strtotime($cur_date);

        $time2=strtotime($post_date);



        $diff = UserExist::dateDifference($time1,$time2,1);

        if($time1>$time2){

            $time_ago=$diff." ago";

        }else{

            $time_ago='just now';

        }

        

        $postdata_array['user_id'] =  $postdata['user_id'];         

        $postdata_array['user_photo'] =  $postdata['user_photo'];

        $postdata_array['fullname'] =  $postdata['fullname'];

        $postdata_array['post_id'] =  $postdata['post_id'];                

        $postdata_array['media_type'] =  $postdata['post_type'];

        if($postdata['post_type'] == 2 || $postdata['post_type'] == 3){

            $postdata_array['post_photo'] =  $postdata['post_media'];

        }if($postdata['post_type'] == 3){

            $string = $this->str_replace_last('.jpg','',$postdata['post_media']);                                 

            $postdata_array['video_name'] =  $string;

        }

        $postdata_array['description'] =  $postdata['message'];

        $postdata_array['post_caption'] =  $postdata['post_caption'];

        $postdata_array['time'] =  $time_ago;

        $postdata_array['is_liked']=$postlikebySameUser;

        $postdata_array['comment_count']=$comment_count;

        $postdata_array['like_count']=$postlikebyuser;

        

        $output = array('comments_list'=>$comments_array,'postdata'=>$postdata_array);

        return $output;

    }

}

