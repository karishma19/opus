<?php



namespace App\Jobs;



use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;

use App\Models\UserPost;

use App\Models\SharePost;

use App\Models\UserMaster;

use App\Models\UserComment;

use App\Models\PostLike;
use App\Models\NotificationTable;

use App\Helpers\FileHelp;

use App\Helpers\UserExist;
use App\Helpers\UserTagHelp;
use App\Jobs\sendNotificationJob;

class SharePostJob 

{

    //use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**

     * Create a new job instance.

     *

     * @return void

     */

    public $data;

    public function __construct($data)

    {

         $this->data = $data;

         

    }



    /**

     * Execute the job.

     *

     * @return void

     */

    public function handle(Request $request)

    {

         $data = $this->data;



         $user_id = $data['userID'];

         $post_id = $data['postID'];

         $shareText = $data['shareText'];



          $sharepostdata = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                                        ->leftJoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                                        ->where('user_post.post_id',$post_id)

                                        ->where('user_post.is_deleted','N')

                                        ->get()

                                        ->toArray(); 

                                       



                     // echo "<pre>";
                     // print_r($sharepostdata);
                     // exit();

                     $sharepostdata = head($sharepostdata); 

                        $postdata = array(

                            'post_id'=>0,   

                            'user_id'=>$user_id,

                            'post_caption'=> $sharepostdata['post_caption'],

                            'message'=> $sharepostdata['message'],

                            'post_media'  => $sharepostdata['post_media'],

                            'post_type'  => $sharepostdata['post_type'],

                            'likes'=>'0',

                            'i_by'=>$user_id,

                            'i_date'=>time(),

                            'u_by'=>$user_id,

                            'u_date'=>time(),

                            'user_photo'=> $sharepostdata['user_photo'],

                            'fullname'=> $sharepostdata['fullname'],

                            'flag'=>'add'

                        );  



                    // share post

                    $post_save = UserPost::create($postdata)->toArray();

                   

                    $postId = $post_save['post_id'];
                    // $postId = $post_id;

                            

                        $share_post_data = array(

                            'user_id'=>$sharepostdata['user_id'],

                            'post_id'=> $postId,

                            'share_text'=>$shareText,

                            'type'=>1,

                            'is_share'=>"Y",

                            'i_by'=>$user_id,

                            'i_date'=>time(),

                            'u_by'=>$user_id ,

                            'u_date'=>time()

                        );

                    $post_save = SharePost::create($share_post_data)->toArray();  

                   
                // getPostsWithUserData
                $postdata = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')
                                            ->leftJoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')
                                                ->where('user_post.post_id',$postId)
                                                ->where('user_post.is_deleted','N')
                                                ->get()
                                                ->toArray();
                $postdata = head($postdata);
                
                // fetchCountCommentbyPostIdWithoutUserData

                $comment_count = UserComment::where('post_id',$postId)->where('is_deleted','N')->count();

                

                // fetchLatestCommentsWithPostId

                $latest_comment = UserComment::select('user_master.fullname','user_master.user_photo')

                                                    ->where('post_id',$postId)

                                                    ->leftJoin('user_master', 'user_comment.i_by', '=', 'user_master.user_id')

                                                    ->orderBy('comment_id', 'DESC')

                                                    ->limit(1)

                                                    ->get()

                                                    ->toArray();  

               

              // checkPostlikebyuser

                $postlikebySameUser = PostLike::where('post_id',$postId)

                                                    ->where('user_id',$user_id)

                                                    ->count();

                if($postlikebySameUser > 0){

                        $postlikebySameUser =  'Y';

                    }else{

                        $postlikebySameUser =  'N';

                    }

                    

                // getUsersLikePost

                $postlikebyuser = PostLike::leftJoin('user_master', 'post_like.user_id', '=', 'user_master.user_id')

                                                ->where('post_like.post_id',$postId)

                                                ->get()

                                                ->toArray();                                       

                $cur_date=date('Y-m-d H:i:s');

                $post_date=date('Y-m-d H:i:s',$postdata['i_date']);

                $time1=strtotime($cur_date);

                $time2=strtotime($post_date);

                $diff= UserExist::dateDifference($time1,$time2,1);

                if($time1>$time2){

                    $time_ago=$diff." ago";

                }else{

                    $time_ago='just now';

                }


           
            $postdata_array ['user_id'] =  $postdata['user_id'];         

            $postdata_array ['user_photo'] =  $postdata['user_photo'];

            $postdata_array ['fullname'] =  $postdata['fullname'];

            $postdata_array ['post_id'] =  $postdata['post_id'];
            $postdata_array ['share_post_user_id'] =  $sharepostdata['user_id'];
            $postdata_array ['share_post_user_fullname'] =  $sharepostdata['fullname'];
                     

            $postdata_array ['media_type'] =  $postdata['post_type'];

            if($postdata['post_type'] == 2 || $postdata['post_type'] == 3){

                $postdata_array['post_photo'] =  $postdata['post_media'];

            }if($postdata['post_type'] == 3){

                $string = $this->str_replace_last('.jpg','',$postdata['post_media']);                                 

                $postdata_array['video_name'] =  $string;

            }

            $postdata_array ['description'] =  $postdata['message'];

            $postdata_array ['post_caption'] =  $postdata['post_caption'];

            $postdata_array ['time'] =  $time_ago;

            $postdata_array['is_liked']=$postlikebySameUser;

            $postdata_array['comment_count']=$comment_count;

            $postdata_array['like_count']=sizeof($postlikebyuser);

            if($user_id != $sharepostdata['user_id']){
                // echo "<pre>";
                // print_r('this');
                // exit();
                $notification_type = config('Constant.sharePost');
                // create notification 
                $notification =[
                    'notify_id'=>$postdata_array['post_id'],
                    'notify_by'=>$postdata_array['user_id'],
                    'notify_to'=>$postdata_array['share_post_user_id'], 
                    'type'=>$notification_type,
                    'i_time'=>time()
                ];
                $notification_create = NotificationTable::create($notification);
                // user Data
                
                $from_user_data = UserExist::getUser($postdata_array['user_id']);
                $full_name='';
                $message ='';
                $device_token ='';
                if(!empty($from_user_data['fullname'])){
                    $full_name = $from_user_data['fullname']; 
                    // $message = "$full_name shared your post";
                    $message = $full_name." has shared your post";
                }
                $to_user_data = UserExist::getUser($postdata_array['share_post_user_id']);
                
                if(!empty($to_user_data['device_token'])){
                    $device_token = $to_user_data['device_token'];
                }
                $title ="post,".$postdata['post_id'];

                $is_notify = UserMaster::where('user_id',$postdata_array['share_post_user_id'])->value('is_notify');
       
                $notificationCount ='';
                if($is_notify == 'Y'){
                    $notificationCount = NotificationTable::where('notify_to', $postdata_array['share_post_user_id'])

                                                                ->where('is_read', 'N')

                                                                ->count();
                }
                $send_notification = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$postdata['post_id'],$title,$notificationCount));
            }

         return $postdata_array;



    }

}

