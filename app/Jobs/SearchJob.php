<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\UserTag;
use App\Models\UserSearch;
use App\Models\UserRating;
use App\Models\UserMaster;
use App\Models\Profession;
use App\Helpers\MostVisitedTagHelp;
use App\Helpers\SearchHelp;
use DB;
class SearchJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$page_count,$page_no,$except)
    {
        $this->user_id = $user_id;
        $this->page_count = $page_count;
        $this->page_no = $page_no;
        $this->except = $except;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $page_count = $this->page_count;
        $page_no = $this->page_no;
        $except = $this->except;
        $suggest_pro =[];

        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }

        $usertags = UserTag::select('tags.id','tags.name')->where('user_tags.user_id',$user_id)
                                    ->leftjoin('tags','tags.id','=','user_tags.tag_id')
                                    ->get()->toArray();
           
        $most_visited_tag_array = [];
        $tags = [];
        foreach ($usertags as $usertag)
        {
            array_push($most_visited_tag_array, $usertag['name']);
            array_push($tags, $usertag['id']);
        }
        $mostvisitedTagUser_data_array = [];
   
        $where_str_out = "1 = ?";
        $where_params_out = array(1);
        
        if(!empty($most_visited_tag_array)){
            $profession_id = UserMaster::where('user_id',$user_id)->value('profession');
        $fullname = UserMaster::where('user_id',$user_id)->value('fullname');
       
        $profession = Profession::where('id',$profession_id)->value('name');
            foreach ($most_visited_tag_array as $key => $value) {
                if($key ==0 ){
                    $where_str_out .= " AND ( user_search.keyword like '%". $value ."%'";
                }else{
                    $where_str_out .= " OR user_search.keyword like '%". $value ."%'";
                }                
            }
            $where_str_out .= " OR user_search.keyword like '%". $profession ."%'"; 
            $where_str_out .= " OR user_search.keyword like '%". $fullname ."%'";
            $where_str_out .= ")";
        }
        // echo "<pre>";
        // print_r($where_str_out);
        // exit();
        $result = UserSearch::select('user_master.fullname','user_master.user_photo','user_master.profession','user_master.location','user_master.firebase','user_search.user_id','user_search.keyword','profession.name as profession')
                        ->leftJoin('user_master', 'user_master.user_id', '=', 'user_search.user_id')
                        ->leftjoin('profession','user_master.profession','=','profession.id')
                        ->whereNotNull('user_master.firebase')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_block','N')
                        ->where('user_master.user_id','!=',$user_id)
                        ->whereRaw($where_str_out, $where_params_out)
                        ->orderBy('user_search.id','DESC')
                        ->get()->toArray();
                    // echo "<pre>";
                    // print_r($result);
                    // exit();
            $users_no = 0;
            $serialized = array_map('serialize', $result);
            $unique = array_unique($serialized);
            $data =  array_intersect_key($result, $unique);

            foreach ($data as $mostvisitedTagUser){
                $suggested_profile_ary = [];
                $suggested_profile_ary['user_id']=$mostvisitedTagUser['user_id'];
                $profile_fullname = $mostvisitedTagUser['fullname'];
                if($mostvisitedTagUser['fullname']==null){
                    $profile_fullname = "";
                }
                $suggested_profile_ary['fullname']=$profile_fullname;
                $profile_image = $mostvisitedTagUser['user_photo'];
                if($mostvisitedTagUser['user_photo']==null){
                    $profile_image = "";
                }
                $suggested_profile_ary['profile_image']=$profile_image;
                $suggested_profile_ary['profession']=$mostvisitedTagUser['profession'];
                $suggested_profile_ary['tag'] = $mostvisitedTagUser['keyword'];
                $suggested_profile_ary['firebase']=$mostvisitedTagUser['firebase']; 
                $mostvisitedTagUser_data_array[$users_no] = $suggested_profile_ary;
                $users_no++;
            }

            
        //SUGGESTED_PROFILES
        $suggestedProfile_data_array = [];
        $alreadyPushedUser = [];
        
        if(!empty($tags)){
            // dd('1');
            $users_no = 0;                                
                $suggested_profile_ary = [];
                // echo "<pre>";
                // print_r($tags);
                // exit();
            foreach ($tags as $most_vstd_tag) {
                $suggestedProfiles_data =  $this->getUserFromTag($most_vstd_tag,$page_count,$page_no,$user_id,$except);
                // echo "<pre>";
                // print_r($suggestedProfiles_data);
                // exit();
                // dd($suggestedProfiles_data);
                if(!empty($suggestedProfiles_data)){
                    foreach ($suggestedProfiles_data as $suggested_profile){
                        if($suggested_profile['user_id'] != $user_id){
                            if(!in_array($suggested_profile['user_id'],$alreadyPushedUser))
                            {
                                $suggested_user_tags = UserTag::select('tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                                $suggested_user_tags_array =[];
                                if(!empty($suggested_user_tags)){
                                    foreach ($suggested_user_tags as $suggested_user_tag){
                                        array_push($suggested_user_tags_array, $suggested_user_tag['name']);
                                    }
                                }

                                $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

                                                ->whereNotNull('rate')
                                                ->groupBy('i_by')
                                                ->get()
                                                ->toArray();
                                $rating_count = count($rating_count);
                                             

                                $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

                                                            ->select('*',DB::raw("SUM(rate) as rate"))
                                                            ->whereNotNull('rate')
                                                            ->groupBy('i_by')

                                                            ->get()

                                                            ->toArray();

                                $total_rate =0;  
                                if(!empty($total_rate_count)){

                                    foreach ($total_rate_count as $count){

                                        $total_rate +=  $count['rate'];

                                    }

                                }
                                $avg=0;
                                if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                    $avg= round(($total_rate/$rating_count),2);

                                }else{

                                    $avg = 0;

                                }
                                // $total_user_give_rate = UserRating::where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->count();
                                // $total_rate_count   = UserRating::select(\DB::raw('SUM(rate) as rate'))->where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->get()->toArray();
                                // if(!empty($total_rate_count)){
                                //     foreach ($total_rate_count as $count){
                                //         $total_rate=  $count['rate'];
                                //     }
                                // }else{
                                //     $total_rate = 0;
                                // }
                                // if($total_rate > 0 && !empty($total_rate) && $total_rate != null){
                                //     $avg= round(($total_rate/$total_user_give_rate),2);
                                // }else{
                                //     $avg = 0;
                                // }
                                $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
                                $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
                                $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
                                $suggested_profile_ary['tag']=$suggested_profile['keyword'];
                                $suggested_profile_ary['profession']=$suggested_profile['profession_name'];
                                $suggested_profile_ary['location']=$suggested_profile['location'];
                                $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
                                $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
                                $suggested_profile_ary['average_rating']=$avg;
                                $suggested_profile_ary['user_give_rating_count']=$rating_count;
                                $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;
                                array_push($alreadyPushedUser,$suggested_profile['user_id']);
                                $users_no++;
                                // dd($suggested_profile_ary);
                            }
                        }
                        else{
                            //dd('else');
                            $users_no = 0; 
                            if(isset($page_no) && $page_no != 0){   
                                $pageNo = (($page_no-1)*$page_count);
                            }
                            if(isset($page_count) && $page_count != 0){
                                $pageCount = $page_count;
                            }
                            $suggestedProfiles_data = UserMaster::select('user_master.*','profession.name as profession_name')->orderBy(\DB::raw('RAND()'))->where('user_id','!=',$user_id)->where('user_master.firebase','!=','NULL')->leftjoin('profession','profession.id','=','user_master.profession')->offset($pageNo)->limit($pageCount)->get()->toArray();
                            //dd($suggestedProfiles_data);
                            if(!empty($suggestedProfiles_data)){
                                foreach ($suggestedProfiles_data as $suggested_profile){
                                    if($suggested_profile['user_id'] != $user_id){
                                        if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){
                                            $suggested_user_tags = UserTag::select('tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                                            $suggested_user_tags_array = [];
                                            if(!empty($suggested_user_tags)){
                                                foreach ($suggested_user_tags as $suggested_user_tag){
                                                    array_push($suggested_user_tags_array, $suggested_user_tag['name']);
                                                }                                
                                            }
                                            $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

                                                ->whereNotNull('rate')
                                                ->groupBy('i_by')
                                                ->get()
                                                ->toArray();
                                    $rating_count = count($rating_count);
                                                                         

                                    $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

                                                                ->select('*',DB::raw("SUM(rate) as rate"))
                                                                ->whereNotNull('rate')
                                                                ->groupBy('i_by')

                                                                ->get()

                                                                ->toArray();

                                    $total_rate =0;      
                                    if(!empty($total_rate_count)){

                                        foreach ($total_rate_count as $count){

                                            $total_rate +=  $count['rate'];

                                        }

                                    }
                                    $avg=0;
                                    if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                        $avg= round(($total_rate/$rating_count),2);

                                    }else{

                                        $avg = 0;

                                    }
                                            // $total_user_give_rate = UserRating::where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->count();
                                            // $total_rate_count   = UserRating::select(\DB::raw('SUM(rate) as rate'))->where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->get()->toArray();
                                            // if(!empty($total_rate_count)){
                                            // foreach ($total_rate_count as $count){
                                            //     $total_rate=  $count['rate'];
                                            // }
                                            // }else{
                                            //     $total_rate = 0;
                                            // }
                                            // if($total_rate > 0 && !empty($total_rate) && $total_rate != null){
                                            //     $avg= round(($total_rate/$total_user_give_rate),2);
                                            // }else{
                                            //     $avg = 0;
                                            // }
                                            // $suggested_profile_ary = [];
                                            $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
                                            $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
                                            $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
                                            //$suggested_profile_ary['tag']=$suggested_profile['keyword'];
                                            $suggested_profile_ary['profession']=$suggested_profile['profession_name'];
                                            $suggested_profile_ary['location']=$suggested_profile['location'];
                                            $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
                                            $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
                                            $suggested_profile_ary['average_rating']=$avg;
                                            $suggested_profile_ary['user_give_rating_count']=$rating_count;
                                            $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;
                                            array_push($alreadyPushedUser,$suggested_profile['user_id']);
                                            $users_no++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }  
                // else{
                    
                //     if($page_no == 1)
                //     {
                //         $users_no = 0; 
                //         if(isset($page_no) && $page_no != 0){   
                //             $pageNo = (($page_no-1)*$page_count);
                //         }
                //         if(isset($page_count) && $page_count != 0){
                //             $pageCount = $page_count;
                //         }
                //         $suggestedProfiles_data = UserMaster::select('user_master.*','profession.name as profession_name')->orderBy(\DB::raw('RAND()'))->where('user_id','!=',$user_id)->where('user_master.firebase','!=','NULL')->leftjoin('profession','profession.id','=','user_master.profession')->offset($pageNo)->limit($pageCount)->get()->toArray();
                        
                //         if(!empty($suggestedProfiles_data)){
                //             foreach ($suggestedProfiles_data as $suggested_profile){
                                
                //                 if($suggested_profile['user_id'] != $user_id){
                //                     if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){
                //                         $suggested_user_tags = UserTag::select('tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                //                         $suggested_user_tags_array = [];
                //                         if(!empty($suggested_user_tags)){
                //                             foreach ($suggested_user_tags as $suggested_user_tag){
                //                                 array_push($suggested_user_tags_array, $suggested_user_tag['name']);
                //                             }                                
                //                         }

                //                         $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

                //                                     ->whereNotNull('rate')
                //                                     ->groupBy('i_by')
                //                                     ->get()
                //                                     ->toArray();
                //                     $rating_count = count($rating_count);
                                                 

                //                     $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

                //                                                 ->select('*',DB::raw("SUM(rate) as rate"))
                //                                                 ->whereNotNull('rate')
                //                                                 ->groupBy('i_by')

                //                                                 ->get()

                //                                                 ->toArray();

                //                     $total_rate =0; 
                //                     if(!empty($total_rate_count)){

                //                         foreach ($total_rate_count as $count){

                //                             $total_rate +=  $count['rate'];

                //                         }

                //                     }
                //                     $avg=0;
                //                     if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                //                         $avg= round(($total_rate/$rating_count),2);

                //                     }else{

                //                         $avg = 0;

                //                     }
                //                         $suggested_profile_ary = [];
                //                         $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
                //                         $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
                //                         $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
                //                         $suggested_profile_ary['tag']=$suggested_profile['search_keyword'];
                //                         $suggested_profile_ary['profession']=$suggested_profile['profession_name'];
                //                         $suggested_profile_ary['location']=$suggested_profile['location'];
                //                         $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
                //                         $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
                //                         $suggested_profile_ary['average_rating']=$avg;
                //                         $suggested_profile_ary['user_give_rating_count']=$rating_count;
                //                         $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;
                //                         array_push($alreadyPushedUser,$suggested_profile['user_id']);
                //                         $users_no++;
                //                     }
                //                 }
                //             }
                //         }
                //     }
                // }              
            }
        }else{
            
            $users_no = 0; 
            if(isset($page_no) && $page_no != 0){   
                $pageNo = (($page_no-1)*$page_count);
            }
            if(isset($page_count) && $page_count != 0){
                $pageCount = $page_count;
            }
            $suggestedProfiles_data = UserMaster::select('user_master.*','profession.name as profession_name')->orderBy(\DB::raw('RAND()'))->where('user_id','!=',$user_id)->where('user_master.firebase','!=','NULL')->leftjoin('profession','profession.id','=','user_master.profession')->offset($pageNo)->limit($pageCount)->get()->toArray();

            
            //dd($suggestedProfiles_data);
            if(!empty($suggestedProfiles_data)){
                foreach ($suggestedProfiles_data as $suggested_profile){
                    
                    if($suggested_profile['user_id'] != $user_id){
                        if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){
                            $suggested_user_tags = UserTag::select('tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                            $suggested_user_tags_array = [];
                            if(!empty($suggested_user_tags)){
                                foreach ($suggested_user_tags as $suggested_user_tag){
                                    array_push($suggested_user_tags_array, $suggested_user_tag['name']);
                                }                                
                            }
                            // $total_user_give_rate = UserRating::where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->count();
                            // //dd($total_user_give_rate);
                            // $total_rate_count   = UserRating::select(\DB::raw('SUM(rate) as rate'))->where('user_id',$suggested_profile['user_id'])->where('rate','!=',null)->groupBy('i_by')->get()->toArray();
                            // if(!empty($total_rate_count)){
                            // foreach ($total_rate_count as $count){
                            //     $total_rate=  $count['rate'];
                            // }
                            // }else{
                            //     $total_rate = 0;
                            // }
                            // if($total_rate > 0 && !empty($total_rate) && $total_rate != null){
                            //     $avg= round(($total_rate/$total_user_give_rate),2);
                            // }else{
                            //     $avg = 0;
                            // }
                            $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

                                                ->whereNotNull('rate')
                                                ->groupBy('i_by')
                                                ->get()
                                                ->toArray();
                                $rating_count = count($rating_count);
                                             

                                $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

                                                            ->select('*',DB::raw("SUM(rate) as rate"))
                                                            ->whereNotNull('rate')
                                                            ->groupBy('i_by')

                                                            ->get()

                                                            ->toArray();

                                $total_rate =0; 
                                if(!empty($total_rate_count)){

                                    foreach ($total_rate_count as $count){

                                        $total_rate +=  $count['rate'];

                                    }

                                }
                                
                                $avg=0;
                                if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

                                    $avg= round(($total_rate/$rating_count),2);

                                }else{

                                    $avg = 0;

                                }
                            $suggested_profile_ary = [];
                            $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
                            $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
                            $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
                            $suggested_profile_ary['tag']=$suggested_profile['search_keyword'];
                            $suggested_profile_ary['profession']=$suggested_profile['profession_name'];
                            $suggested_profile_ary['location']=$suggested_profile['location'];
                            $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
                            $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
                            $suggested_profile_ary['average_rating']=$avg;
                            $suggested_profile_ary['user_give_rating_count']=$rating_count;
                            $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;
                            array_push($alreadyPushedUser,$suggested_profile['user_id']);
                            $users_no++;
                        }
                    }
                    //dd($suggested_profile_ary);
                    
                }
            }
        }
        if(isset($page_count) && isset($page_no)){                        
            $search_update_array = array_slice($mostvisitedTagUser_data_array,$pageNo,$pageCount);
        }
        // if(isset($page_count) && isset($page_no) && !empty($suggestedProfile_data_array)){
        //     $suggested_profile_array = array_slice($suggestedProfile_data_array,$page_no*$page_count-$page_count,$page_count); 
            
        //     if(isset($suggested_profile_array)){
        //         $suggest_pro = $suggested_profile_array;
        //     }  
        // }

        // if(!empty($mostvisitedTagUser_data_array)){
        //     foreach ($mostvisitedTagUser_data_array as $key => $row) 
        //     {
        //         $date[$key] = $row['keyword_date'];
        //     }
        //     array_multisort($date, $mostvisitedTagUser_data_array);
        //     $mostvisitedTagUser_data_array = array_reverse($mostvisitedTagUser_data_array);
        // }
           
        // $last_page_count = count($suggestedProfile_data_array);
        // if($pageCount !=$last_page_count){
        //     if(!is_array($except)){
        //         $except =[$except];
        //     }
        //     $count = $pageCount-$last_page_count;
            
        //     $random_data = UserTag::select('user_master.fullname','user_master.user_photo','profession.name as profession_name','user_master.location','user_tags.user_id','user_tags.tag_id','user_master.firebase','user_search.keyword')
        //         ->leftjoin('user_master','user_master.user_id','=','user_tags.user_id')
        //         ->leftjoin('profession','profession.id','=','user_master.profession')
        //         ->leftjoin('user_search','user_search.user_id','=','user_master.user_id')
        //         // ->where(function($q) use ($most_vstd_tag){
        //         //     $q->where('user_tags.tag_id',$most_vstd_tag)
        //         //         ->orWhere(\DB::raw('TRIM(user_master.fullname)','like','%'. $most_vstd_tag .'%'));
        //         //     })
        //         ->whereNotIN('user_master.user_id',$except)
        //         // ->where('user_tags.tag_id',$most_vstd_tag)
        //         ->where('user_master.user_id','!=',$user_id)
        //         ->where('user_master.firebase','!=','NULL')
        //         ->where('user_master.is_deleted','N')
        //         ->where('user_master.is_active','Y')
        //         ->where('user_master.is_block','N')
        //         ->groupBy('user_tags.user_id')
        //         ->offset($pageNo)->limit($count)
        //         ->get()->toArray();
        //         if(!empty($random_data)){
        //         foreach ($random_data as $suggested_profile){
                    
        //             if($suggested_profile['user_id'] != $user_id){
        //                 if(!in_array($suggested_profile['user_id'],$alreadyPushedUser)){
        //                     $suggested_user_tags = UserTag::select('tags.name')->where('user_tags.user_id',$suggested_profile['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
        //                     $suggested_user_tags_array = [];
        //                     if(!empty($suggested_user_tags)){
        //                         foreach ($suggested_user_tags as $suggested_user_tag){
        //                             array_push($suggested_user_tags_array, $suggested_user_tag['name']);
        //                         }                                
        //                     }
        //                     $rating_count = UserRating::where('user_ratings.user_id',$suggested_profile['user_id'])

        //                                         ->whereNotNull('rate')
        //                                         ->groupBy('i_by')
        //                                         ->get()
        //                                         ->toArray();
        //                         $rating_count = count($rating_count);
                                             

        //                         $total_rate_count = UserRating::where('user_id',$suggested_profile['user_id'])

        //                                                     ->select('*',DB::raw("SUM(rate) as rate"))
        //                                                     ->whereNotNull('rate')
        //                                                     ->groupBy('i_by')

        //                                                     ->get()

        //                                                     ->toArray();

        //                         $total_rate =0; 
        //                         if(!empty($total_rate_count)){

        //                             foreach ($total_rate_count as $count){

        //                                 $total_rate +=  $count['rate'];

        //                             }

        //                         }
                                
        //                         $avg=0;
        //                         if($total_rate > 0 && !empty($total_rate) && $total_rate != null){

        //                             $avg= round(($total_rate/$rating_count),2);

        //                         }else{

        //                             $avg = 0;

        //                         }
        //                     $suggested_profile_ary = [];
        //                     $suggested_profile_ary['user_id']=$suggested_profile['user_id'];
        //                         $suggested_profile_ary['fullname']=$suggested_profile['fullname'];
        //                         $suggested_profile_ary['profile_image']=$suggested_profile['user_photo'];
        //                         $suggested_profile_ary['tag']=$suggested_profile['keyword'];
        //                         $suggested_profile_ary['profession']=$suggested_profile['profession_name'];
        //                         $suggested_profile_ary['location']=$suggested_profile['location'];
        //                         $suggested_profile_ary['firebase']=$suggested_profile['firebase'];
        //                         $suggested_profile_ary['user_tags']=$suggested_user_tags_array;
        //                         $suggested_profile_ary['average_rating']=$avg;
        //                         $suggested_profile_ary['user_give_rating_count']=$rating_count;
        //                         $suggestedProfile_data_array[$users_no] = $suggested_profile_ary;
        //                     array_push($alreadyPushedUser,$suggested_profile['user_id']);
        //                     $users_no++;
        //                 }
        //             }
        //             //dd($suggested_profile_ary);
                    
        //         }
        //     }
        // }
        if(isset($page_count) && isset($page_no)){                        
            $suggestedProfile_data_array = array_slice($suggestedProfile_data_array,$pageNo,$pageCount);
        }
        
        $data = ['search_update_array' => $search_update_array, 'suggested_profile_array' => $suggestedProfile_data_array];
        //dd($data);
        return $data;
    }  
    public function getUserFromTag($most_vstd_tag,$page_count,$page_no,$user_id,$except) //getsearch by user Tag
    {       
        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }
        $where_str_out = "1 = ?";
        $where_params_out = array(1);
        if($except != null){
            // if(!is_array($except)){
            //     $except =[$except];
            // }
            // dd($except);
            $where_str_out .= " AND user_master.user_id NOT IN($except)";
        }
        $tag_data_final=[];
        //dd($pageCount);
        $tag_data = UserTag::select('user_master.fullname','user_master.user_photo','profession.name as profession_name','user_master.location','user_tags.user_id','user_tags.tag_id','user_master.firebase','user_search.keyword')
                ->leftjoin('user_master','user_master.user_id','=','user_tags.user_id')
                ->leftjoin('profession','profession.id','=','user_master.profession')
                ->leftjoin('user_search','user_search.user_id','=','user_master.user_id')
                // ->where(function($q) use ($most_vstd_tag){
                //     $q->where('user_tags.tag_id',$most_vstd_tag)
                //         ->orWhere(\DB::raw('TRIM(user_master.fullname)','like','%'. $most_vstd_tag .'%'));
                //     })

                // ->whereNotIN('user_master.user_id',$except)
                ->whereRaw($where_str_out, $where_params_out)
                        
                ->where('user_tags.tag_id',$most_vstd_tag)
                ->where('user_master.user_id','!=',$user_id)
                ->where('user_master.firebase','!=','NULL')
                ->where('user_master.is_deleted','N')
                ->where('user_master.is_active','Y')
                ->where('user_master.is_block','N')
                ->groupBy('user_tags.user_id')
                // ->offset($pageNo)->limit($pageCount)
                ->get()->toArray();
                // dd($tag_data);
                $profession_id = UserMaster::where('user_id',$user_id)->value('profession');
                $profession_user_id = UserMaster::select('user_id')->where('profession',$profession_id)
                                                        ->where('user_master.user_id','!=',$user_id)
                                                        ->where('user_master.firebase','!=','NULL')
                                                        ->where('user_master.is_deleted','N')
                                                        ->where('user_master.is_active','Y')
                                                        ->where('user_master.is_block','N')
                                                        ->get()
                                                        ->toArray();
                
                $profession_data = UserTag::select('user_master.fullname','user_master.user_photo','profession.name as profession_name','user_master.location','user_tags.user_id','user_tags.tag_id','user_master.firebase','user_search.keyword')
                        ->leftjoin('user_master','user_master.user_id','=','user_tags.user_id')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->leftjoin('user_search','user_search.user_id','=','user_master.user_id')
                        // ->where(function($q) use ($most_vstd_tag){
                        //     $q->where('user_tags.tag_id',$most_vstd_tag)
                        //         ->orWhere(\DB::raw('TRIM(user_master.fullname)','like','%'. $most_vstd_tag .'%'));
                        //     })
                        ->whereIn('user_master.user_id',$profession_user_id)
                        
                        ->whereRaw($where_str_out, $where_params_out)
                        // ->whereNotIN('user_master.user_id',$except)
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_block','N')
                        ->groupBy('user_tags.user_id')
                        // ->offset($pageNo)->limit($pageCount)
                        ->get()->toArray();
                $tag_data = array_merge($tag_data,$profession_data);
                // array_push($tag_data_final, $tag_data);
                // // if(isset($page_count) && isset($page_no)){                        
                // //     $tag_data_final = array_slice($tag_data,$pageNo,$pageCount);
                // // }
                
                $random_data=[];
                // for randum data bt not duplicate
                $last_page_count = count($tag_data);
                    
                if($pageCount !=$last_page_count){
                   
                    $count = $pageCount-$last_page_count;
                    
                    $random_data = UserTag::select('user_master.fullname','user_master.user_photo','profession.name as profession_name','user_master.location','user_tags.user_id','user_tags.tag_id','user_master.firebase','user_search.keyword')
                        ->leftjoin('user_master','user_master.user_id','=','user_tags.user_id')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->leftjoin('user_search','user_search.user_id','=','user_master.user_id')
                        // ->where(function($q) use ($most_vstd_tag){
                        //     $q->where('user_tags.tag_id',$most_vstd_tag)
                        //         ->orWhere(\DB::raw('TRIM(user_master.fullname)','like','%'. $most_vstd_tag .'%'));
                        //     })
                        // ->whereNotIN('user_master.user_id',$except)
                        
                ->whereRaw($where_str_out, $where_params_out)
                        // ->where('user_tags.tag_id',$most_vstd_tag)
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_block','N')
                        ->groupBy('user_tags.user_id')
                        // ->offset($pageNo)->limit($count)
                        ->get()->toArray();
                        $tag_data = array_merge($tag_data,$random_data);

                         // array_push($tag_data_final, $tag_data_infinite);
                        // if(isset($page_count) && isset($page_no)){                        
                        //     $tag_data_final = array_slice($tag_data_infinite,$pageNo,$pageCount);
                        // }
                }
                // echo "<pre>";
                // print_r($tag_data_final);
                // exit();
        return $tag_data;
    }  
}
