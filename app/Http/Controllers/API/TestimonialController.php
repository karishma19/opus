<?php



namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\UserMaster;

use App\Models\UserTestimonial;

use App\Events\GetTestimonialEvent;

use App\Events\AddTestimonialEvent;

use Event;



class TestimonialController extends Controller

{

    public function getTestimonial(Request $request)

    {

    		$data = $request->all();

	    	$testimonial_detail =array();

            $user_id=$data['userID'];

            $pageCount=$data['pageCount'];

            $pageNo=$data['pageNo'];

                    

            if(!is_numeric($user_id)){

                $status = false;

                $msg = "User id must be numeric.";

            

            }else if(isset($pageCount) && !is_numeric($pageCount)){

            	 $status = false;

                $msg = "Page Count must be numeric.";



            }else if(isset($pageNo) && !is_numeric($pageNo)){

            	  $status = false;

                $msg = "Page No must be numeric.";          

            }else{   

               

                 // if(isset($pageNo)){   

                 //      $pageNo = (($pageNo-1)*$pageCount);   

                 // }

                
                $data =['user_id'=>$user_id,'pageCount'=>$pageCount,'pageNo'=>$pageNo];
                $testimonial_detail = Event::fire(new GetTestimonialEvent($data));
                
                 // $testimonials_details = UserMaster::select('user_master.fullname','user_master.user_photo','user_testimonial.i_by','user_testimonial.user_testimonial_id','user_testimonial.testimonial','user_testimonial.i_date')

                 //                        ->leftjoin('user_testimonial','user_testimonial.i_by','=','user_master.user_id')

                 //                        ->get()->toArray();

                    

                   


                    $testimonial_detail =head($testimonial_detail);

                    $status = true;

                    $msg = "Success";  

            }


            $output['STATUS'] = $status;

            $output['Message'] = $msg;

            $output['DATA'] = $testimonial_detail;

       

	        return response()->json($output,200);

	}



    public function AddTestimonial(Request $request)

    {

        $user_id = $request->get('id');

        $touser_id = $request->get('touser_id');

        $testimonial = $request->get('testimonial');

//dd($user_id);

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

        }elseif(!is_numeric($touser_id)){

            $status = false;

            $msg = "To User id must be numeric.";

        }elseif(empty($testimonial) || $testimonial == ""){

            $status = false;

            $msg = "Testimonial should not be null.";

        }else{    



            $testimonial_data = array(

                'user_testimonial_id'=>0,

                'testimonial'=>$testimonial,

                'user_id'=>$touser_id,

                'i_by'=>$user_id,

                'i_date'=>time(),

            );

            Event::fire(new AddTestimonialEvent($testimonial_data));

            $status = true;

            $msg = "Success";

        }

        $output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = [];

        return response()->json($output,200);

    }

}

