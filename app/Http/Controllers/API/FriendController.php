<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\UserFriend;

use App\Models\UserMaster;

use App\Models\GroupMaster;

use App\Models\NotificationTable;

use Event;

use App\Events\GetFriendEvent;

use App\Events\AddFriendEvent;

use App\Helpers\FriendsHelp;

use App\Helpers\UserExist;
use App\Jobs\sendNotificationJob;

class FriendController extends Controller

{

    // getFriends list

    public function getFriends(Request $request)

    {

        $user_id = $request->get('userID');

        $loginuser_id=$user_id;

        $touser_id = $request->get('toUserID');

        $pageNo = $request->get('pageNo');

        $pageCount = $request->get('pageCount');

        // $friends_array =[];

        

        if($touser_id > 0){

            $user_id = $touser_id;

        }  



        if(!is_numeric($user_id)){

    		$status = false;

            $msg = "User id must be numeric.";

        }else if(isset($pageCount) && !is_numeric($pageCount)){

        	$msg = "Page Count must be numeric.";

        }else if(isset($pageNo) && !is_numeric($pageNo)){

            $msg = "Page No must be numeric.";

        }else{ 

        	if(isset($pageNo)){   

                  $pageNo = (($pageNo-1)*$pageCount);   

            }

            $friends_data =['user_id'=>$user_id,'pageNo'=>$pageNo,'pageCount'=>$pageCount,'loginuser_id'=>$loginuser_id,'touser_id'=>$touser_id];

            

            $friends_detail = Event::fire(new GetFriendEvent($friends_data));

            $friends_detail =head($friends_detail);

            

            $status = true;

            $msg = "Success";   

        }

    	$output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = $friends_detail;

        

        return response()->json($output,200);

    }



    public function unFriend(Request $request)

    {

        $user_id = $request->get('userID');

        $friend_id = $request->get('toUserID');

        //dd($friend_id);

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

        }elseif(!is_numeric($friend_id)){

            $status = false;

            $msg = "To User id must be numeric.";

        }else{    

            $friend_count = UserFriend::where(['user_id' => $user_id, 'friend_id' => $friend_id])

                            // ->orWhere(['user_id' => $friend_id, 'friend_id' => $user_id])

                            ->orWhere(function($q) use ($friend_id,$user_id){

                                $q->where('user_id',$friend_id)

                                  ->where('friend_id',$user_id);

                            })

                            ->count();

            if($friend_count > 0){ 

                $user_delete =  UserFriend::where(['user_id' => $user_id,'friend_id' => $friend_id])->delete();

                $friend_delete = UserFriend::where(['user_id' => $friend_id,'friend_id' => $user_id])->delete();

                //dd($friend_delete);                   

                $status = true;

                $msg = "UnFriend successfully";

            }else{

                $status = false;

                $msg = "Not a Friend";

            }



        }

        $output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = [];

        

        return response()->json($output,200);

    }



    // addFriend

    public function addFriend(Request $request)

    {

        $user_id =$request->userID;

        $friend_id =$request->toUserID;

        $user_data = array();

        

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

        }elseif(!is_numeric($friend_id)){

            $status = false;

            $msg = "To User id must be numeric.";

        }elseif(isset($user_id) && isset($friend_id) && $user_id == $friend_id){

            $status = false;

            $msg = "User id and To User id cant't be same.";

        }else{



            $checkIsFriend = FriendsHelp::checkIsFriend($user_id,$friend_id);

           

            if($checkIsFriend == 'N'){

                $friend_data =['user_id'=>$user_id,'friend_id'=>$friend_id];

                

                $add_friend_detail = Event::fire(new AddFriendEvent($friend_data));

                $add_friend_detail =head($add_friend_detail);
                
                $user_id = $add_friend_detail['user_id'];
                $friend_id = $add_friend_detail['friend_id'];


                $from_user_data = UserExist::getUser($user_id);

                $full_name = $from_user_data['fullname'];

                $is_notify = UserMaster::where('user_id',$friend_id)->value('is_notify');
       
                $notificationCount ='';
                if($is_notify == 'Y'){
                    $notificationCount = NotificationTable::where('notify_to', $friend_id)

                                                                ->where('is_read', 'N')

                                                                ->count();
                }

                $to_user_data = UserExist::getUser($friend_id);

                $device_token = $to_user_data['device_token'];
                
                // $message = "$full_name added on you as friend";
                $message = $full_name." has commented to your post";
                $notification_type = config('Constant.AddFriend');
                $post_id ='';
                $title="friend,$user_id";
                $data = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$post_id,$title,$notificationCount));

                $status = $add_friend_detail['status'];

                $msg = $add_friend_detail['msg'];

                

            }else if($checkIsFriend == 'Y'){

                $status = false;

                $msg = "Already Friend";

            }

        }

        $output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = $user_data;

        

        return response()->json($output,200);

    }

}

