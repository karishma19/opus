<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Event;
use App\Events\SearchResultEvent;

class SearchResultController extends Controller
{
    public function SearchResult(Request $request)
    {
    	$suggested_profile_array = [];
	    $most_visited_tag_array= [];
	    $most_visited_tag = "";
	    $user_id=$request->get('userID');
	    $page_count=$request->get('pageCount');
	    $page_no=$request->get('pageNo');
	    $tag=$request->get('tag');
	    $total_results = 0;

	    if(isset($user_id) &&!is_numeric($user_id)){
	        $status = false;
	        $msg = "User id must be numeric.";
	    }else if(isset($pageCount) && !is_numeric($pageCount)){
	        $status = false;
	        $msg = "Page Count must be numeric.";
	    }else if(isset($pageNo) && !is_numeric($pageNo)){
	        $status = false;
	        $msg = "Page No must be numeric.";
	    }else if(empty ($tag)){
	        $status = false;
	        $msg = "Tag must not be null.";
	        $search_result = [];
	    }else{
	    	// dd($tag);
	    	$data = Event::fire(new SearchResultEvent($user_id,$page_count,$page_no,$tag));
	    	//dd($data);
	    	$status = true;
            $msg = "Success";  
            foreach ($data as $key => $value) {
            	$search_result = $value['search_result'];
				$total_results = $value['total_results'];
            }
	    }
	    $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA']['search_result'] = $search_result;
        $output['DATA']['total_results'] = $total_results;
        return response()->json($output,200);
    }
}
