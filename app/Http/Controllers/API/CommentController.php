<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserPost;
use App\Models\UserComment;
use App\Events\CreateCommentEvent;
use App\Events\CommentListEvent;
use Event;

class CommentController extends Controller
{
    public function CreateComment(Request $request)
    {
      $user_id = $request->get('userID');
      $post_id = $request->get('postID');
        $comment = $request->get('comment');
        if(!is_numeric($user_id)){
         $status = false;
         $msg = "User id must be numeric.";
     }elseif(!is_numeric($post_id)){
         $status = false;
         $msg = "Post id must be numeric.";
     }else{    
      $post_count = UserPost::where('post_id',$post_id)->count();
        //dd($post_data);
      if($post_count > 0){
       $post_data = UserPost::where('post_id',$post_id)->first();
       $comment_data = array(
        'comment_id'    => 0,
        'post_id'       => $post_id,
        'user_id'       => $post_data['user_id'],
        'comment'       => $comment,                  
        'is_approved'   => 'Y',
        'is_deleted'    => 'N',
        'i_by'          => $user_id,
        'i_date'        => time(),
    );
       Event::fire(new CreateCommentEvent($comment_data));
       $status = true;
       $msg = "Success";
   }else{
       $status = false;
       $msg = "Could not find row";
   }
}

$output['STATUS'] = $status;
$output['Message'] = $msg;
$output['DATA'] = [];
return response()->json($output,200);
}

// public function CommentList(Request $request)
// {
//   $userID = $request->get('userID');
//   $user_detail = Event::fire(new CommentListEvent($userID));
//   return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>$user_detail]);
// }



public function commentList(Request $request)
{
    $userID = $request->get('userID');
    $pageNo = $request->get('pageNo');
    $pageCount = $request->get('pageCount');
    $postId = $request->get('postID');


    if(!is_numeric($userID) || $userID == ''){
      return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);
    }else if(isset($pageCount) && !is_numeric($pageNo)){
      return response()->json(['STATUS'=>false,'Message'=>"Page No must be numeric.",'DATA'=>[]]);
    }else if(isset($pageNo) && !is_numeric($pageCount)){
      return response()->json(['STATUS'=>false,'Message'=>"Page Count must be numeric.",'DATA'=>[]]);
    }else if(isset($postId) && !is_numeric($postId)){
      return response()->json(['STATUS'=>false,'Message'=>"Post id must be numeric.",'DATA'=>[]]);
    }else{ 
      
      $user_count = UserComment::where('post_id',$userID)->count();
      // if ($user_count > 0) {
      $commentList_data = ['userID'=>$userID, 'pageNo'=>$pageNo, 'pageCount'=>$pageCount, 'postId'=>$postId];
      
        $user_detail = Event::fire(new CommentListEvent($commentList_data));
        $comment_list_detail = head($user_detail);
        return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>$comment_list_detail]);
      // }
      // return response()->json(['STATUS'=>false,'Message'=>"User not found",'DATA'=>[]]);
    }
}



}
