<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Helpers\FileHelp;

use App\Helpers\UserExist;

use App\Models\PostAbuse;

use App\Events\AbusePostEvent;

use Event;

use App\Models\UserPost;

use App\Models\SharePost;

use App\Models\UserAlbum;

use App\Models\UserMaster;

use App\Models\UserComment;

use App\Models\PostLike;

use App\Events\postsEvent;



use App\Events\SharePostEvent;





class PostController extends Controller

{

    // createPost

    public function createPost(Request $request)

    {

        $user_id = $request->userID;

        $caption = $request->caption;

        

        $contenttype = 1;

        $path        = BASE_PATH . "/uploads/images/";

        $filename    = "";

        $imageUploadPath = IMAGE_UPLOAD_PATH;

        

        $file = $request->media;

        

        if (!empty($file)) {

            // $file = $request->file('media');

            $name = $file->getClientOriginalName();

            

            if (!empty($name) && $name != '') {

                $ext = explode('.', $name);

                

                $prefix    = $ext[0];

                $extension = strtolower($ext[sizeof($ext) - 1]);

                

                if (($extension == 'jpg') || ($extension == 'jpeg') || ($extension == 'gif') || ($extension == 'bmp') || ($extension == 'png')) {

                    $contenttype = '2';

                    $filename    = $prefix . '_' . time() . '_' . $user_id . '.' . $extension;

                    $path_upload = BASE_PATH . '/uploads/images/';

                    
                    $request->file('media')->move($path_upload, $filename);

                    

                    $path_target = BASE_PATH . "/uploads/images/" . $filename;

                    

                    $newfile = "album_post_" . $filename;

                    

                    $path = BASE_PATH . "/uploads/images/" . $newfile;

                    copy($path_target, $path);

                    

                    

                    $filename1 = DIR_UPLOAD_PATH . "$filename";

                    

                    if (!file_exists(DIR_UPLOAD_PATH . '/img_thumbs')) {

                        mkdir(DIR_UPLOAD_PATH . '/img_thumbs', 0777, true);

                    }

                    $pathimg = DIR_UPLOAD_PATH . "img_thumbs/$filename";

                    $imgSrc  = $filename1;

                    $ext     = explode(".", $filename1);

                    $fext    = $ext[sizeof($ext) - 1];

                    

                    

                    list($width, $height) = getimagesize($imgSrc);

                    if ($fext == 'jpg' || $fext == 'jpeg')

                        $myImage = imagecreatefromjpeg($imgSrc);

                    elseif ($fext == 'png')

                        $myImage = imagecreatefrompng($imgSrc);

                    elseif ($fext == 'gif')

                        $myImage = imagecreatefromgif($imgSrc);

                    

                    if ($width > $height) {

                        $y            = 0;

                        $x            = ($width - $height) / 4;

                        $smallestSide = $height;

                    } else {

                        $x            = 0;

                        $y            = ($height - $width) / 4;

                        $smallestSide = $width;

                    }

                    $thumbSizew = 240;

                    $thumbSizeh = 240;

                    $thumb      = imagecreatetruecolor($thumbSizew, $thumbSizeh);

                    imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSizew, $thumbSizeh, $smallestSide, $smallestSide);

                    ob_start();

                    if ($fext == 'jpg' || $fext == 'jpeg')

                        imagejpeg($thumb, $pathimg);

                    elseif ($fext == 'png')

                        imagepng($thumb, $pathimg);

                    elseif ($fext == 'gif')

                        imagegif($thumb, $pathimg);

                    

                    $rawImageBytes = ob_get_clean();

                } else {

                    if (($extension == 'mp4') || ($extension == 'flv') || ($extension == 'mov')) {

                        $contenttype = '3';

                        $filename    = str_replace(".", "", $prefix) . '_' . time() . '_' . $user_id . '.' . strtolower($extension);

                        

                        $request->file('media')->move($path, $filename);

                        

                        // $home     = "/usr";

                        // $ffmpeg   = $home . "/bin/ffmpeg";

                        // // $options  = "-vf scale='min(160\, iw):-1' -ss 00:00:5 -f image2 -vframes 2>&1";

                        // // $vidthumb = $path . $filename . ".jpg";

                        // $options = "-vf scale='min(160\, iw):-1' -ss 00:00:5 -f image2 -vframes 1";

                        // $vidthumb = $path . "video_thumbs" . "/" . $filename . ".jpg";

                       

                        // exec($ffmpeg . " -i " . $path . $filename . " " . $options . " " . $vidthumb, $output);



                        if (!file_exists(BASE_PATH . '/uploads/images/video_thumbs')) {

                            mkdir(BASE_PATH . '/uploads/images/video_thumbs/', 0777, true);

                        }

                        $path_target = BASE_PATH . "/uploads/images/" . $filename;

                        $newfile     = "album_post_" . $filename;

                        

                        $path = BASE_PATH . "/uploads/images/video_thumbs/" . $newfile;

                        

                        \File::copy($path_target, $path);

                    }

                }



                $base_path = BASE_PATH;

                $extType = '';

                

                if (($extension == 'mp4') || ($extension == 'flv') || ($extension == 'mov')) {

                    $extType = 'video';

                } else 

                    if (($extension == 'jpg') || ($extension == 'jpeg') || ($extension == 'gif') || ($extension == 'bmp') || ($extension == 'png')) {

                        $extType = 'image';

                    }

                

                if ($extType === 'image') {

                    //$media_data = "<br/><img id='uploadimg' src='$path$filename' value='$filename'  alt='No Image' class='upload_img'/>";

                    $media_data = "<br/><img id='uploadimg' src='$path' value='$filename'  alt='No Image' class='upload_img'/>";

                } else 

                    if ($extType === 'video') {

                        //$media_data = "<div><object id='player' width='500' height='300' data='$path/public/js/player/gddflvplayer.swf'><param name='movie' value='$path/public/js/player/gddflvplayer.swf'><param name='allowfullscreen' value='true'><param name='flashvars' value='vdo=$imageUploadPath/$filename&autoplay=false'><param name='wmode' value='transparent'></object></div>";

                        $media_data = "<div><object id='player' width='500' height='300' data='".$base_path."public/js/player/gddflvplayer.swf'><param name='movie' value='".$base_path."public/js/player/gddflvplayer.swf'><param name='allowfullscreen' value='true'><param name='flashvars' value='vdo=$imageUploadPath/$filename&autoplay=false'><param name='wmode' value='transparent'></object></div>";

                        $filename = $filename . ".jpg";

                    }else {

                                    

                        if (filter_var($request->media, FILTER_VALIDATE_URL)) {

                            $contenttype = '3';

                            $media_path = $request->media;

                            // $media_data = "<div><object id='player' width='500' height='300' data='$path/public/js/player/gddflvplayer.swf'><param name='movie' value='$path/public/js/player/gddflvplayer.swf'><param name='allowfullscreen' value='true'><param name='flashvars' value='vdo=$media_path&autoplay=false'><param name='wmode' value='transparent'></object></div>";

                            $media_data = $request->media;

                            $extType = 'url';

                        } else {

                            $media_data = '';

                        }

                    }

            }

        }

            $user_id = $request->userID;

            $message = $request->caption;

            $media = $request->media;

            $media_data = $media_data;            

            $filename = $filename; 

            $contenttype = $contenttype;          

            $extType = $extType;

            $extentions_array = array('jpg','jpeg','gif','bmp','mp4','flv','mov');

            $postdata_array = array();



            

            if(!is_numeric($user_id)){echo "<pre>";



                $status = false;

                $msg = "User id must be numeric.";

            }else{

                // create post

                $postdata = array(

                    'user_id'    => $user_id,

                    'message'    => "$message$media_data",                          

                    'post_caption'    => $message,

                    'i_by'       => $user_id,

                    'u_by'       => $user_id,

                    'post_type'  => $contenttype,

                    'post_media'  => $filename,

                    'i_date'     => time(),

                    'u_date'     => time(),

                    'flag'       => 'add'

                );

                $post_save = UserPost::create($postdata);

                $postId = $post_save->post_id;

                

                // create user

                $share_post_data = array(

                    'user_id'=>$user_id,

                    'post_id'=>$postId,

                    'share_text'=>'',

                    'i_by'=>$user_id,

                    'i_date'=>time(),

                    'u_by'=>$user_id ,

                    'u_date'=>time()

                );

                

                //SharePost save 

                $share_post = SharePost::create($share_post_data);

                if($extType == 'image' || $extType == 'video'){

                    $useralbumdata = array(

                        'albumcontent_id'=>0,

                        'user_id'=>$user_id,

                        'content_title'=>'',

                        'content_type'=>$contenttype,

                        'content_folder'=>'',

                        'filename'=>"album_post_".$filename,

                        'post_id' => $postId,

                        'flag'=>'add'

                    );                    

                }else if(!empty($postId) && $extType == 'url'){             

                    $useralbumdata = array(

                        'albumcontent_id'=>0,

                        'user_id'=>$user_id,

                        'content_title'=>'',

                        'content_type'=>$contenttype,

                        'content_folder'=>'',

                        'filename'=>$media,

                        'post_id' => $postId,

                        'flag'=>'add'

                    );

                }else if(!empty($postId) && empty($extType)){

                    $useralbumdata = array(

                        'albumcontent_id'=>0,

                        'user_id'=>$user_id,

                        'content_title'=>'',

                        'content_type'=>'4',

                        'content_folder'=>'',

                        'filename'=>null,

                        'post_id' => $postId,

                        'flag'=>'add'

                    );

                }

                $user_album_save = UserAlbum::create($useralbumdata);

                $albumcontentId = $user_album_save->useralbum_id;



                //getPostsWithUserData

                $postdata = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                                        ->leftJoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                                        ->where('user_post.post_id',$postId)  

                                        ->where('user_post.is_deleted','N')

                                        ->get()

                                        ->toArray();

                $postdata= head($postdata);

                // fetchCountCommentbyPostIdWithoutUserData

                $comment_count = UserComment::where('post_id',$postId)->where('is_deleted','N')->count();

                // fetchLatestCommentsWithPostId

                $latest_comment = UserComment::select('user_master.fullname','user_master.user_photo')

                                                    ->where('post_id',$postId)

                                                    ->leftJoin('user_master', 'user_comment.i_by', '=', 'user_master.user_id')

                                                    ->orderBy('comment_id', 'DESC')

                                                    ->limit(1)

                                                    ->get()

                                                    ->toArray();

                // checkPostlikebyuser

                $postlikebySameUser = PostLike::where('post_id',$postId)

                                                    ->where('user_id',$user_id)

                                                    ->count();

                    if($postlikebySameUser > 0){

                        $postlikebySameUser =  'Y';

                    }else{

                        $postlikebySameUser =  'N';

                    }

                // getUsersLikePost

                $postlikebyuser = PostLike::leftJoin('user_master', 'post_like.user_id', '=', 'user_master.user_id')

                                                ->where('post_like.post_id',$postId)

                                                ->get()

                                                ->toArray();





                $cur_date=date('Y-m-d H:i:s');

                $post_date=date('Y-m-d H:i:s',$postdata['i_date']);

                $time1=strtotime($cur_date);

                $time2=strtotime($post_date);

                $diff= UserExist::dateDifference($time1,$time2,1);

                

                if($time1>$time2){

                    $time_ago=$diff." ago";

                }else{

                    $time_ago='just now';

                }



                $postdata_array ['user_id'] =  $postdata['user_id'];         

                $postdata_array ['user_photo'] =  $postdata['user_photo'];

                $postdata_array ['fullname'] =  $postdata['fullname'];

                $postdata_array ['post_id'] =  $postdata['post_id'];                

                $postdata_array ['media_type'] =  $postdata['post_type'];

                if($postdata['post_type'] == 2 || $postdata['post_type'] == 3){

                    $postdata_array['post_photo'] =  $postdata['post_media'];

                }if($postdata['post_type'] == 3){

                    $string = $this->str_replace_last('.jpg','',$postdata['post_media']);                                 

                    $postdata_array['video_name'] =  $string;

                }

                $postdata_array ['description'] =  $postdata['message'];

                $postdata_array ['post_caption'] =  $postdata['post_caption'];

                $postdata_array ['time'] =  $time_ago;

                $postdata_array['is_liked']=$postlikebySameUser;

                $postdata_array['comment_count']=$comment_count;

                $postdata_array['like_count']=sizeof($postlikebyuser);

                $status = true;

                $msg = "Success";                

            }

        $output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = $postdata_array;    

        return response()->json($output,200);

    }



    // string replace

    public function str_replace_last( $search , $replace , $str ) {

        if( ( $pos = strrpos( $str , $search ) ) !== false ) {

            $search_length  = strlen( $search );

            $str    = substr_replace( $str , $replace , $pos , $search_length );

        }

        return $str;

    }

//abusePost

  public function abusePost(Request $request)

    {

    $data = $request->all();

    $user_data = array();

    $user_id = $request->UserID;   

    $post_id = $request->postID;

    if(!is_numeric($user_id)){

        $status = false;

        $msg = "User id must be numeric.";

    }else if(!is_numeric($post_id)){

        $status = false;

        $msg = "Post id must be numeric.";

    }else{ 

     

       $alreadyAbuse_detail = Event::fire(new AbusePostEvent($data));

       $alreadyAbuse_detail =head($alreadyAbuse_detail);

       $status = $alreadyAbuse_detail['status']; 



        if($status == '0' ){

            $status = true;

            $msg = "Success";

        }else{

            $status = false;

            $msg = "Already Post is Abuse by same User";

        }

    }

    $output['STATUS'] = $status;

    $output['Message'] = $msg;

    $output['DATA'] =$user_data;

   

    return response()->json($output,200);

 

    }

//sharePost

    public function sharePost(Request $request)

    {

       // $user_data = array();

        $data = $request->all();

        $sharepostdata_data = array();

        $user_id = $request->userID;

        $post_id = $request->postID;

        $shareText = $request->shareText;

        

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

            

        }elseif(!is_numeric($post_id)){

            $status = false;

            $msg = "Post id must be numeric.";



        }else

        {



        $sharepostdata_data = Event::fire(new SharePostEvent($data));

     

                     $status = true;

                     $msg = "Success";           

        

             }    

            $output['STATUS'] = $status;

            $output['Message'] = $msg;

            $output['DATA'] = $sharepostdata_data;

       

            return response()->json($output,200);   

    }



    public function posts(Request $request)

    { 

        $userID = $request->get('userID');

        $pageNo = $request->get('pageNo');

        $pageCount = $request->get('pageCount');





        if(!is_numeric($userID) || $userID == ''){

            return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);

        }else if(isset($pageCount) && !is_numeric($pageCount)){

            return response()->json(['STATUS'=>false,'Message'=>"Page Count must be numeric.",'DATA'=>[]]);

        }else if(isset($pageNo) && !is_numeric($pageNo)){

            $msg = "Page No must be numeric.";

            return response()->json(['STATUS'=>false,'Message'=>"Page No must be numeric.",'DATA'=>[]]);

        }else{ 

            if(isset($pageNo)){   

                $pageNo = (($pageNo-1)*$pageCount);   

            }

            $user_count = UserPost::where('user_id',$userID)->count();
            //dd($user_count);
            if ($user_count > 0) {

                $post_data =['userID'=>$userID,'pageNo'=>$pageNo,'pageCount'=>$pageCount];

                $user_detail = Event::fire(new postsEvent($post_data));

                $user_detail = head($user_detail);



                return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>$user_detail]);

            }

            return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>[]]);

        }

    }

}

