<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\UserFriend;

use App\Models\GroupMaster;

use App\Models\NotificationTable;
use App\Models\UserMaster;

use Event;

use App\Events\FeedListEvent;



class FeedListController extends Controller

{

    public function feedList(Request $request)

    {

    	$user_id=$request->userID;

        $pageNo=$request->pageNo;

        $pageCount=$request->pageCount;

        $posts_array = [];

        $suggested_profile_array =[];

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

        }else if(isset($pageCount) && !is_numeric($pageCount)){

            $msg = "Page Count must be numeric.";

        }else if(isset($pageNo) && !is_numeric($pageNo)){

            $msg = "Page No must be numeric.";

        }else{   

        	$feed_list_data = ['user_id'=>$user_id,'pageNo'=>$pageNo,'pageCount'=>$pageCount];



        	$feed_list__detail = Event::fire(new FeedListEvent($feed_list_data));

        	$feed_list__detail = head($feed_list__detail);

        	$posts_array = $feed_list__detail['Feel_list'];

        	$suggested_profile_array = $feed_list__detail['suggested_profile_array'];

        	$status = $feed_list__detail['status'];

        	$msg = $feed_list__detail['msg'];

        	

        }

    
        $is_notify = UserMaster::where('user_id',$user_id)->value('is_notify');
       
        $notificationCount ='';
        if($is_notify == 'Y'){
            $notification_type = config('Constant.chatNotification');
            $notificationCount = NotificationTable::where('notify_to', $user_id)

                                                        ->where('type', '!=', $notification_type)
            											->where('is_read', 'N')

            											->count();
        }
    	$output['STATUS'] = $status;

        $output['Message'] = $msg;

    	$output['DATA'] = array('Feed_list'=>$posts_array,'suggested_profile'=>$suggested_profile_array,'notificationCount'=>$notificationCount);

        

	return response()->json($output,200);

    }

}

