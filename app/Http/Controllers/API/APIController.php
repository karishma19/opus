<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserMaster;
use App\Models\PageMaster;
use App\Models\Profession;
use App\Models\GetVersion;
use Validator;
use Event;
use App\Events\EditProfileEvent;

class APIController extends Controller
{
    public function CMSPages(Request $request)
    {
        $type = $request->get('type');
        $content_pages = array('A' => 'about', 'T' => 'terms', 'P' => 'privacy');
        
        $about_content = $this->getpagecontent($content_pages['A']);
        //dd($about_content['page_desc']);
        $terms_content = $this->getpagecontent($content_pages['T']);
        $privacy_content = $this->getpagecontent($content_pages['P']);
        if(!empty($about_content) && !empty($terms_content) && !empty($privacy_content)) 
        {
            $data = $about_content['page_desc'].$terms_content['page_desc'].$privacy_content['page_desc'];
            //dd($data);
            $responseArray = ['STATUS'=> true, 'Message'=>'Success','DATA'=>$data];
            return response()->json($responseArray,200);
        }else{
            $responseArray = ['STATUS'=> false, 'Message'=>'Page Content not found.','DATA'=>[]];
            return response()->json($responseArray,200);
        }
    }
    public function getpagecontent($type)
    {  
        $cms_page_count = PageMaster::select('page_desc')->where('page_name','like', $type)->first();
       return $cms_page_count; 
    }

    public function GetVersion(Request $request)
    {
        $version=$request->version;
        $type=$request->type;
        $get_version = GetVersion::where('type',$type)
                                   ->where('version',$version)
                                   ->get()
                                   ->toArray();
        // dd($get_version);
        if($get_version)
        {
            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false]);
        }      
    }

    // public function getProfession(Request $request)
    // {
    //     $profession_count = Profession::where('is_active',1)->count();
    //     if($profession_count > 0)
    //     {
    //         $profession = Profession::where('is_active',1)->get()->toArray();
    //         $status = true;
    //         $msg = "Success";
    //     }
    //     else{
    //         $status = false;
    //         $msg = "Profession not found";
    //         $profession = [];
    //     }
    //     $output['STATUS'] = $status;
    //     $output['Message'] = $msg;
    //     $output['DATA'] = $profession;
    //     return response()->json($output,200);
    // }
}
