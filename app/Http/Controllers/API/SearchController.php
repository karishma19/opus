<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Event;
use App\Events\SearchEvent;

class SearchController extends Controller
{
    public function search(Request $request)
    {
 		$user_id = $request->get('userID');
 		$page_no = $request->get('pageNo');
 		$page_count = $request->get('pageCount');
        $except = $request->get('except_id');
 		$search_update_array = [];
        $suggested_profile_array = [];
        $most_visited_tag_array= [];
        $most_visited_tag = "";
        
    	//dd($page_count);
    	if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }else if(isset($page_count) && !is_numeric($page_count)){
        	$status = false;
            $msg = "Page Count must be numeric.";
        }else if(isset($page_no) && !is_numeric($page_no)){
        	$status = false;
            $msg = "Page No must be numeric.";
        }else{
        	//dd('1');
            // if(!empty($except)){
            //     $except = explode(',', $except);
            // }
        	$data = Event::fire(new SearchEvent($user_id,$page_count,$page_no,$except));    //dd($data);  
        	$status = true;
            $msg = "Success";
          
            foreach ($data as $key => $value) {
            	$search_update_array = $value['search_update_array'];
				$suggested_profile_array = $value['suggested_profile_array'];
            }

        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA']['search_updates'] = $search_update_array;
        $output['DATA']['suggested_profiles'] = $suggested_profile_array;
        return response()->json($output,200);
    }
}
