<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserRating;
use Event;
use App\Events\GiveRateEvent;

class RatingController extends Controller
{
	// giveRating
    public function giveRating(Request $request)
    {
    	$user_id = $request->fromUserID;
        $to_id = $request->toUserID;
        $rate = $request->rating;
        $rate_data=[];
        if(!is_numeric($user_id)){
	        $status = false;
	        $msg = "From User id must be numeric.";
	    }else if(!is_numeric($to_id)){
	        $status = false;
	        $msg = "To User id must be numeric.";
	    }else if(!is_numeric($rate)){
	        $status = false;
	        $msg = "Rating must be numeric.";
	    }else if(is_numeric($rate) && $rate>10){
	        $status = false;
	        $msg = "Rating should not greater than 10.";                
	    }else{ 
	    	$ratingdata = array(
	            'userrating_id'       => null,
	            'rating_type'       => 1,
	            'user_id'       => $to_id,
	            'rate'       => $rate,
	            'i_by'          => $user_id,
	            'i_date'        => time(),
	            'flag'          => 'add'
	        );
	        $is_already_retting_given = UserRating::where('i_by',$user_id)
	        											->where('user_id',$to_id)
	        											->count();
	        
        	if($is_already_retting_given == 0){
				$rate_detail = Event::fire(new GiveRateEvent($ratingdata));

				$userrating_id = head($rate_detail);
				
				if(!empty($userrating_id) && is_numeric($userrating_id)){
					$status = true;
                	$msg = "Success";
				}
        	}else{
	            $status = false;
	            $msg = "You have already rated this user."; 
	        }
	    }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = $rate_data;
        return response()->json($output,200);
    }
}
