<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PostLike;
use App\Models\UserPost;
use App\Models\UserMaster;
use Event;
use App\Events\LikeListEvent;

class LikeController extends Controller
{
    //likepost
    public function likePost(Request $request)
    {
        $userID = $request->get('userID');

        $postID = $request->get('postID');

        $pageNo = $request->get('pageNo');

        $pageCount = $request->get('pageCount');

        $type = $request->get('type'); 

        if(!is_numeric($userID) || $userID == ''){

            return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);

        }else if(isset($postID) && !is_numeric($postID)){

            return response()->json(['STATUS'=>false,'Message'=>"Post id must be numeric.",'DATA'=>[]]);

        }else if(isset($pageCount) && !is_numeric($pageNo)){

            return response()->json(['STATUS'=>false,'Message'=>"Page No must be numeric.",'DATA'=>[]]);

         }else if(isset($pageNo) && !is_numeric($pageCount)){

            return response()->json(['STATUS'=>false,'Message'=>"Page Count must be numeric.",'DATA'=>[]]);
        }else{ 
            
            // $user_count = PostLike::where('post_id',$postID)->count();

      // if ($user_count > 0) {
            if($type=='like'){
                $user_count = PostLike::where('post_id',$postID)->
                                    where('user_id',$userID)->
                                    count();
                if ($user_count > 0) {
                    return response()->json(['STATUS'=>true,'Message'=>"Post already like by same user",'DATA'=>[]]);
                }
            }
            $likeList_data = ['userID'=>$userID, 'pageNo'=>$pageNo, 'pageCount'=>$pageCount, 'postID'=>$postID,'type'=>$type];

      

        $user_detail = Event::fire(new LikeListEvent($likeList_data));
            // dd($user_detail);
        if($type=='like'){
            // $like_list_detail = head($user_detail);
            $STATUS = true;
            $Message = 'Success';
            $DATA = head($user_detail);
        }else
        {
            $STATUS = true;
            $Message = 'Post unlike Success'; 
            $DATA=[];  
        }
            return response()->json(['STATUS'=>$STATUS,'Message'=>$Message,'DATA'=>$DATA]);
        // echo "<pre>";
        // print_r($user_detail);
        // exit();

        }

    }
}
