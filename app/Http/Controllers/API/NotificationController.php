<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NotificationTable;
use App\Models\UserMaster;
use App\Models\Profession;
use App\Models\UserTag;
use App\Models\Tag;
use Event;
use App\Events\notificationEvent;
use App\Helpers\UserExist;
use App\Events\NotificationSettingEvent;


class NotificationController extends Controller
{
	public function notification(Request $request)
	{
		$userID = $request->get('userID');
		if (!is_numeric($userID) || $userID == '') {
			return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);
		}else{
			$user_count = NotificationTable::where('notify_to',$userID)->count();
		
			// echo "<pre>";
			// print_r($user_count);
			// exit();
			if ($user_count > 0) {
				$is_notify = UserMaster::where('user_id',$userID)->where('is_deleted','N')->where('is_active','Y')->value('is_notify');
				
				if($is_notify =='Y'){

					$user_detail = Event::fire(new notificationEvent($userID));
					$user_detail = head($user_detail);
					$notification_arr = $user_detail['notification_arr'];
					$status = $user_detail['status'];
					$msg = $user_detail['msg'];
					return response()->json(['STATUS'=>$status,'Message'=>$msg,'DATA'=>$notification_arr]);
				}
				return;
			}
			return response()->json(['STATUS'=>false,'Message'=>"User not found",'DATA'=>[]]);
		}
	}

	// notificationSetting
	public function notificationSetting(Request $request)
	{
		$user_id = $request->userID;
		$is_notify = $request->is_notify;
		$tags = [];
		$user_data = [];
		if(!is_numeric($user_id)){
	        $status = false;
	        $msg = "User id must be numeric.";
	    }else{
        	$user_detail = UserExist::getUser($user_id);
        
	        if(!empty($user_detail)){
	        	$notification_data = array(
			        'user_id'    => $user_id,
			        'is_notify'    => $is_notify,	        				
			        'flag'       => 'updatenotificationsetting'
			    );

			    $notification_detail = Event::fire(new NotificationSettingEvent($notification_data));
			    $usertags = UserTag::select('tags.name')->where('user_tags.user_id',$user_id)
			    					->Leftjoin('tags','user_tags.tag_id','=','tags.id')
			    					->get()->toArray();
			    foreach ($usertags as $key=>$tag){
			    	array_push($tags, $tag['name']);
			    	// $tags[$tag['name']] = $tag['name'];
			    }
			    $status = true;
		        $msg = "Success";

        		$user_data = UserExist::getUser($user_id);	   
        		     		
		        $user_data = array('user_data'=>$user_data, 'user_tags'=>$tags);
	        }else{
		        $status = false;
		        $msg = "User Not Found.";
		    }
		}
	    $output['STATUS'] = $status;
	    $output['Message'] = $msg;
	    $output['DATA'] = $user_data;
	    return response()->json($output,200);
	}

	public function clearAllNotifications(Request $request)
    {
    	$user_id=$request->get('userID');
    	//dd($user_id);
	    $postdata_array =array();
	    if(!is_numeric($user_id)){
	        $status = false;
	        $msg = "User id must be numeric.";
	    }else{
	        $data = [];
			$data['is_clear'] = 'Y';
			$notification_update = NotificationTable::where('notify_to',$user_id)->where('is_clear','N')->update($data);
			$status = true;
	        $msg = "Success";                  

	    }
	    $output['STATUS'] = $status;
	    $output['Message'] = $msg;
	    $output['DATA'] = [];
	    return response()->json($output,200);
	}

	public function clearChatNotifications(Request $request)
    {
    	$user_id=$request->get('userID');
    	$friend_id=$request->get('friendId');
    	// dd($friend_id);
	    // $postdata_array =array();
	    if(!is_numeric($user_id)){
	        $status = false;
	        $msg = "User id must be numeric.";
	    }else if(!is_numeric($friend_id)){
	    	$status = false;
	        $msg = "Friend id must be numeric.";
	    }else{

       	 	$notification_type = config('Constant.chatNotification'); 
	        $data = [];
			$data['is_clear'] = 'Y';
			$data['is_read'] = 'Y';
			$notification_update = NotificationTable::where('type',$notification_type)
													->where('notify_to',$user_id)
													->where('notify_by',$friend_id)
													->where('is_clear','N')
													->update($data);
	        // dd($notification_update);

			$status = true;
	        $msg = "Success";                  

	    }
	    $output['STATUS'] = $status;
	    $output['Message'] = $msg;
	    $output['DATA'] = [];
	    return response()->json($output,200);
	}
}
