<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\UserMaster;

use App\Models\NotificationTable;

use App\Events\aboutUserEvent;

use Validator;

use App\Helpers\UserExist;
use App\Helpers\UserTagHelp;
use App\Helpers\FileHelp;

use App\Models\Tag;

use App\Models\Profession;
use App\Models\AlbumContent;

use App\Models\UserRating;
use App\Models\Setting;

use App\Models\UserFriend;
use App\Models\UserAbuse;
use App\Models\UserChatHistory;

use Auth;

use Event;

use App\Events\EditProfileEvent;

use App\Events\UserProfileEvent;

use DB;
use App\Jobs\sendNotificationJob;

class UserController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    

    // user login

    public function loginUser(Request $request)

    { 

      $validator = Validator::make($request->all(),[

        'email'=>'required',

        'password'=>'required',

        'device_type'=>'required',

        'device_token'=>'required'

      ]);



      if ($validator->fails()) 

      {

       return response()->json(['STATUS'=> false,'Message'=>$validator->errors()->first()],200);

     }

     $email = $request->get('email');

     $password = $request->get('password');

     $device_type = $request->get('device_type');

     $device_token = $request->get('device_token');



     $tags =array();

     $userdata =array();

     $user_data =array();

     $userExist = UserExist::checkUserExist($email);



     if($userExist>0){

      $auth = array('email_id' => $email, 'user_password' => base64_encode($password));

      $data = UserMaster::where('email_id',$email)

      ->where('is_active','Y')

      ->where('is_deleted','N')

      ->where('is_block','N')

      ->where('device_token',$device_token)

      ->get()

      ->toArray();



      $data = head($data);



      if(!empty($data)){



       if($data['is_block'] == "Y"){

        $status = false;

        $msg = "Your Account has been Blocked.Please Contact Administrator";

      }elseif ($data['is_active'] == "N") {

        $status = false;

        $msg = "Your Account is not active. Please check your email to activate your account.";

      } else {

                    // $d =$this->getUserTable($data['user_id']);                        

        $getTags = UserTag::select('tags.name')

        ->where('user_id',$data['user_id'])

        ->leftjoin('tags', 'tags.id', '=', 'user_tags.tag_id')

        ->get()

        ->toArray();



        foreach ($getTags as $tag_key => $tag) {

          array_push($tags, $tag['name']);

        }



        $userdata['user_id'] = $data['user_id'];

        $userdata['online_status'] = 'L';

        $userdata['flag'] = 'updatedevicedetails';

        $userdata['device_type'] = $data['device_type'];

        $userdata['device_token'] = $data['device_token'];



        $user_data = UserExist::getUser($data['user_id']);



        $status = true;

        $msg = "Success";



                   //  echo "<pre>";

                   //  print_r(Auth::guard('user_master')->attempt(['email_id' => $email,'user_password' => $password]));

                   //  // print_r($email);

                   // exit();

                   //  if(Auth::guard('user_master')->attempt(['email_id' => $email,'user_password' => $password]))

                   //  {

                   //      echo "<pre>";

                   //      print_r('this');

                   //      exit();

                   //  }

                   //  echo "<pre>";

                   //  print_r('sadfs');

                   //  exit();

        $user = UserMaster::where('email_id', $email)

        ->where('user_password',  base64_encode($password))

        ->where('is_active','Y')

        ->where('is_deleted','N')

        ->where('is_block','N')

        ->first();

        if($user){

          $token =  $user->createToken('MyApp')->accessToken;                        

        }

      }

    }else{

      $status = false;

      $msg = "Invalid Email or Password";

    }

  }else{

    $status = false;

    $msg = "You are not registered. Please register first.";

  }

  $output['STATUS'] = $status;

  $output['Message'] = $msg;

  $output['DATA'] = array('user_data'=>$user_data,'user_tags'=>$tags);

  if(!empty($user_data)){

    $output['access_token'] = $token;

  }            

  return response()->json($output,200);

}



    // getFriends list

public function getFriends(Request $request)

{

  $user_id = $request->get('userID');

  $loginuser_id=$user_id;

  $touser_id = $request->get('toUserID');

  $pageNo = $request->get('pageNo');

  $pageCount = $request->get('pageCount');

  if($touser_id > 0){

    $user_id = $touser_id;

  }  



  if(!is_numeric($user_id)){

    $status = false;

    $msg = "User id must be numeric.";

  }else if(isset($pageCount) && !is_numeric($pageCount)){

    $msg = "Page Count must be numeric.";

  }else if(isset($pageNo) && !is_numeric($pageNo)){

    $msg = "Page No must be numeric.";

  }else{ 



  }



}



    // addUserTag

public function addUserTag(Request $request)

{

  $user_id = $request->get('userID');

  $tag = $request->get('tag');





}



public function EditProfile(Request $request)
{
  $profile_data = $request->all();
  $user_id = $profile_data['user_id'];

  // $validator = Validator::make($request->all(),[
  //   'email'     => 'unique:user_master,email_id,'.$user_id.',user_id',
  // ]);
  // if ($validator->fails())
  // {
  //     $status = 'false';
  //     $msg = 'Email ID already associated with other user. Try Other email id';
  //     $data = [];
  // }
  // else{
      $data = Event::fire(new EditProfileEvent($profile_data));
      // echo "<pre>";
      // print_r($data);
      // exit();
      foreach ($data as $key => $value) {
        if($value){
          if($value['birth_date'] == '1970-01-01' || $value['birth_date'] == NUll || $value['birth_date'] == '0000-00-00' || $value['birth_date'] == 0){
                    $date = null;
            }else{
                // $date = date('F d,Y', $value['birth_date']);
                $date = date("F d,Y", strtotime($value['birth_date']));
            }
          // echo "<pre>";
          // print_r($date);
          // exit();
          $value['birth_date'] = $date;
          $status = 'true';
          $msg = 'Success';
          $data = $value;
          // dd($data);
        }
        else{
          $status = 'false';
          $msg = 'User not found';
          $data = [];
        }
      }
    // }
      $output['STATUS'] = $status;
      $output['Message'] = $msg;
      $output['DATA']['user_data'] = $data;
      return response()->json($output,200);

}

    // get userProfile

public function userProfile(Request $request)

{

  $user_id = $request->get('userID');

  $touser_id = $request->get('toUserID');
  $output_data =[];


  if(!is_numeric($user_id)){

    $status = false;

    $msg = "User id must be numeric.";

  }if(isset($touser_id) && !is_numeric($user_id)){

    $status = false;

    $msg = "To User id must be numeric.";

  }else{

    $user_data = UserMaster::where('user_master.user_id',$user_id)

    ->leftjoin('profession', 'profession.id', '=', 'user_master.profession')

    ->where('user_master.is_active','Y')

    ->where('user_master.is_deleted','N')

    ->where('user_master.is_block','N')

    ->get()->toArray();

    if(!empty($user_data)){

      $user_data =['user_data'=>$user_data,'user_id'=>$user_id,'touser_id'=>$touser_id];

      $user_profile_data = Event::fire(new UserProfileEvent($user_data));



      $status  = true;

      $msg = "Success";
      
      $output_data = head($user_profile_data);

    }else{

      $status = false;

      $msg = "User not found.";

    }

    $output['STATUS'] = $status;

    $output['Message'] = $msg;

    $output['DATA'] = $output_data;



    return response()->json($output,200);

  }

}

    public function Logout(Request $request){

        $user_id = $request->get('userID');

        //dd($user_id);

        if(!is_numeric($user_id)){

            $status = false;

            $msg = "User id must be numeric.";

        }else{ 

            $user_count = UserMaster::where('user_id',$user_id)->count();

            //dd($user_count);

            if($user_count > 0){

                $logout = UserMaster::where('user_id',$user_id)->update(['device_token' => null]);

                $msg = 'success';

                $status = true;

            }else{

                $msg = 'user not present';

                $status = false;

            }               

        }      



        $output['STATUS'] = $status;

        $output['Message'] = $msg;

        $output['DATA'] = [];

        return response()->json($output,200);

    }



public function aboutUser(Request $request)

{

  $userID = $request->get('userID');

    // 

  if (!is_numeric($userID) || $userID == '') {

    return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);

  }else{



    $user_count = UserMaster::where('user_id',$userID)

                        ->where('is_active','Y')

                        ->where('is_deleted','N')

                        ->where('is_block','N')

                        ->count();

    if ($user_count > 0) {

     $user_detail = Event::fire(new aboutUserEvent($userID));

     $user_detail = head($user_detail);

     return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>$user_detail]);

   }

   return response()->json(['STATUS'=>false,'Message'=>"User not found",'DATA'=>[]]);  



 }

}



  // socialLogin

  public function socialLogin(Request $request)

  {

    $social_id = $request->social_id;

    $fullname = $request->fullname;

    $email_id = $request->email;

    $device_type = $request->device_type;

    $device_token = $request->device_token;
    
    $gender = $request->gender;
    
    // $birth_date = $request->birth_date;

    // $profile_picture = $request->profile_picture;
    $data = $request->all();
    
    $url = $request->profile_picture;
                
    $tags =[];
    $user_data =[];
    $is_register_user ='';
    if((isset($data['social_id']) && $data['social_id']!="")  && (isset($data['device_type']) && $data['device_type']!="" && is_numeric($data['device_type'])))

    {

      $check_user_block = UserMaster::where('social_id',$social_id)

                                      // ->where('is_deleted','Y')
                                      ->where('is_block','Y')
                                      ->where('is_active','N')

                                     ->count();
        
        if($check_user_block>0){
          return response()->json(['STATUS'=>false,'Message'=>'Your acoount is inactive','DATA'=>[]]);
        }
        $checksocial_id = UserMaster::where('social_id',$social_id)
                                        ->get()
                                        ->toArray();
                                        
        $checksocial_id = head($checksocial_id);
        if(empty($checksocial_id)){                   

            // if(filter_var($data['profile_picture'],FILTER_VALIDATE_URL) === FALSE){

            //     $status = false;

            //     $msg = "Profile picture URL is not valid.";

            // }else{
          $filename='';
                if(isset($request->profile_picture) && !empty($request->profile_picture)){
                  $filename = $this->imgUpload($url,$social_id);
                }
               
                $userdata = array(
                    'user_id'=>'0',
                    'fullname'=>$data['fullname'],
                    // 'email_id'=>isset($data['email']),
                    // 'user_password'=>$data['user_pass'],
                    'search_keyword'=>$data['fullname'],
                    // 'user_photo'=>$data['social_id'].'.jpg',
                    'device_type'=>$data['device_type'],
                    'device_token'=>$data['device_token'],
                    'social_id'=>$data['social_id'],
                    'online_status'=>'L',
                    'is_active'=>'Y',
                    'is_search'=>'Y',
                    'is_deleted'=>'N',
                    'i_by'=>1,
                    'i_date'=>time(),
                    'u_by'=>1,
                    'u_date'=>time(),
                    'flag'=>'add',
                    'sociallogin'=>1
                    // 'firebase'=>$data['fire_id']
                    // 'gender'=>$gender
                );
                if(!empty($request->email) && !empty($request->email)) {
                    $userdata['email_id'] = $request->email;
                }
                if($request->email == 'undefined'){
                  $userdata['email_id'] = null;
                }
                if(isset($request->fire_id) && !empty($request->fire_id)){
                  $userdata['firebase'] = $data['fire_id'];
                }
                if(isset($request->profile_picture) && !empty($request->profile_picture)){
                  $userdata['user_photo'] = $data['social_id'].'.jpg';
                
                }
                $user_save = UserMaster::create($userdata);
                
                // opus chat history for default create
                if($user_save->user_id != OPUS)
                {
                  $chat_history_opus = new UserChatHistory(); 
                  $chat_history_opus->user_id = $user_save->user_id;
                  $chat_history_opus->to_user_id = OPUS;
                  $chat_history_opus->is_block = 'N';
                  $chat_history_opus->i_date =time();
                  $chat_history_opus->u_date =time();
                  $chat_history_opus->save();
                }

                $data = array(

                    'user_id'=>$user_save->user_id,

                    'friend_id'=>OPUS,

                    'group_id'=>'',

                    

                    );

                $datafriend = array(

                    'user_id'=>OPUS,

                    'friend_id'=>$user_save->user_id,

                    'group_id'=>'',                    

                    );

                

                $addFriend = UserFriend::create($data);

                $datafriend = UserFriend::create($datafriend);

                if(!empty($user_save)){
                    $user_data = UserExist::getUser($user_save->user_id);
                    $user_data['isNewUser'] = true;
                    $useralbumdata = array(
                        'albumcontent_id'=>0,
                        'user_id'=>$user_save->user_id,
                        'content_title'=>'',
                        'content_type'=>1,
                        'content_folder'=>'',
                        'filename'=>$filename,
                        'flag'=>'add'
                    ); 
                    $album_content = AlbumContent::create($useralbumdata);
                    $status = true;
                    $msg = "Success";
                    $is_register_user = "NO";
                }else
                {
                    $status = false;
                    $msg = "Social Login Failed.";
                }
            // }
        }else{
            $usertags = UserTagHelp::getTags($checksocial_id['user_id']);
            $i = 0;
            foreach ($usertags as $tag){
                // $tags[$i]['id'] = $tag['tag_id'];
              array_push($tags, $tag['user_tag']);
                // $tags[$i]['name'] = $tag['user_tag'];
                $i++;
            }
            // $userdata['user_id'] = $checksocial_id['user_id'];
            // $userdata['online_status'] = 'L';
            // // $userdata['flag'] = 'updatedevicedetails';
            // $userdata['device_type'] = $data['device_type'];
            // $userdata['device_token'] = $data['device_token'];
            // $user_edit_data = UserMaster::select('gender')->where('user_id',$userdata['user_id'])->first();
            
            // if(isset($gender)){
            //     if($gender == 'null' || $gender == ''){
            //         $gender = null;
            //     }
            //     else{
            //         $gender = $gender;
            //     }            
            // }
            // else{
            //     $gender = $user_edit_data['gender'];
            // }
            
            // if(isset($email_id)){
            //     if($email_id == 'null' || $email_id == '' || $email_id == 'undefined'){
            //         $email_id = null;
            //     }
            //     else{
            //         $email_id = $email_id;
            //     }            
            // }
            // else{
            //     $gender = UserMaster::select('gender')->where('user_id',$userdata['user_id'])->first();
            //     $email_id = $user_edit_data['email_id'];
            // }
            // $userdata['gender'] = $gender;
            // $userdata['email_id'] = $email_id;
            // // $userdata['profession'] = $profession;
            // // $userdata['location'] = $location;
            // // $userdata['brief_desc'] = $brief_desc;
            // // $userdata['birth_date'] = $birth_date;
            // // if(isset($request->email) && !empty($request->email)){
            // //   $userdata['email_id'] = $email_id;
            // // }
            // if(isset($request->fire_id) && !empty($request->fire_id)){
            //   $userdata['firebase'] = $data['fire_id'];
            // }
            
            // if(isset($request->profile_picture) && !empty($request->profile_picture)){
                
            //   $old_image = $checksocial_id['user_photo'];
            //   if ($old_image != null) {
            //       $filename1 = BASE_PATH.'/uploads/images/';
            //       $img_thumbs= BASE_PATH.'/uploads/images/img_thumbs/';
                  
            //       $img = FileHelp::UnlinkImage($filename1, $old_image);
            //       FileHelp::UnlinkImage($img_thumbs, $old_image);
            //   }
            //   // image update 
            //   $image = $this->imgUpload($url,$social_id);
            //   $userdata['user_photo'] = $data['social_id'].'.jpg';
            // }
            
            $user_update = UserMaster::where('user_id',$checksocial_id['user_id'])->update(['device_token'=>$device_token,'device_type'=>$device_type]);
           
            $user_data = UserExist::getUser($checksocial_id['user_id']);
            if($user_data['birth_date'] != null){
              // $user_data['birth_date'] = '';
              
              // $user_data['birth_date'] = date('F d,Y', $user_data['birth_date']);
              $user_data['birth_date'] = date("F d,Y", strtotime($user_data['birth_date']));
            }

           
            $status = true;
            $msg = "Success";
            $is_register_user = "YES";
        }
    }else{
        $status = false;
        $msg = "Social Login Failed.";
    }
    $output['STATUS'] = $status;
    $output['Message'] = $msg;
    $output['is_register_user'] = $is_register_user;
    $output['DATA'] = array('user_data'=>$user_data,'user_tags'=>$tags);
    return response()->json($output);
}
    // image compress
    public function compress($source, $destination, $quality) {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source);

        elseif ($info['mime'] == 'image/png') 
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    // image upload
    public function imgUpload($url,$social_id)
    {
        
        $info = pathinfo($url);
        // echo "<pre>";
        // print_r($social_id);
        // exit();
        $contents = file_get_contents($url);
        $extc = explode('.', $info['basename']);
        $filename = $social_id.'.jpg';
        
        $file_old = BASE_PATH .'/uploads/images/'. $filename;
        
        file_put_contents($file_old, $contents);

        $image_name = BASE_PATH.'/uploads/images/'.$social_id.'.jpg';
      
          $file = fopen($image_name, 'w+');
          
          fputs($file, $contents);

          fclose($file);

          $source_img = $image_name;
          
          $destination_img = $image_name;

          $d = $this->compress($source_img, $destination_img, 90);
         
          $filename1 = DIR_UPLOAD_PATH.$social_id.'.jpg';
          $pathimg= DIR_UPLOAD_PATH.'img_thumbs/'.$social_id.'.jpg';

          $imgSrc = $filename1;
          $ext = explode(".",$filename1);
          $fext = $ext[sizeof($ext)-1];

          list($width, $height) = getimagesize($imgSrc);
          if($fext=='jpg' || $fext=='jpeg')
              $myImage = imagecreatefromjpeg($imgSrc);
          elseif($fext=='png')
              $myImage = imagecreatefrompng($imgSrc);
          elseif($fext=='gif')
              $myImage = imagecreatefromgif($imgSrc); 

        if ($width > $height) {
            $y = 0;
            $x = ($width - $height) / 4;
            $smallestSide = $height;
        } else {
            $x = 0;
            $y = ($height - $width) / 4;
            $smallestSide = $width;
        }
        $thumbSizew = 240;
        $thumbSizeh = 240;
        $thumb = imagecreatetruecolor($thumbSizew, $thumbSizeh);
        imagecopyresampled($thumb, $myImage, 0, 0, $x, $y, $thumbSizew, $thumbSizeh,$smallestSide, $smallestSide);
        ob_start();
        if($fext=='jpg' || $fext=='jpeg')
            imagejpeg($thumb,$pathimg);
        elseif($fext=='png')
            imagepng($thumb,$pathimg);
        elseif($fext=='gif')
            imagegif($thumb,$pathimg);  

        $rawImageBytes = ob_get_clean();
        return $filename;
    }


    // blockUser
    public function blockUser(Request $request)
    {
        $user_id = $request->userID;
        $abuse_by = $request->userID;
        $touser_id = $request->toUserID;
        
        if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }elseif(!is_numeric($touser_id)){
            $status = false;
            $msg = "To User id must be numeric.";
        }else{
            $alreadyAbuse_count = UserAbuse::where('user_id',$touser_id)
                                        ->where('abuse_by',$abuse_by)
                                        ->count();
            if($alreadyAbuse_count == 0){

                
                $userdata = array(
                    'abuse_id'=>0,
                    'user_id'=>$touser_id,
                    'abuse_by'=>$abuse_by
                );
                $set_abuser_setting = Setting::where('setting_id','2')->value('var_value');
                // user block
                $abuse_id = UserAbuse::create($userdata);
                // $user_block = UserMaster::where('user_id',$touser_id)->update(['is_block'=>'Y']);
                $time = time();
                $user_detail = UserChatHistory::firstOrNew(['user_id'=>$abuse_by,'to_user_id'=>$touser_id]);
                $user_detail->user_id = $abuse_by;
                $user_detail->to_user_id = $touser_id;
                $user_detail->is_block = 'Y';
                $user_detail->i_date = $time;
                $user_detail->u_date = $time;
                $user_detail->save();

                $to_user_detail = UserChatHistory::firstOrNew(['user_id'=>$touser_id,'to_user_id'=>$abuse_by]);
                $to_user_detail->user_id = $touser_id;
                $to_user_detail->to_user_id = $abuse_by;
                $to_user_detail->is_block = 'N';
                $to_user_detail->i_date = $time;
                $to_user_detail->u_date = $time;
                $to_user_detail->save(); 
                   
                $no_of_timeabuser = UserAbuse::where('user_id',$touser_id)->count();
                if($no_of_timeabuser >= 5)
                {
                    $user_inactive = UserMaster::where('user_id',$touser_id)->update(['is_active'=>'N','is_block'=>'Y']);
                }
                $status = true;
                $msg = "Success";
            }else{
                $status = false;
                $msg = "User has already been reported";
            }
        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = [];
        return response()->json($output);
    }

    // chatNotification
    public function chatNotification(Request $request)
    {
        $to_user_id = $request->toUserId;
        $from_user_id = $request->fromUserId;
        $message = $request->message;
        
        $user_name = UserMaster::where('user_id',$from_user_id)->value('fullname');
        $device_token = UserMaster::where('user_id',$to_user_id)->value('device_token');
        $firebase = UserMaster::where('user_id',$from_user_id)->value('firebase');
        $notification_type = 'chat';
        $post_id ='';
        $message = " New Message from ". $user_name;
        $title ='chat,'.$from_user_id.','.$user_name.','.$firebase;

        $is_notify = UserMaster::where('user_id',$to_user_id)->value('is_notify');
       
        $notification_type = config('Constant.chatNotification');
                $save_detail = new NotificationTable();
                $save_detail->notify_id = '';
                $save_detail->notify_by = $from_user_id;
                $save_detail->notify_to = $to_user_id;
                $save_detail->type = $notification_type;
                $save_detail->i_time = time();
                $save_detail->save();

        $notificationCount ='';
        if($is_notify == 'Y'){
            $notificationCount = NotificationTable::where('notify_to', $to_user_id)

                                  ->where('is_read', 'N')

                                  ->count();
        }

        $send_notification = dispatch(new sendNotificationJob($device_token,$message,$notification_type,$post_id,$title,$notificationCount));

        $output['STATUS'] = true;
        $output['Message'] = 'Success';
        $output['DATA'] = [];
        return response()->json($output);
        
    }

    // userChatHistory
    public function userChatHistory(Request $request)
    {
        $user_id = $request->get('userID');
        $touser_id = $request->get('toUserId');
        if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }elseif(!is_numeric($touser_id)){
            $status = false;
            $msg = "To User id must be numeric.";
        }else{
            $is_block = UserChatHistory::where('user_id',$user_id)->where('to_user_id',$touser_id)->value('is_block');
            
            if($is_block == 'N' || $is_block == null){
                $time = time();
                $user_detail = UserChatHistory::firstOrNew(['user_id'=>$user_id,'to_user_id'=>$touser_id]);
                $user_detail->user_id = $user_id;
                $user_detail->to_user_id = $touser_id;
                $user_detail->is_block = 'N';
                $user_detail->i_date = $time;
                $user_detail->u_date = $time;
                $user_detail->save();

                $to_user_detail = UserChatHistory::firstOrNew(['user_id'=>$touser_id,'to_user_id'=>$user_id]);
                $to_user_detail->user_id = $touser_id;
                $to_user_detail->to_user_id = $user_id;
                $to_user_detail->is_block = 'N';
                $to_user_detail->i_date = $time;
                $to_user_detail->u_date = $time;
                $to_user_detail->save();                
                
                $status = true;
                $msg = "User chat available";
            }else{
                $status = false;
                $msg = "User block.";
            }           
        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = [];
        return response()->json($output);
    }

    // userChatHistory
    public function userChatList(Request $request)
    {
        $user_id = $request->get('userID');
        $chat_list =[];
        
        if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }else{
            
            $chat_detail = UserChatHistory::select('to_user_id')->where('user_id',$user_id)
                                                // ->where('is_block','N')
                                                ->groupBy('to_user_id')->get()->toArray();
            if(!empty($chat_detail)){
                $data =[];
                foreach ($chat_detail as $key => $value) {
                    array_push($data, $value['to_user_id']);
                }
                                                
                $user_chat_data = UserChatHistory::select('user_chat_history.id','user_master.fullname','user_master.user_photo','user_master.firebase','user_master.location','user_chat_history.to_user_id','user_chat_history.is_block')
                        ->leftjoin('user_master','user_chat_history.to_user_id', '=', 'user_master.user_id')
                        // ->leftjoin('user_friends','user_chat_history.to_user_id', '=', 'user_friends.friend_id')
                        ->with('userIsBlock')
                        ->whereIn('user_master.user_id',$data)
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_active','Y')
                        ->where('user_master.firebase','!=','')
                        ->groupBy('user_master.user_id')
                        ->orderBy('user_master.fullname','ASC')
                        ->get()->ToArray();
                     
                if(!empty($user_chat_data)){
                    $i = 0;
                    foreach ($user_chat_data as $key => $chat_value) {
                        $chat_list[$i]['to_user_id']=$chat_value['to_user_id'];
                        $chat_list[$i]['fullname'] = $chat_value['fullname'];

                        $chat_list[$i]['profile_image']=$chat_value['user_photo'];

                        $chat_list[$i]['firebase']=$chat_value['firebase'];
                        $chat_list[$i]['location']=$chat_value['location'];
                        $chat_list[$i]['is_block']=$chat_value['is_block'];
                        if(isset($chat_value['to_user_id']) != ''){
                          $is_friend = UserFriend::where('user_id',$user_id)
                                                    ->where('friend_id',$chat_value['to_user_id'])->count();
                                                    // echo "<pre>";
                                                    // print_r($is_friend);
                                                    // exit();
                          if($is_friend>0){
                            $chat_list[$i]['is_friend'] = 'Y';
                          }else{
                            $chat_list[$i]['is_friend'] = 'N';
                          }
                        }

                        if(!empty($chat_value['user_is_block'])){
                          foreach ($chat_value['user_is_block'] as $key => $value) {
                            if($value['to_user_id'] != $user_id){
                              if($value['is_block'] == 'Y'){
                                $chat_list[$i]['chat_available'] = 'N';
                              }else{
                                $chat_list[$i]['chat_available'] = 'Y';
                              }
                            }
                          }
                        }
                        $i++;
                    }
                }
                
            }

            $status = true;
            $msg = "User chat list.";   
        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = $chat_list;
        return response()->json($output);
    }

    //userChatIsBlock

    public function userIsBlock(Request $request)
    {
        $user_id = $request->get('userID');
        $touser_id = $request->get('toUserID');
        if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }elseif (!is_numeric($touser_id)) {
            $status = false;
            $msg = "to User id must be numeric.";
        }
        else{
            $user_chat_count =UserChatHistory::where('user_chat_history.user_id',$user_id)
                        ->where('user_chat_history.to_user_id',$touser_id)->count();

            if($user_chat_count>0){
                $user_chat_data = UserChatHistory::select('user_chat_history.id','user_master.fullname','user_master.user_photo','user_master.firebase','user_master.location','user_chat_history.to_user_id','user_chat_history.is_block','user_chat_history.is_dot_allow','user_friends.friend_id')
                            ->leftjoin('user_master','user_chat_history.to_user_id', '=', 'user_master.user_id')
                            ->leftjoin('user_friends','user_chat_history.user_id', '=', 'user_friends.friend_id')
                            ->with('userIsBlock')
                            ->where('user_chat_history.user_id',$user_id)
                            ->where('user_chat_history.to_user_id',$touser_id)
                            ->where('user_master.user_id',$touser_id)
                            ->where('user_master.is_deleted','N')
                            ->where('user_master.is_active','Y')
                            ->where('user_master.firebase','!=','')
                            ->groupBy('user_master.user_id')
                            ->orderBy('user_master.fullname','ASD')
                            ->get()->ToArray();

                $user_chat_data = head($user_chat_data);
                
                $user_chat_data['is_friend'] = 'N';
                $user_chat_data['chat_available'] = 'Y';
                
                $user_is_block = UserChatHistory::where('user_id',$touser_id)->where('to_user_id',$user_id)->value('is_block');
                if($user_is_block == 'Y'){
                    $user_chat_data['chat_available'] = 'N';                
                }
                $user_friend_count = UserFriend::where('user_friends.user_id',$user_id)
                        ->where('user_friends.friend_id',$touser_id)->count();
                        // echo "<pre>";
                        // print_r($user_friend_count);
                        // exit();
                 if($user_friend_count > 0){
                    $user_chat_data['is_friend'] = 'Y';
                }       
                // if(isset($user_chat_data['friend_id']) && !empty($user_chat_data['friend_id'])){
                //     $user_chat_data['is_friend'] = 'Y';                
                // }
            }else{
                $user_friend_count = UserFriend::where('user_friends.user_id',$user_id)
                        ->where('user_friends.friend_id',$touser_id)->count();
                        $user_chat_data['is_friend'] = 'N';
                        $user_chat_data['is_block'] = 'N';
                $user_block = UserAbuse::where('abuse_by',$user_id)
                                        ->where('user_id',$touser_id)->count();
                if($user_friend_count > 0){
                    $user_chat_data['is_friend'] = 'Y';
                }
                    $user_chat_data['chat_available'] = 'Y';
                if($user_block > 0){
                    $user_chat_data['is_block'] = 'Y';
                }
                    // echo "<pre>";
                    // print_r($user_chat_data);
                    // exit();
            }
            

            
            // $is_friend = UserFriend::where('user_id',$user_id)
            //                                         ->where('friend_id',$touser_id)->count();
            // if($is_friend>0){
            //     $user_chat_data['is_friend'] = 'Y';
            // }
            $status = true;
            $msg = "User chat list.";   

            unset($user_chat_data['user_is_block']);
            // $chat_permission = ChatPermission::where('user_id',$user_id)
            //                                     ->where('to_userid',$touser_id)->count();
            // $user_chat_data['allow_all_permission'] = 'Y';
            // if($chat_permission > 0){
            //     $user_chat_data['allow_all_permission'] = 'N';
            // }
        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = $user_chat_data;
        return response()->json($output);
    } 


    // isDotEnable for block user, add testimonial , give rating etc
    public function isDotEnable(Request $request)
    {
        $user_id = $request->get('userID');
        $touser_id = $request->get('toUserID');
        if(!is_numeric($user_id)){
            $status = false;
            $msg = "User id must be numeric.";
        }elseif (!is_numeric($touser_id)) {
            $status = false;
            $msg = "to User id must be numeric.";
        }
        else{
            $chat_permission_count =UserChatHistory::where('user_id',$user_id)->where('to_user_id',$touser_id)->count();
            if($chat_permission_count > 0){
              $chat_permission_detail = UserChatHistory::where('user_id',$user_id)->where('to_user_id',$touser_id)
                                    ->update(['is_dot_allow'=>'Y']);
              $status = true;
              $msg = "User chat permission allow."; 
            }else{
              $status = false;
              $msg = "User not found."; 
            }
        }
        $output['STATUS'] = $status;
        $output['Message'] = $msg;
        $output['DATA'] = [];
        return response()->json($output);
    }
}