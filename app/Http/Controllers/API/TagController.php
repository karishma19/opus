<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserTag;
use App\Models\UserMaster;
use App\Models\Tag;
use App\Events\tagsEvent;
use Auth;
use Event;
use App\Events\AddUserTagEvent;
use App\Events\UserTagEvent;
use App\Models\Profession;
use App\Helpers\UserTagHelp;
use App\Events\AddTagEvent;
use Validator;

class TagController extends Controller
{
   	// addUserTag
  public function addUserTag(Request $request)
  {
    $user_id = $request->get('userID');
    $tag = $request->get('tag');

    $user_count = UserMaster::where('user_id',$user_id)
    ->where('is_active','Y')
    ->where('is_deleted','N')
    ->where('is_block','N')
    ->count();
    if($user_count>0){
        $tag_count = Tag::where('name',$tag)->count();
            // tag not exisit
        
        if($tag_count == 0){
            $tags = ['tag'=>$tag,'user_id'=>$user_id];
                  // create tag
            $tag_save = Event::fire(new AddUserTagEvent($tags));
            $tag_id = head($tag_save);
                        
                // create user tag
            if($tag_save){
                      // $user_tag = ['usertag_id'=>null,'user_id'=>$user_id,'tag_id'=>$tag_id,'likes'=>0,'i_by'=>$user_id,'i_date'=>$i_date,'u_by'=>$user_id,'u_date'=>$i_date];
                      // userTagSave
                $user_tag_save = $this->userTagSave($user_id,$tag_id);
            }  
        }else{
        $tag_id = Tag::where('name',$tag)->value('id');
        
        $user_tag_id = UserTag::where('tag_id',$tag_id)
                                ->where('user_id',$user_id)
                                ->count();
        
            if($user_tag_id == 0){
                    // fire event 
                $user_tag_save = $this->userTagSave($user_id,$tag_id);
     
            }else{
                return response()->json(['STATUS'=>false, 'Message'=>'tag already created','DATA'=>[]],200);;
           }
    }
    $tags = Tag::select('name as user_tag','id as user_tag_id')->where('name',$tag)->get()->toArray(); 
    return response()->json(['STATUS'=>true,'Message'=>'Success', 'DATA'=>$tags],200);
    }        
}

    // userTagSave
public function userTagSave($user_id, $tag_id)
{
 $i_date = strtotime('now');	

 $user_tag = ['usertag_id'=>null,'user_id'=>$user_id,'tag_id'=>$tag_id,'likes'=>0,'i_by'=>$user_id,'i_date'=>$i_date,'u_by'=>$user_id,'u_date'=>$i_date];
    $users_data =UserTag::create($user_tag);
    
 return $users_data->usertag_id;
}

    // delete user Tag
public function deleteUserTag(Request $request)
{
    $tag_check = Tag::where('id',$request['tag_id'])
                      ->value('default_tag');
        
        if($tag_check == 0){
          $user_tag_count = UserTag::where('tag_id',$request['tag_id'])
                                      ->where('user_id','!=',$request['userID'])->count();
          if($user_tag_count == 0){
             $tag_delete = Tag::where('id',$request['tag_id'])
                                  ->delete();
          }
        }

      $user_tag_delete = UserTag::where('user_id',$request['userID'])
                             ->where('tag_id',$request['tag_id'])
                             ->delete();

 return response()->json(['STATUS'=>true,'Message'=>'Tag delete success', 'DATA'=>[]],200);
}

public function getAllTag(Request $request)
{
  ini_set('display_startup_errors',true);
  ini_set('display_errors',true);
  $tags =array();         
  $users =array();
  $locations =array();
  $professions =array();

  $users_data = UserMaster::where('is_block','N')->where('is_active','Y')->where('is_deleted','N')->orderBy('user_id','desc')->get()->toArray();
  if(!empty($users_data)){                    
    foreach ($users_data as $user){
      array_push($users,$user['fullname']);
    }
  }
        //dd($users);
  $users_location = UserMaster::where('location','<>','')->where('is_block','N')->where('is_active','Y')->where('is_deleted','N')->groupBy('location')->get()->toArray();
  if(!empty($users_location)){
    foreach ($users_location as $location){
      $locations[] = $location['location'];
    }
  }
        //dd($locations);
  $profession_data = Profession::where('is_active',1)->orderBy('name','asc')->get()->toArray();
  if(!empty($profession_data)){
    $i = 0;
    foreach ($profession_data as $profession){
      $professions[$i]['id'] = $profession['id'];
      $professions[$i]['name'] = $profession['name'];
      $i++;
    }
  }
        //dd($professions);

  $tags_data = Tag::where('is_active',1)->/*where('default_tag',1)->*/orderBy('name','asc')->get();
  if(!empty($tags_data)){
    $i = 0;
    foreach ($tags_data as $tag){
      $tags[$i]['id'] = $tag['id'];
      $tags[$i]['name'] = $tag['name'];
      $i++;
    }
  }
        //dd($tags);
  $status = true;
  $msg = "Success";
  $output['STATUS'] = $status;
  $output['Message'] = $msg;
        //$output['DATA'] = $tags;
  $output['DATA'] = array('tags'=>$tags,'users'=>$users, 'professions' => $professions, 'locations' => $locations);
  return response()->json($output,200);
}

    // tags
public function tags(Request $request)
{
  $userID = $request->get('userID');
    // 
  if (!is_numeric($userID) || $userID == '') {
    return response()->json(['STATUS'=>false,'Message'=>"User id must be numeric.",'DATA'=>[]]);
  }else{
    $user_count = UserTag::where('user_id',$userID)->count();
    
    if ($user_count > 0) {
     $user_detail = Event::fire(new tagsEvent($userID));
     $user_detail = head($user_detail);
     return response()->json(['STATUS'=>true,'Message'=>"Success",'DATA'=>$user_detail]);
   }
   return response()->json(['STATUS'=>false,'Message'=>"Tag not found",'DATA'=>[]]);  

 }
}

  // AddTag
  public function addTag(Request $request)
  {
    $data = $request->all();
    $user_id =$request->userID;
    $output =[];
    $user_data =[];
    // $validator = Validator::make($data,[
    //     'professions'     => 'required'
    //     // 'location'      => 'required',  
    // ]);
    
    // if ($validator->fails()) 
    // {
    //     return response()->json(['STATUS'=>false,'Message'=>$validator->errors(),'DATA'=>[]]);       
    // }
    if(!is_numeric($user_id)){

      $status = false;

      $msg = "User id must be numeric.";

    }else{ 
      $userExist = UserMaster::where('user_id',$user_id)
                                      ->where('is_active','Y')

                                    ->where('is_deleted','N')

                                    ->where('is_block','N')->count();

      if($userExist>0){
        $details = Event::fire(new AddTagEvent($data));
        $user_data = head($details);
        
        $status = true;

        $msg ='Success';
      }else{
        $status =false;
        $msg ='user not found';
      }
  }
  
      $output['STATUS']=$status;
      $output['Message']=$msg;
      $output['DATA']=$user_data;
  return response()->json($output);
}
}
