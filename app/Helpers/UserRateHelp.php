<?php

namespace App\Helpers;

use App\Models\UserRating;

use App\Models\UserTag;



class UserRateHelp

{

	// getAllRatingsforMobile

	public static function getAllRatingsforMobile($user_id)

	{

		$total_user_give_rate =UserRating::where('user_id', $user_id)

                                                    ->groupBy('i_by')

                                                    ->get()
                                                	->toArray();                            	
        return count($total_user_give_rate);

	}



	// gettotalRatingsforMobile

	public static function gettotalRatingsforMobile($user_id)

	{

		$total_rate_count = UserRating::select(\DB::raw('SUM(rate) as rate'))

                                                		->whereNotNull('rate')
                                                        ->where('user_id',$user_id)

                                                        ->where('rate','!=',null)

                                                        ->groupBy('i_by')

                                                        ->get()

                                                        ->toArray();

        return $total_rate_count;

	}

}