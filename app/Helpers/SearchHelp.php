<?php
namespace App\Helpers;
use App\Models\UserTag;
use App\Models\UserMaster;
use App\Models\UserSearch;
use App\Models\Tag;
//use Session;

class SearchHelp
{
	// public static function getSearchUser($search_keyword,$page_count,$page_no,$type,$user_id)
	// {
	// 	// dd($search_keyword);
	// 	if(isset($page_no) && $page_no != 0){   
 //            $pageNo = (($page_no-1)*$page_count);
 //        }
 //        if(isset($page_count) && $page_count != 0){
 //            $pageCount = $page_count;
 //        }
 //        $query = UserSearch::select('user_master.fullname','user_master.user_photo','user_master.profession','user_master.location','user_search.user_id','user_search.keyword','user_master.profession as profession_id','tags.name as tag_name','user_master.firebase',\DB::raw('COUNT(user_search.user_id) as num'))
 //                            ->leftJoin('user_master', 'user_master.user_id', '=', 'user_search.user_id')
 //                            ->leftjoin('profession','profession.id','=','user_master.user_id')
 //                            ->leftjoin('user_tags','user_tags.user_id','=','user_master.user_id')
 //                            ->leftjoin('tags','tags.id','=','user_tags.tag_id');
 //                            //dd($query);
 //        if(!empty($search_keyword))
 //        {
 //        	foreach ($search_keyword as $key => $value) {
 //        		//print_r($value);
 //                $tag_id = UserTag::select('tags.name as tag_name','user_tags.user_id')->leftjoin('tags','tags.id','=','user_tags.tag_id')->where('tags.name','like','***'. $value['keyword'] .'***')->get()->toArray();
                
 //        		// $query->orWhere('user_master.fullname','like','%'. $value['keyword'] .'%');
 //        		// $query->orWhere('profession.name','like','%'. $value['keyword'] .'%');
 //                $query->where(function($q) use($value){
 //                    $q->orWhere('user_master.fullname','like','***'. $value['keyword'] .'***')
 //                        ->orWhere('profession.name','like','***'. $value['keyword'] .'***');
 //                });
 //            }
 //            foreach ($tag_id as $key1 => $value1) {
 //                $query->where(function($q) use($value1){
 //                    $q->orWhere('user_master.user_id',$value1['user_id']);
 //                });
 //        		//$query->orWhere('user_master.user_id',$value1['user_id']);
 //            }
 //        	if(isset($user_id)){	
 //        		$query->where('user_master.user_id','!=',$user_id);			
	// 		}
 //        }
 //        $query->where('user_master.firebase','!=','NULL');
 //        $query->where('user_master.is_deleted','N');
 //        $query->where('user_master.is_active','Y');
 //        $query->where('user_master.is_block','N');
 //        $query->groupBy('user_search.user_id');
 //        $query->take(3);
 //        if(isset($type)){
 //            if($type == 'suggestedProfile' || $type == 'searchResult'){
 //                //print_r($type);exit;  
 //                $query->orderBy('num','desc');   
 //            }
 //        }
 //       $result = $query->offset($pageNo)->limit($pageCount)->get()->toArray();
 //        //dd($result);
 //        if(isset($type) && $type == 'count'){
	// 		return $result->count();
	// 	}else{
	// 		return $result;
	// 	}
	// }

    // new code

    public static function getSearchUser($search_keyword,$page_count,$page_no,$type,$user_id)
    {
        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }
       
        $where_str_out = "1 = ?";
        $where_params_out = array(1);
        
        if(!empty($search_keyword))
        {
            $location ='';
            // $search_keyword[0]['keyword'] = 'Backpacking';
            foreach ($search_keyword as $key => $value) {
                
                $keyword = $value['keyword'];
                if (strpos($value['keyword'], ' in') !== false){
                    $keyword_arr = explode(' ', $value['keyword']);
                    
                    $keyword = $keyword_arr[0];
                    $location = $keyword_arr[2];
                }
                
                $where_str_out .= " AND ( tags.name like \"%{$keyword}%\""
                    ." OR user_master.fullname like \"%{$keyword}%\""
                    ." OR profession.name like \"%{$keyword}%\""
                    . ")";

                //print_r($value);
                $tag_id = UserTag::select('tags.name as tag_name','user_tags.user_id')
                                    ->leftjoin('tags','tags.id','=','user_tags.tag_id')
                                    ->where('tags.name','like','%'. $keyword .'%')
                                    ->get()->toArray();

                foreach ($tag_id as $key1 => $value1) {
                    $where_str_out .= " OR user_master.user_id =". $value1['user_id'];
                }
                // for location  
                // if($location != ''){
                //     $where_str_out .= " AND (FIND_IN_SET('user_master.location','$location'))";
                // }

            }
            if(isset($user_id)){  
                $where_str_out .=" OR user_master.user_id = $user_id";     
            }
        }
            //dd($result);
        $result = UserSearch::select('user_master.fullname','user_master.user_photo','user_master.profession','user_master.location','user_search.user_id','user_search.keyword','user_master.profession as profession_id','tags.name as tag_name','user_master.firebase',\DB::raw('COUNT(user_search.user_id) as num'))
                            ->leftJoin('user_master', 'user_master.user_id', '=', 'user_search.user_id')
                            ->leftjoin('profession','user_master.user_id','=','profession.id')
                            ->leftjoin('user_tags','user_tags.user_id','=','user_master.user_id')
                            ->leftjoin('tags','tags.id','=','user_tags.tag_id')
                            ->where('user_master.firebase','!=','NULL')
                            ->where('user_master.is_deleted','N')
                            ->where('user_master.is_active','Y')
                            ->where('user_master.is_block','N')
                            ->groupBy('user_search.user_id')
                            ->orderBy('user_search.keyword','DESC')
                            ->whereRaw($where_str_out, $where_params_out)
                            // ->take(3)
                            ->offset($pageNo)->limit($pageCount)
                            ->get()->toArray();
        
        if(isset($type) && $type == 'count'){
            return $result->count();
        }else{
            return $result;
        }
    }
}

