<?php 



namespace App\Helpers;



use App\Models\UserMaster;

use App\Models\Profession;



class UserExist {



	// check if user exist or not

    public static function checkUserExist($email)

    {

        

        $user_count = UserMaster::where('email_id',$email)

                                    ->where('is_active','Y')

                                    ->where('is_deleted','N')

                                    ->where('is_block','N')

                                    ->count();

        return $user_count;

    }



    // getUser

    public static function getUser($user_id)

    {

    	$profession = UserMaster::select('user_master.*','profession.name as profession')->where('user_master.user_id', $user_id)

    								->leftjoin('profession', 'profession.id', '=', 'user_master.profession')

                                    ->where('user_master.is_active','Y')

                                    ->where('user_master.is_deleted','N')

                                    ->where('user_master.is_block','N')

    								->get()->toArray();

    	$profession = head($profession);

    	return $profession;                                            

    }



    // dateDifference

    public static function dateDifference($time1, $time2, $precision)

    {

        // If not numeric then convert texts to unix timestamps

        if (!is_int($time1)) {

          $time1 = strtotime($time1);

        }

        if (!is_int($time2)) {

          $time2 = strtotime($time2);

        }

     

        // If time1 is bigger than time2

        // Then swap time1 and time2

        if ($time1 > $time2) {

          $ttime = $time1;

          $time1 = $time2;

          $time2 = $ttime;

        }

     

        // Set up intervals and diffs arrays

        $intervals = array('year','month','day','hour','minute','second');

        $diffs = array();

     

        // Loop thru all intervals

        foreach ($intervals as $interval) {

          // Set default diff to 0

          $diffs[$interval] = 0;

          // Create temp time from time1 and interval

          $ttime = strtotime("+1 " . $interval, $time1);

          // Loop until temp time is smaller than time2

          while ($time2 >= $ttime) {

             $time1 = $ttime;

             $diffs[$interval]++;

             // Create new temp time from time1 and interval

             $ttime = strtotime("+1 " . $interval, $time1);

          }

        }$count = 0;

        $times = array();

        // Loop thru all diffs

        foreach ($diffs as $interval => $value) {

          // Break if we have needed precission

          if ($count >= $precision) {

            break;

          }

          // Add value and interval 

          // if value is bigger than 0

            if ($value > 0) {

                         // Add s if value is not 1

                         if ($value != 1) {

                           $interval .= "s";

                         }

                         // Add value and interval to times array

                         $times[] = $value . " " . $interval;

                         $count++;

            }/*else{

             $times[] = "0 minutes ago";    

            }*/

        }



        // Return string with times

        return implode(" ", $times);    

    }



    // getUserIfEmptyTag

    public static function getUserIfEmptyTag($pageCount,$pageNo,$user_id)

    {

      if(isset($pageNo) && $pageNo != 0){   

          $pageNo = (($pageNo-1)*$pageCount);                    

       }

      

      $user_data_empty_tag =  UserMaster::select('user_master.*','profession.name as profession')->where('user_id','!=',$user_id)
                                            ->leftjoin('profession', 'profession.id', '=', 'user_master.profession')
                                            ->offset($pageNo)

                                            ->limit($pageCount)

                                            ->inRandomOrder()

                                            ->get()->toArray();

      return $user_data_empty_tag;



    }

}