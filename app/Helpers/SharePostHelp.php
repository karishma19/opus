<?php

namespace App\Helpers;

use App\Models\SharePost;

use App\Models\UserMaster;



class SharePostHelp

{

	// getSharedPostByWithUserData

	public static function getSharedPostByWithUserData($post_id,$user_id)

	{

		$share_post_user_data =SharePost::select('post_share.*','user_master.fullname','user_master.user_photo')

												->leftJoin('user_master', 'post_share.user_id', '=', 'user_master.user_id')

												->where('post_share.i_by',$user_id)

												->where('post_share.post_id',$post_id)

												->where('post_share.is_share',"Y")

												->where('post_share.type',1)

												->get()

												->toArray();



		return $share_post_user_data;

	}

}