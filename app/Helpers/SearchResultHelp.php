<?php
namespace App\Helpers;
use App\Models\UserTag;
use App\Models\UserMaster;
use App\Models\Profession;
//use Session;

class SearchResultHelp
{
	// check friends exist or not
	public static function getUsersFromTags($tags, $friends = null, $abuse_userids = null, $user_id)
	{
        //$user_session = Session::put('user_id',$user_id);

        if(isset($tags) && !empty($tags)){
            $query = UserTag::select('user_master.fullname','user_master.user_photo','user_master.profession','user_master.location','tags.name','user_tags.user_id','user_master.firebase')
                        ->where('user_master.firebase','!=','NULL')
                        ->leftjoin('tags','tags.id','=','user_tags.tag_id')
                        ->leftjoin('user_master','user_master.user_id','=','user_tags.user_id');
                // ->whereRaw(FIND_IN_SET('css', Tags))
            $tags = str_replace(",", "", $tags);
            $tags_arr = explode(" ", $tags);
           
        // if($location != ''){
        //     // $query->whereRaw("FIND_IN_SET(location,$location)");
        //     $query->where('user_master.location','like', '%' . $location . '%');
        // }
        //dd($tags_arr);
            foreach ($tags_arr as $tag) {
              //$query->orWhere('tags.name','like', '%' . $tag . '%');
              $query->where(function($q) use($tag){
                    $q->orWhere('tags.name','like', '%' . $tag . '%');
                });
        //dd($result);
            }
            if(isset($friends) && !empty($friends)){
                $implode_friends = implode(',', $friends);
                $query->whereNotIn('user_tags.user_id',$friends);
            }
            if(isset($abuse_userids) && !empty($abuse_userids)){
                $implode_abuse_userids = implode(',', $abuse_userids);
                $query->whereNotIn('user_master.user_id',$abuse_userids);
            }
            $query->where('user_tags.user_id','!=',$user_id);
            $query->orderBy('user_tags.likes','desc');
            $result = $query->limit(10)->get()->toArray();
            //dd($result);
            $suggeted_profiles = [];
            $suggeted_counter = 0;
            $result_count = count($result);
            
            if($result_count > 0){
                foreach ($result as $user_key => $user) {
            //dd($user);
                    $suggeted_profiles[$suggeted_counter]['user_id'] =  $user['user_id'];
                    $suggeted_profiles[$suggeted_counter]['fullname'] =  $user['fullname'];
                    $suggeted_profiles[$suggeted_counter]['user_photo'] =  $user['user_photo'];
                    $suggeted_profiles[$suggeted_counter]['profession'] =  $user['profession'];
                    $suggeted_profiles[$suggeted_counter]['location'] =  $user['location'];
                    $user_tags = UserTag::select('tags.id','tags.name')->where('user_tags.user_id',$user['user_id'])->leftjoin('tags','tags.id','=','user_tags.tag_id')->get()->toArray();
                    if(count($user_tags) > 0){
                        $usertags = [];
                        $tagcnt = 0;
                        foreach ($user_tags as $usertag_key => $user_tag) {
                            $usertags[$tagcnt] = $user_tag['name'];
                            $tagcnt++;
                        }
                        $suggeted_profiles[$suggeted_counter]['tags'] = $usertags;
                    }
                    else{
                        $suggeted_profiles[$suggeted_counter]['tags'] = '';
                    }
                    $suggeted_counter++;
                }
            }
        }
        //dd($suggeted_profiles);
        return $suggeted_profiles;		
	}


    public static function getSearchUser($searchdata, $tag_userids, $abuse_userids, $user_id, $page_count,$page_no,$location)
    {
        // dd($searchdata);
        
        $query = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N');

        if($location != ''){
            // $query->whereRaw("FIND_IN_SET(location,$location)");
            $query->where('user_master.location','like', '%' . $location . '%');
        }
        $implode_tag_userids = implode(',',$tag_userids);
        if(isset($tag_userids) && !empty($tag_userids)){
            //dd('if');
            $query->whereIn('user_master.user_id',$tag_userids);
            // $searchdata = str_replace(",", "", $searchdata);        
            // $search_arr = explode(" ", $searchdata);
            if(!empty($search_arr)) {
                foreach ($searchdata as $search_key => $search) {
                    // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                    // $query->orWhere('profession.name','like','%'. $search .'%');
                    // $query->where(function($q) use($search){
                        $query->orWhere('user_master.fullname','like','%'. $search .'%');
                            // ->orWhere('profession.name','like','%'. $search .'%');
                    // });
                }               
            }
        }
        else{
            //dd('else');
            $search_arr = [];
            // $searchdata = str_replace(",", "", $searchdata);
            // $search_arr = explode(" ", $searchdata);
            //dd($search_arr);
            if(!empty($search_arr)) {
                $counter = 0;
                foreach ($searchdata as $search_key => $search) {
                    if($counter == 0) {
                        // $query->where('user_master.fullname','like','%'. $search .'%');
                        // $query->orWhere('profession.name','like','%'. $search .'%');
                        $query->where(function($q) use($search){
                            $q->orWhere('user_master.fullname','like','%'. $search .'%');
                                // ->orWhere('profession.name','like','%'. $search .'%');
                        });
                        //dd($result);
                    } else {
                        // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                        // $query->orWhere('profession.name','like','%'. $search .'%');
                        $query->where(function($q) use($search){
                            $q->orWhere('user_master.fullname','like','%'. $search .'%');
                                // ->orWhere('profession.name','like','%'. $search .'%');
                        });
                    }
                    $counter++;
                }
            }
            else{
                // $query->where('user_master.fullname','like','%'. $search .'%');
                // $query->orWhere('profession.name','like','%'. $search .'%');
                $query->where(function($q) use($searchdata){
                    $q->where('user_master.fullname','like','%'. $searchdata .'%');
                        // ->orWhere('profession.name','like','%'. $search .'%');
                });
            }
        }
        if(isset($abuse_userids) && !empty($abuse_userids)) {
            $implode_abuse_userids = implode(',', $abuse_userids);
           // dd($implode_abuse_userids);
           $query->whereNotIn('user_master.user_id',$abuse_userids);
        }
        // $result = $query->get()->toArray();
                    // dd($result);
        // $query->where('user_master.is_deleted','N');
        // $query->where('user_master.is_block','N');

        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }




// ========================================= for profession ==============================
        // dd($searchdata);
        $profession_id = Profession::select('id')
                                    ->where('name','like', '%' . $searchdata . '%')
                                    ->get()
                                    ->toArray();
                                    // dd($profession_id);
        $where_str_out = "1 = ?";
        $where_params_out = array(1);

        if($location != ''){
            $where_str_out .= " AND ( user_master.location like '%". $location ."%' )";
        }
        $profession_data = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N')
                        ->whereIn('user_master.profession',$profession_id)
                        ->whereRaw($where_str_out, $where_params_out)
                        ->get()
                        ->toArray();
        // dd($profession_data);
// ========================================= end profession ==============================

// ========================================= for location without in and from ==============================
        // dd($searchdata);

        $location_data = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N')
                        ->where('user_master.location','like', '%' . $searchdata . '%')
                        ->get()
                        ->toArray();
        // dd($location_data);
// ========================================= end location without in and from ==============================

// ======================================= for location ===============================================
        if($location !=''){

                // $query->where('user_master.location','like', '%' . $location . '%');
                $result = $query->get()->toArray();
            
            $query_location = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N')
                        ->where('user_master.location','like', '%' . $location . '%');
                
                if(isset($abuse_userids) && !empty($abuse_userids)) {
                    $implode_abuse_userids = implode(',', $abuse_userids);
                   // dd($implode_abuse_userids);
                   $query_location->whereNotIn('user_master.user_id',$abuse_userids);
                }

                $query_location_data = $query_location->get()->toArray();
                

// ======================================= end location ===============================================
                
// ======================================= start tag or name or profassion  ============================
            $query_tag_name = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N');

                
                // $implode_tag_userids = implode(',',$tag_userids);
                if(isset($tag_userids) && !empty($tag_userids)){
                    //dd('if');
                    $query_tag_name->whereIn('user_master.user_id',$tag_userids);
                    $searchdata = str_replace(",", "", $searchdata);        
                    $search_arr = explode(" ", $searchdata);
                    if(!empty($search_arr)) {
                        foreach ($search_arr as $search_key => $search) {
                            // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                            // $query->orWhere('profession.name','like','%'. $search .'%');
                            // $query->where(function($q) use($search){
                                $query_tag_name->orWhere('user_master.fullname','like','%'. $search .'%');
                                    // ->orWhere('profession.name','like','%'. $search .'%');
                            // });
                        }               
                    }
                }
                else{
                    //dd('else');
                    $search_arr = [];
                    $searchdata = str_replace(",", "", $searchdata);
                    $search_arr = explode(" ", $searchdata);
                    //dd($search_arr);
                    if(!empty($search_arr)) {
                        $counter = 0;
                        foreach ($search_arr as $search_key => $search) {
                            if($counter == 0) {
                                // $query->where('user_master.fullname','like','%'. $search .'%');
                                // $query->orWhere('profession.name','like','%'. $search .'%');
                                $query_tag_name->where(function($q) use($search){
                                    $q->orWhere('user_master.fullname','like','%'. $search .'%');
                                        // ->orWhere('profession.name','like','%'. $search .'%');
                                });
                                //dd($result);
                            } else {
                                // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                                // $query->orWhere('profession.name','like','%'. $search .'%');
                                $query_tag_name->where(function($q) use($search){
                                    $q->orWhere('user_master.fullname','like','%'. $search .'%');
                                        // ->orWhere('profession.name','like','%'. $search .'%');
                                });
                            }
                            $counter++;
                        }
                    }
                    else{
                        // $query->where('user_master.fullname','like','%'. $search .'%');
                        // $query->orWhere('profession.name','like','%'. $search .'%');
                        $query_tag_name->where(function($q) use($search){
                            $q->where('user_master.fullname','like','%'. $search .'%');
                                // ->orWhere('profession.name','like','%'. $search .'%');
                        });
                    }
                }
                if(isset($abuse_userids) && !empty($abuse_userids)) {
                    $implode_abuse_userids = implode(',', $abuse_userids);
                   // dd($implode_abuse_userids);
                   $query_tag_name->whereNotIn('user_master.user_id',$abuse_userids);
                }
                $query_tag_name_data = $query_tag_name->get()->toArray();
                
// ======================================= end tag or name or profassion  ============================


// // ========================================= for profession ==============================
//         // dd($searchdata);
        $profession_id = Profession::select('id')
                                    ->where('name','like', '%' . $searchdata . '%')
                                    ->get()
                                    ->toArray();
                                    // dd($profession_id);
        $where_str_out = "1 = ?";
        $where_params_out = array(1);

        if($location != ''){
            $where_str_out .= " AND ( user_master.location like '%". $location ."%' )";
        }
        $profession_data = UserMaster::select('user_master.*','profession.name')
                        ->leftjoin('profession','profession.id','=','user_master.profession')
                        ->where('user_master.firebase','!=','NULL')
                        ->where('user_master.user_id','!=',$user_id)
                        ->where('user_master.is_active','Y')
                        ->where('user_master.is_deleted','N')
                        ->where('user_master.is_block','N')
                        ->whereIn('user_master.profession',$profession_id)
                        ->whereRaw($where_str_out, $where_params_out)
                        ->get()
                        ->toArray();

// // ========================================= end profession ==============================

                // name,tag and location
                $result_id =[];
                if(!empty($result)){
                    foreach ($result as $result_key => $result_value) {
                        array_push($result_id, $result_value['user_id']);
                    }
                }

                // for only name and tag
                foreach ($query_tag_name_data as $key => $value) {
                    if(!in_array($value['user_id'], $result_id)){
                        array_push($result, $value);
                        array_push($result_id, $value['user_id']);
                    }
                }

                // forr profession
                foreach ($profession_data as $key => $value) {
                    if(!in_array($value['user_id'], $result_id)){
                        array_push($result, $value);
                        array_push($result_id, $value['user_id']);
                    }
                }

                // for location
                foreach ($query_location_data as $key => $value) {
                    if(!in_array($value['user_id'], $result_id)){
                        array_push($result, $value);
                        array_push($result_id, $value['user_id']);
                    }
                }
                
                $result = array_slice($result,$pageNo,$pageCount);
        }else{
            // $result = $query->offset($pageNo)->limit($pageCount)->get()->toArray();
            $result = $query->get()->toArray();
            $result_id =[];

            // for profession
                foreach ($profession_data as $key => $value) {
                    if(!in_array($value['user_id'], $result_id)){
                        array_push($result, $value);
                        array_push($result_id, $value['user_id']);
                    }
                }
            // for profession
                foreach ($location_data as $key => $value) {
                    if(!in_array($value['user_id'], $result_id)){
                        array_push($result, $value);
                        array_push($result_id, $value['user_id']);
                    }
                }

                $result = array_slice($result,$pageNo,$pageCount);

        }
        
        return $result;
    }



    // public static function getSearchUser($searchdata, $tag_userids, $abuse_userids, $user_id, $page_count,$page_no,$location)
    // {
    //     //dd($tag_userids);
        
    //     $query = UserMaster::select('user_master.*','profession.name')
    //                     ->leftjoin('profession','profession.id','=','user_master.profession')
    //                     ->where('user_master.firebase','!=','NULL')
    //                     ->where('user_master.user_id','!=',$user_id)
    //                     ->where('user_master.is_active','Y')
    //                     ->where('user_master.is_deleted','N')
    //                     ->where('user_master.is_block','N');

    //     if($location != ''){
    //         // $query->whereRaw("FIND_IN_SET(location,$location)");
    //         $query->where('user_master.location','like', '%' . $location . '%');
    //     }
    //     $implode_tag_userids = implode(',',$tag_userids);
    //     if(isset($tag_userids) && !empty($tag_userids)){
    //         //dd('if');
    //         $query->whereIn('user_master.user_id',$tag_userids);
    //         $searchdata = str_replace(",", "", $searchdata);        
    //         $search_arr = explode(" ", $searchdata);
    //         if(!empty($search_arr)) {
    //             foreach ($search_arr as $search_key => $search) {
    //                 // $query->orWhere('user_master.fullname','like','%'. $search .'%');
    //                 // $query->orWhere('profession.name','like','%'. $search .'%');
    //                 // $query->where(function($q) use($search){
    //                     $query->orWhere('user_master.fullname','like','%'. $search .'%')
    //                         ->orWhere('profession.name','like','%'. $search .'%');
    //                 // });
    //             }               
    //         }
    //     }
    //     else{
    //         //dd('else');
    //         $search_arr = [];
    //         $searchdata = str_replace(",", "", $searchdata);
    //         $search_arr = explode(" ", $searchdata);
    //         //dd($search_arr);
    //         if(!empty($search_arr)) {
    //             $counter = 0;
    //             foreach ($search_arr as $search_key => $search) {
    //                 if($counter == 0) {
    //                     // $query->where('user_master.fullname','like','%'. $search .'%');
    //                     // $query->orWhere('profession.name','like','%'. $search .'%');
    //                     $query->where(function($q) use($search){
    //                         $q->orWhere('user_master.fullname','like','%'. $search .'%')
    //                             ->orWhere('profession.name','like','%'. $search .'%');
    //                     });
    //                     //dd($result);
    //                 } else {
    //                     // $query->orWhere('user_master.fullname','like','%'. $search .'%');
    //                     // $query->orWhere('profession.name','like','%'. $search .'%');
    //                     $query->where(function($q) use($search){
    //                         $q->orWhere('user_master.fullname','like','%'. $search .'%')
    //                             ->orWhere('profession.name','like','%'. $search .'%');
    //                     });
    //                 }
    //                 $counter++;
    //             }
    //         }
    //         else{
    //             // $query->where('user_master.fullname','like','%'. $search .'%');
    //             // $query->orWhere('profession.name','like','%'. $search .'%');
    //             $query->where(function($q) use($search){
    //                 $q->where('user_master.fullname','like','%'. $search .'%')
    //                     ->orWhere('profession.name','like','%'. $search .'%');
    //             });
    //         }
    //     }
    //     if(isset($abuse_userids) && !empty($abuse_userids)) {
    //         $implode_abuse_userids = implode(',', $abuse_userids);
    //        // dd($implode_abuse_userids);
    //        $query->whereNotIn('user_master.user_id',$abuse_userids);
    //     }
    //     // $result = $query->get()->toArray();
    //                 // dd($result);
    //     // $query->where('user_master.is_deleted','N');
    //     // $query->where('user_master.is_block','N');

    //     if(isset($page_no) && $page_no != 0){   
    //         $pageNo = (($page_no-1)*$page_count);
    //     }
    //     if(isset($page_count) && $page_count != 0){
    //         $pageCount = $page_count;
    //     }
    //     $result = $query->offset($pageNo)->limit($pageCount)->get()->toArray();
    //         //dd($result);
    //     return $result;
    // }

    // public static function getSearchUserCount($searchdata, $tag_userids, $abuse_userids, $user_id)
    // {
    //     $query = UserMaster::select('profession.name','user_master.*')->leftjoin('profession','profession.id','=','user_master.user_id');
    //     if(isset($tag_userids) && !empty($tag_userids)){
    //         $query->whereNotIn('user_master.user_id',$tag_userids);
    //         $query->orWhere('user_master.fullname','like','%'. $searchdata .'%');
    //         $query->orWhere('profession.name','like','%'. $searchdata .'%');
    //     }else{
    //         $query->where('user_master.fullname','like','%'. $searchdata .'%');
    //         $query->orWhere('profession.name','like','%'. $searchdata .'%');
    //     }
    //     if(isset($abuse_userids) && !empty($abuse_userids)) {
    //         $query->whereNotIn('user_master.user_id',$abuse_userids);
    //     }
    //     $query->where('user_master.is_deleted','N');
    //     $query->where('user_master.is_active','Y');
    //     $result_set= $query->where('user_master.is_block','N')->get()->toArray();
    //     $result = count($result_set);
    //     return $result;
    // }
    public static function getSearchUserCount($searchdata, $tag_userids, $abuse_userids, $user_id)
    {
        $query = UserMaster::select('user_master.*','profession.name')->leftjoin('profession','profession.id','=','user_master.profession','user_master.firebase')
        ->where('user_master.firebase','!=','NULL')
        ->where('user_master.user_id','!=',$user_id)
        ->where('user_master.is_active','Y')
        ->where('user_master.is_deleted','N')
        ->where('user_master.is_block','N');

        
        $implode_tag_userids = implode(',',$tag_userids);
        if(isset($tag_userids) && !empty($tag_userids)){
            //dd('if');
            $query->whereIn('user_master.user_id',$tag_userids);
            $searchdata = str_replace(",", "", $searchdata);        
            $search_arr = explode(" ", $searchdata);
            if(!empty($search_arr)) {
                foreach ($search_arr as $search_key => $search) {
                    // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                    // $query->orWhere('profession.name','like','%'. $search .'%');
                    // $query->where(function($q) use($search){
                        $query->orWhere('user_master.fullname','like','%'. $search .'%')
                            ->orWhere('profession.name','like','%'. $search .'%');
                    // });
                }               
            }
        }
        else{
            //dd('else');
            $search_arr = [];
            $searchdata = str_replace(",", "", $searchdata);
            $search_arr = explode(" ", $searchdata);
            //dd($search_arr);
            if(!empty($search_arr)) {
                $counter = 0;
                foreach ($search_arr as $search_key => $search) {
                    if($counter == 0) {
                        // $query->where('user_master.fullname','like','%'. $search .'%');
                        // $query->orWhere('profession.name','like','%'. $search .'%');
                        $query->where(function($q) use($search){
                            $q->orWhere('user_master.fullname','like','%'. $search .'%')
                                ->orWhere('profession.name','like','%'. $search .'%');
                        });
                        //dd($result);
                    } else {
                        // $query->orWhere('user_master.fullname','like','%'. $search .'%');
                        // $query->orWhere('profession.name','like','%'. $search .'%');
                        $query->where(function($q) use($search){
                            $q->orWhere('user_master.fullname','like','%'. $search .'%')
                                ->orWhere('profession.name','like','%'. $search .'%');
                        });
                    }
                    $counter++;
                }
            }
            else{
                // $query->where('user_master.fullname','like','%'. $search .'%');
                // $query->orWhere('profession.name','like','%'. $search .'%');
                $query->where(function($q) use($search){
                    $q->where('user_master.fullname','like','%'. $search .'%')
                        ->orWhere('profession.name','like','%'. $search .'%');
                });
            }
        }
        if(isset($abuse_userids) && !empty($abuse_userids)) {
            $implode_abuse_userids = implode(',', $abuse_userids);
           // dd($implode_abuse_userids);
           $query->whereNotIn('user_master.user_id',$abuse_userids);
        }
        // $query->where('user_master.is_deleted','N');
        // $query->where('user_master.is_active','Y');
        // $query->where('user_master.is_block','N');
                    //dd($result);

        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }
        $result = $query->get()->toArray();
            //dd($result);
        $result_count = count($result);
        return $result_count;
    }
}