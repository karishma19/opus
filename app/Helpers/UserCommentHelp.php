<?php

namespace App\Helpers;

use App\Models\UserComment;

use App\Models\UserTag;



class UserCommentHelp

{



	public static function fetchCountCommentbyPostIdWithoutUserData($post_id)

	{

		$commet_count =  UserComment::where('post_id',$post_id)

                        ->where('is_deleted','N')

                        ->count();

        return $commet_count;

	}



	public static function fetchLatestCommentsWithPostId($post_id)

	{

		$latest_comment = UserComment::select('user_comment.*','user_master.fullname','user_master.user_photo')

                                        ->leftJoin('user_master', 'user_comment.i_by', '=', 'user_master.user_id')

                                        ->where('user_comment.post_id',$post_id)

                                        ->orderBy('user_comment.comment_id', 'DESC')

                                        ->limit(1)

                                        ->get()

                                        ->toArray();

        return $latest_comment;

	}

    // getUserFromTag

    public static function getUserFromTag($tag,$pageCount,$pageNo,$user_id) //getsearch by user Tag

    {       
        if(isset($page_no) && $page_no != 0){   

            $pageNo = (($page_no-1)*$page_count);

        }

        if(isset($page_count) && $page_count != 0){

            $pageCount = $page_count;

        }
    
        $query = UserTag::select('user_master.fullname','user_master.user_photo','user_master.location','user_master.profession','user_tags.*')

                ->join('user_master','user_master.user_id','=','user_tags.user_id')                

                ->leftjoin('profession','profession.id','=','user_master.profession')

                ->leftjoin('user_search','user_search.user_id','=','user_master.user_id');


                $query->orWhere('user_tags.tag_id',$tag);
                     $query->orWhere(\DB::raw('TRIM(user_master.fullname)','like','%{$tag}%'));

                 $query->where('user_master.user_id','!=',$user_id);
                $tag_data = $query->offset($pageNo)->limit($pageCount)->get()->toArray();               
       
        return $tag_data;        

    }  

}