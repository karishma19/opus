<?php 



namespace App\Helpers;

use App\Models\UserPost;
use App\Models\UserAbuse;
use App\Models\PostLike;
use App\Models\UserComment;



class UserPostHelp {

	

	// getPostsWithPagecount



	public static function getPostsWithPagecount($user_id,$pageCount,$pageNo,$count,$login_user = null)

	{
        $user_id_array = $user_id;
        if(!is_array($user_id)){
            $user_id_array = [$user_id];
        }
        $where_str = "1 = ?";
        $where_params = array(1);
        
        // echo "<pre>";
        // print_r($abuse_user);
        // exit();
        $abuse_user = UserAbuse::select('user_id')->where('abuse_by',$login_user)->get()->toArray();
        $abuse_user_data = [];
        if(!empty($abuse_user)){
            foreach ($abuse_user as $abuse_user){
                array_push($abuse_user_data,$abuse_user['user_id']);
            }
        }
       
		if(is_array($user_id)){
            if(sizeof($user_id) >= 0){   
                $user_id =  implode(',',$user_id);
                $where_str .= " AND user_post.user_id IN($user_id)";                
            }
            if(!empty($abuse_user_data)){
                if(sizeof($abuse_user_data) >= 0){   
        			$abuse_user_data =  implode(',',$abuse_user_data);
                    
                	$where_str .= " AND user_post.user_id NOT IN($abuse_user_data)";	        	
                }
            }    

        }elseif ($user_id == '0') {
            $where_str .= " AND user_post.user_id != '$user_id'";
        }
        else{
        	$where_str .= " AND user_post.user_id = '$user_id'";
        }

        // if login user id set

        // $login_user = 106;

        if(isset($login_user)){
                    	$where_str .= " AND user_post.post_id NOT IN(select post_id from post_abuse where user_id = $login_user)";

        }


        // page Number

        if(isset($pageNo)){   

            $pageNo = (($pageNo-1)*$pageCount);              

            // $select->offset($pageNo);

        }

        // page count

        if(isset($pageCount)){

            // $select->limit($pageCount);

        }

        // count

        if(isset($count) && $count == 'Y'){
           
            $resultSet = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

            				->leftjoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

							->whereRaw($where_str,$where_params)

							->where('user_post.is_deleted', 'N')

							->offset($pageNo)->limit($pageCount)

							->count();

        }else{

            $resultSet = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                            ->leftjoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                            ->whereRaw($where_str,$where_params)

                            ->where('user_post.is_deleted', 'N')
                            // ->offset($pageNo)->limit($pageCount)
                            ->orderBy('user_post.post_id', 'DESC')

                            ->get()

                            ->toArray();                            
            // post_like_highst data
            $post_like_highest =  PostLike::select('post_id',\DB::raw('COUNT(post_id) as post_id_count'))
                                                        ->groupBy('post_id')
                                                        // ->limit(2)
                                                        ->get()->toArray(); 
            
            $like_array = collect($post_like_highest)->sortByDesc(function ($post_like, $key) {
                    return $post_like['post_id_count'];
            })->values()->take(2)->all();
           
            $post_like_id=[];
            foreach ($like_array as $key => $value) {
                array_push($post_like_id, $value['post_id']);
            }

            $most_like_resultSet = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                            ->leftjoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                            ->whereIn('user_post.post_id',$post_like_id)
                            ->where('user_post.is_deleted', 'N')
                            // ->offset($pageNo)->limit($pageCount)
                            ->orderBy('user_post.post_id', 'DESC')

                            ->get()

                            ->toArray();  
            

            // for most comment 
            $post_comment_highest =  UserComment::select('post_id',\DB::raw('COUNT(post_id) as post_id_count'))
                                        ->groupBy('post_id')
                                        // ->limit(2)
                                        ->get()->toArray();
            $comment_array = collect($post_comment_highest)->sortByDesc(function ($post_comment, $key) {
                    return $post_comment['post_id_count'];
            })->values()->take(2)->all();
           
            $post_comment_id=[];
            foreach ($comment_array as $key => $value) {
                array_push($post_comment_id, $value['post_id']);
            }

            $most_comment_resultSet = UserPost::select('user_post.*','user_master.fullname','user_master.user_photo')

                            ->leftjoin('user_master', 'user_post.user_id', '=', 'user_master.user_id')

                            ->whereIn('user_post.post_id',$post_comment_id)
                            ->where('user_post.is_deleted', 'N')
                            // ->offset($pageNo)->limit($pageCount)
                            ->orderBy('user_post.post_id', 'DESC')

                            ->get()

                            ->toArray();  
           
            $final_data  = array_merge($resultSet,$most_like_resultSet,$most_comment_resultSet);
            $final_unique_data = array_unique($final_data, SORT_REGULAR);
            $resultSet = array_slice($final_unique_data,$pageNo,$pageCount);

        }

        return $resultSet;

	}



}