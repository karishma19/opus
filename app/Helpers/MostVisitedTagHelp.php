<?php
namespace App\Helpers;
use App\Models\UserMaster;
use App\Models\UserSearch;

class MostVisitedTagHelp
{
	// check friends exist or not
	public static function getUserofMostVisitedTag($most_visited_tag,$page_count,$page_no,$type,$user_id)
	{
        //dd('sd');
        if(isset($page_no) && $page_no != 0){   
            $pageNo = (($page_no-1)*$page_count);
        }
        if(isset($page_count) && $page_count != 0){
            $pageCount = $page_count;
        }
        $query = UserSearch::select('user_master.fullname','user_master.user_photo','user_master.profession','user_master.location','user_search.user_id','user_search.keyword',\DB::raw('COUNT(user_search.user_id) as num'))
                            ->leftJoin('user_master', 'user_master.user_id', '=', 'user_search.user_id');
        $most_visited_tag_array = $most_visited_tag;
        //dd($most_visited_tag_array);
        if(is_array($most_visited_tag)){
            //dd('1');
        	if(sizeof($most_visited_tag) >= 0){ 
        		foreach ($most_visited_tag as $most_vstd_tag){
        			if($most_vstd_tag == $most_visited_tag_array[0]){
        				$query->where('user_search.keyword',$most_vstd_tag);
        			}
        			else{
        				$query->orWhere('user_search.keyword',$most_vstd_tag);
        			}
        		}
        	}
        	if(isset($user_id)){	
        		$query->where('user_master.user_id','!=',$user_id);			
			}
        }
        else{
            $result = $query->where('user_search.keyword',$most_visited_tag)->get()->toArray();
            //dd($result);
            if(isset($user_id)){    
            //d('ndfn');
                $query->where('user_master.user_id','!=',$user_id);
            }
        }
        $query->groupBy('user_search.user_id');
        if(isset($type)){
            if($type == 'suggestedProfile' || $type == 'searchResult'){
                //print_r($type);exit;  
                $query->orderBy('num','desc');   
            }
        }
       $result = $query->offset($pageNo)->limit($pageCount)->get()->toArray();
        //dd($result);
        if(isset($type) && $type == 'count'){
			return $result->count();
		}else{
		return $result;
		}
	}
}