<?php
namespace App\Helpers;
use App\Models\UserFriend;

class FriendsHelp
{
	// check friends exist or not
	public static function checkIsFriend($user_id,$friend_id)
	{
		$where_str = "1 = ?";
        $where_params = array(1);

        $where_str ="(user_friends.user_id = '$user_id'";
        $where_str .= " AND user_friends.friend_id = '$friend_id')";
        $where_str .= " OR (user_friends.user_id = '$friend_id' ";
        $where_str .= " AND user_friends.friend_id = '$user_id') ";
        
        $checkIsFriend = UserFriend::whereRaw($where_str,$where_params)->get()->toArray();
        $friend_id = "N";
        if (count($checkIsFriend)>0) {
            $friend_id = "Y";
        }
        return $friend_id;
	}
}