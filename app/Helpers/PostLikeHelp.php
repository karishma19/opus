<?php

namespace App\Helpers;

use App\Models\PostLike;



class PostLikeHelp

{ 

	// checkPostlikebyuser

	public static function checkPostlikebyuser($post_id,$user_id)

	{

		$postlikebySameUser = PostLike::where('post_id',$post_id)

											->where('user_id',$user_id)

											->count();



		if($postlikebySameUser > 0){

            return 'Y';

        }else{

            return 'N';

        }

	}



	//getUsersLikePost 

	public static function getUsersLikePost($post_id)

	{

		$postlikebyuser = PostLike::select('post_like.*','user_master.fullname','user_master.user_photo')

										->join('user_master', 'post_like.user_id', '=', 'user_master.user_id')

										->where('post_like.post_id',$post_id)

										->get()

										->toArray();

		return $postlikebyuser;

	}

}