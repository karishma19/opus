<?php

namespace App\Helpers;

use App\Models\Tag;

use App\Models\UserTag;



class UserTagHelp

{

	// getTag

	public static function getTags($user_id=0)

	{

		$tags = UserTag::select('tags.name as user_tag')

						->where('user_id',$user_id)

						->leftJoin('tags', 'tags.id', '=', 'user_tags.tag_id')

						->get()

						->toArray();
					
		return $tags;

	}

}