<?php



namespace App\Events;



use Illuminate\Broadcasting\Channel;

use Illuminate\Queue\SerializesModels;

use Illuminate\Broadcasting\PrivateChannel;

use Illuminate\Broadcasting\PresenceChannel;

use Illuminate\Foundation\Events\Dispatchable;

use Illuminate\Broadcasting\InteractsWithSockets;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;



class SearchEvent

{

    use Dispatchable, InteractsWithSockets, SerializesModels;



    /**

     * Create a new event instance.

     *

     * @return void

     */

    public function __construct($user_id,$page_count,$page_no,$except)

    {

        $this->user_id = $user_id;

        $this->page_count = $page_count;

        $this->page_no = $page_no;
        $this->except = $except;

    }



    /**

     * Get the channels the event should broadcast on.

     *

     * @return \Illuminate\Broadcasting\Channel|array

     */

    public function broadcastOn()

    {

        return new PrivateChannel('channel-name');

    }

}

