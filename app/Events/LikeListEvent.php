<?php



namespace App\Events;



use Illuminate\Broadcasting\Channel;

use Illuminate\Queue\SerializesModels;

use Illuminate\Broadcasting\PrivateChannel;

use Illuminate\Broadcasting\PresenceChannel;

use Illuminate\Foundation\Events\Dispatchable;

use Illuminate\Broadcasting\InteractsWithSockets;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;



class LikeListEvent

{

    use Dispatchable;



    /**

     * Create a new event instance.

     *

     * @return void

     */

    public $likeList_data;

    public function __construct($likeList_data)

    {

        $this->likeList_data = $likeList_data;

    }



    /**

     * Get the channels the event should broadcast on.

     *

     * @return \Illuminate\Broadcasting\Channel|array

     */

    public function broadcastOn()

    {

        return new PrivateChannel('channel-name');

    }

}

