<?php

namespace App\Listeners;

use App\Events\aboutUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\aboutUserJob;

class aboutUserListener
{
     /**
     * Create the event listener.
     *
     * @return void
     */

    use DispatchesJobs;
    public $userID;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  =CustomerEvent  $event
     * @return void
     */
    public function handle(aboutUserEvent $event)
    {
         $userID = $event->userID;

        return $userID = $this->dispatch(new aboutUserJob($userID));
    }
}
