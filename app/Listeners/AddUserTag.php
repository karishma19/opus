<?php



namespace App\Listeners;



use App\Events\AddUserTagEvent;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Jobs\AddUserTagJob;



class AddUserTag

{

    /**

     * Create the event listener.

     *

     * @return void

     */

    use DispatchesJobs;

    public function __construct()

    {

        //

    }



    /**

     * Handle the event.

     *

     * @param  AddUserTag  $event

     * @return void

     */

    public function handle(AddUserTagEvent $event)

    {

        $user_tags = $event->tags;


        $save_details = $this->dispatch(new AddUserTagJob($user_tags));

         return $save_details;

    }

}

