<?php

namespace App\Listeners;

use App\Events\UserTagEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserTag
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserTagEvent  $event
     * @return void
     */
    public function handle(UserTagEvent $event)
    {
        //
    }
}
