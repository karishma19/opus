<?php

namespace App\Listeners;

use App\Events\AddFriendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\AddFriendJob;

class AddFriendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddFriendEvent  $event
     * @return void
     */
    public function handle(AddFriendEvent $event)
    {
        $add_friend_data = $event->friend_data;

        $save_details = $this->dispatch(new AddFriendJob($add_friend_data));
        return $save_details;
    }
}
