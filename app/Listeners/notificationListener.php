<?php

namespace App\Listeners;

use App\Events\notificationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\notificationJob;

class notificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(notificationEvent $event)
    {
        $userID = $event->userID;
        return $userID = $this->dispatch(new notificationJob($userID));
    }
}
