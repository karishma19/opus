<?php



namespace App\Listeners;



use App\Events\LikeListEvent;

use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Foundation\Bus\DispatchesJobs;

use App\Jobs\GetLikeJob;



class GetLikeListener

{

    /**

     * Create the event listener.

     *

     * @return void

     */

    use DispatchesJobs;

    public function __construct()

    {

        //

    }



    /**

     * Handle the event.

     *

     * @param  GetFriendEvent  $event

     * @return void

     */

    public function handle(LikeListEvent $event)

    {

        $likeList_data = $event->likeList_data;

        $like_details = $this->dispatch(new GetLikeJob($likeList_data));

        return $like_details;

    }

}

