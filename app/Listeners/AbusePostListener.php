<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AbusePostEvent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\AbusePostJob;

class AbusePostListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AbusePostEvent $event)
    {
        $data = $event->data;  
        $data = $this->dispatch(new AbusePostJob($data));
        return $data;
    }
}
