<?php

namespace App\Listeners;

use App\Events\AddTestimonialEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\AddTestimonialJob;

class AddTestimonialListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddTestimonialEvent  $event
     * @return void
     */
    public function handle(AddTestimonialEvent $event)
    {
        $testimonial_data = $event->testimonial_data;
        $testimonial_data = $this->Dispatch(new AddTestimonialJob($testimonial_data));
        return $testimonial_data;
    }
}
