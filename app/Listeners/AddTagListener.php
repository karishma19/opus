<?php

namespace App\Listeners;

use App\Events\AddTagEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\AddTagJob;

class AddTagListener
{
    use DispatchesJobs;
    public $add_tag;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AddTagEvent $event)
    {

        $add_tag = $event->add_tag;

        $add_tag = $this->Dispatch(new AddTagJob($add_tag));

        return $add_tag;    
    }
}
