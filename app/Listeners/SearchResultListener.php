<?php

namespace App\Listeners;

use App\Events\SearchResultEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SearchResultJob;

class SearchResultListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SearchResultEvent  $event
     * @return void
     */
    public function handle(SearchResultEvent $event)
    {
        $user_id = $event->user_id;
        $page_count = $event->page_count;
        $page_no = $event->page_no;
        $tag = $event->tag;
        $user_id = $this->Dispatch(new SearchResultJob($user_id,$page_count,$page_no,$tag));
        return $user_id;
    }
}
