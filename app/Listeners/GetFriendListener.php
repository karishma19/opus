<?php

namespace App\Listeners;

use App\Events\GetFriendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\GetFriendJob;

class GetFriendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetFriendEvent  $event
     * @return void
     */
    public function handle(GetFriendEvent $event)
    {
        $friend_data = $event->friend_data;
        $friend_details = $this->dispatch(new GetFriendJob($friend_data));
        return $friend_details;
    }
}
