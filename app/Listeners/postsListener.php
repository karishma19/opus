<?php

namespace App\Listeners;

use App\Events\postsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\postsJob;

class postsListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(postsEvent $event)
    {
     $userID = $event->userID;
     // dd($userID);
     return $userID = $this->dispatch(new postsJob($userID));
 }
}
