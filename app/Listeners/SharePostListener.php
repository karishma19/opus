<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SharePostJob;
use App\Events\SharePostEvent;

class SharePostListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SharePostEvent $event)
    {
         $data = $event->data; 
         
        $data = $this->dispatch(new SharePostJob($data));

        return $data;
    }
}
