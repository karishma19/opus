<?php

namespace App\Listeners;

use App\Events\CommentListEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CommentListJob;


class CommentListListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CommentListEvent $event)
    {
        $userID = $event->userID;
        return $userID = $this->dispatch(new CommentListJob($userID));
    }
}
