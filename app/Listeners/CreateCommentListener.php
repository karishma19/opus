<?php

namespace App\Listeners;

use App\Events\CreateCommentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CreateCommentJob;

class CreateCommentListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateCommentEvent  $event
     * @return void
     */
    public function handle(CreateCommentEvent $event)
    {
        $comment_data = $event->comment_data;
        $comment_data = $this->Dispatch(new CreateCommentJob($comment_data));
        return $comment_data;
    }
}
