<?php

namespace App\Listeners;

use App\Events\SearchEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SearchJob;

class SearchListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SearchEvent  $event
     * @return void
     */
    public function handle(SearchEvent $event)
    {
        $user_id = $event->user_id;
        $page_count = $event->page_count;
        $page_no = $event->page_no;
        $except = $event->except;
        $user_id = $this->Dispatch(new SearchJob($user_id,$page_count,$page_no,$except));
        //dd($user_id);
        return $user_id;
    }
}
