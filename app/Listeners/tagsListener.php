<?php

namespace App\Listeners;

use App\Events\tagsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\tagsJob;

class tagsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(tagsEvent $event)
    {
       $userID = $event->userID;
       return $userID = $this->dispatch(new tagsJob($userID));
   }
}
