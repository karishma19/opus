<?php

namespace App\Listeners;

use App\Events\GiveRateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\GiveRateJob;

class GiveRateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GiveRateEvent  $event
     * @return void
     */
    public function handle(GiveRateEvent $event)
    {
        $rating_detail = $event->rating_detail;

        $rating_detail = $this->dispatch(new GiveRateJob($rating_detail));
        return $rating_detail;
    }
}
