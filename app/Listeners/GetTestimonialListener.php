<?php

namespace App\Listeners;

use App\Events\GetTestimonialEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\GetTestimonialJob;

class GetTestimonialListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GetTestimonialEvent $event)
    {
        $testimonials_details = $event->testimonials_details;  
        
        $testimonials_details = $this->dispatch(new GetTestimonialJob($testimonials_details));
        return $testimonials_details;
    }
}
