<?php

namespace App\Listeners;

use App\Events\NotificationSettingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\NotificationSettingJob;

class NotificationSettingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSettingEvent  $event
     * @return void
     */
    public function handle(NotificationSettingEvent $event)
    {
        $notification_setting = $event->notification_setting;

        $notification_setting_details = $this->dispatch(new NotificationSettingJob($notification_setting));
        return $notification_setting_details;
    }
}
