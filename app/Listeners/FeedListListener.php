<?php

namespace App\Listeners;

use App\Events\FeedListEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\FeedListJob;

class FeedListListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FeedListEvent  $event
     * @return void
     */
    public function handle(FeedListEvent $event)
    {
        $feed_data = $event->feed_data;

        $feed_details = $this->dispatch(new FeedListJob($feed_data));
        return $feed_details;
    }
}
