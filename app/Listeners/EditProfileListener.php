<?php

namespace App\Listeners;

use App\Events\EditProfileEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\EditProfileJob;

class EditProfileListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EditProfileEvent  $event
     * @return void
     */
    public function handle(EditProfileEvent $event)
    {
        $profile_data = $event->profile_data;
        $profile_data = $this->Dispatch(new EditProfileJob($profile_data));
        return $profile_data;
    }
}
