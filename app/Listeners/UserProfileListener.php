<?php

namespace App\Listeners;

use App\Events\UserProfileEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\UserProfileJob;

class UserProfileListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    use DispatchesJobs;
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserProfileEvent  $event
     * @return void
     */
    public function handle(UserProfileEvent $event)
    {
        $user_data = $event->user_data;
        $save_details = $this->dispatch(new UserProfileJob($user_data));
        return $save_details;
    }
}
