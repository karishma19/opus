<?php



use Illuminate\Http\Request;

/*

|--------------------------------------------------------------------------

| API Routes

|--------------------------------------------------------------------------

|

| Here is where you can register API routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| is assigned the "api" middleware group. Enjoy building your API!

|

*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();

});



// middleware 

Route::group(['middleware' =>'client_credentials','namespace'=>'API'], function(){

	Route::post('/login','UserController@loginUser');    

	Route::post('/sociallogin','UserController@socialLogin');    

	Route::get('/user-profile','UserController@userProfile');    

	Route::get('/friends','FriendController@getFriends');    

	Route::post('/add-friend','FriendController@addFriend');    

	Route::get('/add-user-tag','TagController@addUserTag');    

	Route::get('/user-tag-delete','TagController@deleteUserTag');    

	Route::post('/giveRating','RatingController@giveRating');    

	Route::post('notification-setting','NotificationController@notificationSetting');

	Route::post('/edit-profile','UserController@EditProfile');

	Route::get('/cms-page','APIController@CMSPages');

	Route::get('/alltags','TagController@getAllTag');

	Route::get('clearAllNotifications','NotificationController@clearAllNotifications');

	Route::post('/unfriends','FriendController@unFriend');

	Route::get('/like-post','LikeController@likePost');    

	Route::post('/createComment','CommentController@CreateComment');  

	Route::post('/addTestimonial','TestimonialController@AddTestimonial'); 

	Route::post('/logout','UserController@Logout');

	Route::post('/createPost','PostController@createPost');   

	Route::get('/feedList','FeedListController@feedList');   

	Route::get('/search','SearchController@search');

	Route::post('/abusePost','PostController@abusePost'); 

    Route::get('/testimonials','TestimonialController@getTestimonial');

	Route::get('/searchresult','SearchResultController@SearchResult');

	Route::get('aboutUser','UserController@aboutUser');

	Route::get('notification','NotificationController@notification');

	Route::get('tags','TagController@tags');

	Route::get('posts','PostController@posts');

	//Route::get('getprofession','APIController@getProfession');

	Route::get('commentList','CommentController@commentList');

	Route::post('/sharePost','PostController@sharePost');

	Route::post('abuseUser','UserController@blockUser');
	Route::post('/completeProfile','TagController@addTag');

	Route::post('/getVersion','APIController@GetVersion');
	Route::post('chatNotification','UserController@chatNotification');
	Route::post('userChatHistory','UserController@userChatHistory');
	Route::post('userChatList','UserController@userChatList');
	Route::post('/userChatIsBlock','UserController@userIsBlock');
	Route::post('/isDotEnable','UserController@isDotEnable');
});

Route::group(['namespace'=>'API'], function(){
	
	Route::get('clearChatNotifications','NotificationController@clearChatNotifications');

});
Route::get('sendnotification',function(){
		// url 
        $url = 'https://fcm.googleapis.com/fcm/send';
		$message ='hi';
		$new_badge=1;
		$notification_type =3;
		$post_id =1;
		$device_token = 'c4LZzAauNZc:APA91bGi-P0wwDkwtRtru1JZ6MttZ9bMId0Vnw2y2eC9pjZQXDF-udAp6bpZO56vuUtzR691pQhF49bF_bqhrtBYXmX2vs86TmFhiaS_WdpIF1j5Wz1LHvvW1AbDk8mwT2vNHh1K2B9k';
		$msg = array
		(
			'title'     => 'OPUS',
                'body'  => $message,
                "sound"=> "default",       
                "content-available"=> 1,       
                "mutable-content"=> 1,  
                'type'  => $notification_type
		);
		// registration_ids  = divice ID
		$fields = array (
            'registration_ids' => array ($device_token),
            'data'=>$msg,
            'notification' => $msg

	    );
        $fields = json_encode ( $fields );
        // echo "<pre>";
        // print_r($fields);
        // exit();
        $headers = array ('Authorization: key=' . "AAAA3dsJ-Tk:APA91bEkcLCIHGRfdpWcN6lse4_8kABxG0m11DnrGrOYO85JgqVHMLfNu_qgnmUhIoF52Aw-WHMyBPAJapvor6jy8xel8-T6AuzNyVOEJZQ4nkoh9QE7ghHK1UFpFKEcX2s2YrIOKk_r",
        	'Content-Type: application/json'
                	);
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        curl_close ( $ch ); 
        echo "<pre>";
        print_r($result);
        exit();
});


